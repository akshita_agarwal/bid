import React, { PropTypes, Component } from 'react'
import { TouchableOpacity,Linking, Text, View, Image,Platform, WebView } from "react-native";
//import style from "./style";
import Spinner from 'react-native-loading-spinner-overlay';
const logo_img = require('../themes/Images/logo-img.png');
const iLogoRed = require('../themes/Images/iLogoRed.png');
const bg_red = require('../themes/Images/bg_red.png');
import { AppStyles, AppSizes, AppColors } from '../themes/'
import TimerCountdown from 'react-native-timer-countdown';
import OpenFile from 'react-native-doc-viewer';

const Buttons = props => {
    const { text, onPress, style } = props;
    return (
        <TouchableOpacity onPress={onPress}>
            <Text>{text}</Text>
        </TouchableOpacity>
    )
}

export const LoginButton = props => {
    const { text, onPress, style } = props;
    return (
        <TouchableOpacity onPress={onPress} style = {{justifyContent : "center", alignItems : "center"}} activeOpacity={.8} >
            <Text style={[style,{overflow: (Platform.OS == "ios")?'hidden': null},{textAlign : "center"}]}>{text}</Text>
        </TouchableOpacity>
    )
}
export const ForgotButton = props => {
    const { text, onPress, style } = props;
    return (
        <TouchableOpacity onPress={onPress} activeOpacity={.8} >
            <Text style={style}>{text}</Text>
        </TouchableOpacity>
    )
}
export const RegisterButton = props => {
    const { text, onPress, style } = props;
    return (
        <TouchableOpacity onPress={onPress} activeOpacity={.8} >
            <Text style={style}>{text}</Text>
        </TouchableOpacity>
    )
}
export const Button2 = props => {
    const { text, onPress, style } = props;
    return (
        <TouchableOpacity onPress={onPress}>
            <Text>{text}</Text>
        </TouchableOpacity>
    )
}

export class Header extends React.Component
{
    constructor(props){
      super(props);

      this.state = {
        "_visible" : false,

      }
    }

    render(){
      const { text , navigation} = this.props;
      return (
          <View style={{ position: 'absolute', top: 0, width: AppSizes.screen.width, }}>
              <View style={AppStyles.Headerbckimg}>
                  <Image source={logo_img} style={AppStyles.customcpimg} />
                <Spinner visible={this.state._visible} textContent={"User guide loading.."} textStyle={{ color: '#FFF' }} />
              {
                (text != "User Guide")  ?

              <TouchableOpacity style = {{marginTop : 70, marginLeft : AppSizes.screen.width - 43, height : 42, width : 42,position :"absolute", alignItems :"center", justifyContent :"center"}} onPress = {()=>{this.openPDF(this.props)}} >
                  <Image  style = {{height : 30, width : 30}} source={iLogoRed} />
              </TouchableOpacity>:null

            }
            </View>
              <View style={AppStyles.Titlebckimg}>
                  <Image source={bg_red} style = {(Platform.OS == "ios") ? { width: 320, height: 46,resizeMode: 'contain',marginLeft:0}:{ width: 320, height: 46} }/>
                  <Text style={AppStyles.TitleTxt}>{text}</Text>
              </View>
          </View>
      )
    }

/*
    Show the PDF In WebView in IOS and show the PDF in PDF view in Android
*/
openPDF = () =>{

  if (Platform.OS == 'ios') {
    this.props.navigation.navigate('webview', { url: 'http://app.contactbids.com/Content/pdf/guide.pdf',headerTxt:"User Guide"})
  }else {
    this.setState({_visible : true})
    OpenFile.openDoc([{
              url: 'http://app.contactbids.com/Content/pdf/guide.pdf',
              fileName: "sample",
              cache: false,
              fileType: ""
          }], (error, url) => {
              if (error) {
                  Alert.alert("URL not found");
              } else {
                  console.log(url)
                    this.setState({_visible : false})
              }
          })

   }
  }
}



// export const Header = props => {
//     const { text , navigation} = props;
//
//     return (
//         <View style={{ position: 'absolute', top: 0, width: AppSizes.screen.width, }}>
//             <View style={AppStyles.Headerbckimg}>
//                 <Image source={logo_img} style={AppStyles.customcpimg} />
//               <TouchableOpacity style = {{marginTop : 70, marginLeft : AppSizes.screen.width - 43, height : 42, width : 42,position :"absolute", alignItems :"center", justifyContent :"center"}} onPress = {()=>{this.openPDF(props)}} >
//                 <Image  style = {{height : 30, width : 30}} source={iLogoRed} />
//             </TouchableOpacity>
//           </View>
//             <View style={AppStyles.Titlebckimg}>
//                 <Image source={bg_red} style = {(Platform.OS == "ios") ? { width: 320, height: 46,resizeMode: 'contain',marginLeft:0}:{ width: 320, height: 46} }/>
//                 <Text style={AppStyles.TitleTxt}>{text}</Text>
//             </View>
//         </View>
//     )
// }



export const ShareBtn = props => {
    const { text, onPress, style } = props;
    return (
        <TouchableOpacity onPress={onPress} style = {{alignItems : "center", justifyContent : "center"}}>
            <Text style={[style,{overflow: (Platform.OS == "ios")?'hidden': null},{textAlign : "center"}]}>{text}</Text>
        </TouchableOpacity>
    )
}

export const BidderListBtn = props => {
    const { text, onPress, style } = props;
    return (
        <TouchableOpacity onPress={onPress}>
            <Text style={style}>{text}</Text>
        </TouchableOpacity>
    )
}

export const Countdwn = props => {
    const { time } = props;
    return (
        <TimerCountdown
            initialSecondsRemaining={time}
            onTick={() => console.log('tick')}
            onTimeElapsed={() => console.log('complete')}
            allowFontScaling={true}

        />
    )
}

export const webview = props => {
    const { url } = props;

    return (
        <WebView
            source={{ uri: url }}
            style={{ marginTop: 0 }}
        />
    )
}
export const NoRecordFound = props =>{

  const { txtMsg,marginTopSpace } = props;

  let msg =( (txtMsg != undefined)?txtMsg : "No Record Found");
  let topSpace =( (marginTopSpace != undefined)?marginTopSpace : AppSizes.screen.height/2);


            return(

                 <View style ={[{marginTop:topSpace,alignItems:"center",justifyContent :"center"}]} ><Text style = {{alignSelf :"center",fontSize : 17, fontWeight :"200"}}> {msg}</Text></View>
            )

}

export default Buttons
