

class Constant {
    /**
     * Demo URL Base URL
     */
   static BASE_URL = "http://wds2.projectstatus.co.uk/ContactBidsApp/api/ContactBidsAppAPI/";
    /**
     * Live URL Base URL
     */

  // static BASE_URL = "http://app.contactbids.com/api/ContactBidsAppAPI/";

    static URL_UserLoginWithFacebook = Constant.BASE_URL + 'LoginWithFacebook';
    static URL_UserLoginWithMobile = Constant.BASE_URL + 'LoginWithMobile';
    static URL_RegisterUser = Constant.BASE_URL + 'MobileRegistration';
    static URL_ForgotPassword = Constant.BASE_URL + 'ForgotPassword';
    static URL_Update_Profile = Constant.BASE_URL + 'UserProfileUpdate';
    static URL_UserPhoneNoUpdate = Constant.BASE_URL + 'UserPhoneNoUpdate';
    static URL_GetProductCategory = Constant.BASE_URL + 'GetProductCategory';
    static URL_AddUpdateProduct = Constant.BASE_URL + 'AddUpdateProduct';
    static URL_GetProductList = Constant.BASE_URL + 'GetProductList';
    static URL_ViewProfile = Constant.BASE_URL + 'GetUserProfile';
    static URL_ShareAuction = Constant.BASE_URL + 'ShareAuction';
    static URL_AuctionInviteList = Constant.BASE_URL + 'AuctionInviteList';
    static URL_SetBidAmount = Constant.BASE_URL + 'SetBidAmount';
    static URL_InvitedUserBySMS = Constant.BASE_URL + 'InvitedUserBySMS';
    static URL_GetBiddersList = Constant.BASE_URL + 'GetBiddersList';
    static URL_UpdateAuctionStatus = Constant.BASE_URL + 'UpdateAuctionStatus';
    static URL_UserLogout = Constant.BASE_URL + 'UserLogout';
    static URL_AuctionWonListByBidder = Constant.BASE_URL + 'AuctionWonListByBidder';
    static URL_DeleteProductByID = Constant.BASE_URL + 'DeleteProductByID';
    static URL_AuctionWonDelete = Constant.BASE_URL + 'AuctionWonDelete';
    static URL_DeleteAuctionInvite = Constant.BASE_URL + 'DeleteAuctionInvite';
    static URL_AddUpdateExchangeProduct = Constant.BASE_URL + 'AddUpdateExchangeProduct';
    static URL_GetMyExchangeProductList = Constant.BASE_URL + 'GetMyExchangeProductList';
    static URL_GetProductWithExchangeProduct = Constant.BASE_URL + 'GetProductWithExchangeProduct';
    static URL_DeleteExchangeProductByID = Constant.BASE_URL + 'DeleteExchangeProductByID';
    static URL_ExchangeProductListByProductID = Constant.BASE_URL + 'ExchangeProductListByProductID';
    static URL_ExchangeProductDetail = Constant.BASE_URL + 'ExchangeProductDetail';
    static URL_AcceptExchangeProduct = Constant.BASE_URL + 'AcceptExchangeProduct';
    static URL_GetAuctionInviteProductDetail = Constant.BASE_URL + 'GetAuctionInviteProductDetail';
    static URL_GetProductDetailByProductID = Constant.BASE_URL + 'GetProductDetailByProductID';
    static URL_AuctionWonDetails = Constant.BASE_URL + 'AuctionWonDetails';
    static URL_GetMyAcceptedExchangeProductDetail = Constant.BASE_URL + 'GetMyAcceptedExchangeProductDetail'
    static URL_GetMyAcceptedExchangeProductList = Constant.BASE_URL + 'GetMyAcceptedExchangeProductList'
  }


var WebServices = {
    callWebService: function (url, body) {
        return fetch(url, {
            method: 'POST',
            headers: {

                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'CallToken': body.CallToken ? body.CallToken : '',
            },
            body: JSON.stringify(body)
        })
            .then(response => response.text()) // Convert to text instead of res.json()
            .then((text) => {
                return text;
            })
            .then(response => JSON.parse(response)) // Parse the text.
            .then((jsonRes) => {
                return jsonRes; //main output
            });
    },


    callWebService_GET: function (url, body) {

        console.log("Web Url " + url + " body " + JSON.stringify(body));

        return fetch(url, { // Use your url here
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'CallToken': body.CallToken ? body.CallToken : '',
            },
        })
            .then(response => response.text()) // Convert to text instead of res.json()
            .then((text) => {

                return text;
            })
            .then(response => JSON.parse(response)) // Parse the text.
            .then((jsonRes) => {

                return jsonRes;
            });
    }
}
module.exports = {
    Constant,
    WebServices
}
