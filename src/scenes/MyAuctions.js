/**
 * Created by Rishi Paliwal
 * 9 Nov 2017
 * FileManager Class
 */

 import React, { Component } from 'react';
 import { DrawerNavigator, StackNavigator, } from 'react-navigation';
 import { AppStyles, AppSizes, AppColors } from '../themes/'
 import Buttons, { LoginButton, Button2, Header, ShareBtn, BidderListBtn, Countdwn,NoRecordFound } from "../components/Buttons";
 import Spinner from 'react-native-loading-spinner-overlay';
 import {
     Platform,
     StyleSheet,
     Text,
     View,
     Button,
     ListView,
     Image,
     ScrollView,
     AsyncStorage,
     BackHandler,
     Alert,
     AlertIOS,
     TouchableWithoutFeedback,
     TouchableOpacity,
     RefreshControl,
     SafeAreaView,
     DeviceEventEmitter
 } from 'react-native';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import SearchBar from 'react-native-material-design-searchbar';
 var folderIcon = require('../themes/Images/folder.png')
 const bckimg = require('../themes/Images/bg-bottom.png')
 const deleteimg = require('../themes/Images/close_red.png')
 import * as commonFunctions from '../utils/CommonFunctions'
 var Constant = require('../api/WebInteractor').Constant;
 var WebServices = require('../api/WebInteractor').WebServices;
 import TimerCountdown from 'react-native-timer-countdown'
 const _that = '';
 var data = [];
 var ds;
 var key;
 var search='';
 class MyAuctions extends React.Component {
     constructor(props) {
         super(props);
         _that = this
         ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
         this.state = {
             dataSource: ds.cloneWithRows(data),
             _visible : false,
             isBidderExist:false,
             boxHeight:315,
             isBack:false,
             WinProductId:'',
             refreshing: false,
             searchText:'',
             search:'',
             isConnected : true,
             isDataAvailable:true,//intially its show LISTVIEW with zero Items

         };
     }

     componentWillMount() {

          key = this.props.navigation.state.key
         AsyncStorage.getItem('UserData')
             .then((res) => {
                 if (res) {
                     var data = JSON.parse(res)
                     if (data.isRemember) {
                         this.setState({
                             UserID: data.UserID
                         })
                         this.FetchMyAuction()
                     }
                 }
             });

             BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
             DeviceEventEmitter.addListener("isConnected",this.checkConnection.bind(this));

     }
     componentWillUnmount() {
          search=''
         BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
           DeviceEventEmitter.removeListener("isConnected",this.checkConnection.bind(this));
     }

   //GO BACK TO previous screen on device back button click
   handleBackButtonClick() {
       _that.props.navigation.goBack();
       return true;
    }

    _onRefresh() {
      if(_that.state.isConnected){
        this.setState({refreshing: true});
        this.FetchMyAuction()
      }
    }

    _searchProducts=(searchText)=>{
      this.setState({searchText:searchText})
      }

   //Check the Internet Connection
      checkConnection(data){

          this.setState({isConnected : data.isConnected});
    }

      _searchIconClicked=()=>{
        if(_that.state.isConnected){
            search = this.state.searchText

            if(this.state.searchText==''){
              this.setState({ _visible: false })
              commonFunctions.message('Please enter search text')
            }else{
              this.FetchMyAuction()
            }
          }
      }


     render() {


         return (
             <KeyboardAwareView animated={true}>
            <SafeAreaView style={{ flex: 1}}>
            <View style={{flex: 1, backgroundColor: AppColors.white, }}>
             <Image source={bckimg} style={{ width: AppSizes.screen.width, height: AppSizes.screen.height, position: 'absolute', top: 0, left: 0 }} />

             <View style = {{flexDirection:'row'}}>
                  <Header text='My Auction List' navigation = {this.props.navigation} />

                   <SearchBar
                       onSearchChange={(searchText) => this._searchProducts(searchText)}
                       height={50}
                       onFocus={() => console.log('On Focus')}
                       onBlur={() => console.log('On Blur')}
                       placeholder={'Search'}
                       autoCorrect={false}

                       inputStyle = {{marginTop:160,width:AppSizes.screen.width-60}}
                       returnKeyType={'done'}
                     />

                     <TouchableOpacity style = {{marginTop:165,height:49,width:40,justifyContent:'center',backgroundColor:'#ddd',marginRight:10}} onPress = {this._searchIconClicked}>
                        <Image source = {require('../themes/Images/search_icon.png')} style = {{width:25,height:25,alignSelf:'center',resizeMode:'contain'}}/>
                     </TouchableOpacity>

                   </View>

                   <ScrollView style={{flex: 1 }}
                               refreshControl={
                                                <RefreshControl
                                                  refreshing={this.state.refreshing}
                                                    onRefresh={this._onRefresh.bind(this)}
                                                />
                                          }>


                {
                  (this.state.isDataAvailable==true)?
                 <ListView
                     contentContainerStyle={AppStyles.filemanager_list}
                     dataSource={this.state.dataSource}
                     renderRow={(data, rowID) => this.renderRow(data, rowID)}
                     enableEmptySections={true}

                 />:<NoRecordFound marginTopSpace = {AppSizes.screen.height/5}></NoRecordFound>
              }
                 </ScrollView>
                 <Spinner visible={this.state._visible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />

             </View>
             </SafeAreaView>
             </KeyboardAwareView>
         );
     }
     /**
      * Display Products list  from API's
      */
     renderRow(data, rowID) {


         const profile_pic = (data.ProductImage) ? { uri: data.ProductImage } : '';
         const time = (data.TimeLeft * 1000)
         return (
           <View>


             <TouchableWithoutFeedback onPress = {()=>{if(_that.state.isConnected){this.goToDetail(data)}}}>
               <View style={[AppStyles.filemanager_list_item,{height:(Platform.OS == "ios"? 265 :250)},{paddingTop : 0},{marginTop : 30}]}>
              <View  onPress = {()=>{}} style = {{}}>


                 <View style={[AppStyles.filemanager_folder_view,{paddingTop : 10}]}>
                     <Image
                         source={profile_pic}
                         style={AppStyles.filemanager_folder_icon}
                     />
                 </View>



                 <View style={[AppStyles.filemanager_folder_view,{height : 60, paddingTop:5,marginTop:5}]}>
                     <Text style={[AppStyles.filemanager_folder_title,{paddingTop :10}]} numberOfLines={2}>Title : {data.Title} {"\n"}
                     </Text>
                     <Text style={AppStyles.filemanager_folder_title} numberOfLines={1}>{data.Description} {"\n"}
                     </Text>


                 </View>

                 <View style={[AppStyles.filemanager_folder_view,{paddingTop:3},{marginBottom : 20}]}>
                   <ShareBtn text="Details" onPress={()=>{if(_that.state.isConnected){this.goToDetail(data)}}} style={[AppStyles.Auctionlist_button,{fontSize : (Platform.OS == "ios")? 11 : 12}, {justifyContent:'center', fontWeight:'bold',backgroundColor: '#f77f83' },{width:100,height:38,paddingTop:10},{alignSelf: 'center'},{marginBottom:20}]} />
                 </View>

              </View>
            </View>
            </TouchableWithoutFeedback>

                 <TouchableOpacity onPress = {()=>{if(_that.state.isConnected){this.deleteProduct(data.Title,data.ID)}}} style={{ width: 30, height: 40, top: 15 ,position : 'absolute' ,alignSelf: "flex-end" }} >
                     <Image source={deleteimg} style ={{position : 'absolute' }} />
                  </TouchableOpacity>

             </View>

         )

     }

     /*
        goto detail screen
      */

      goToDetail=(data)=>{_that.props.navigation.navigate('Details',{ProductID:data.ID,Routekey:key,updateMyAuction:this.updateMyAuction.bind(this)})}



     /*
      this method delete the product
     */
       deleteProduct = (ProductName,ProductID)=> {

         var data = {
           "ProductID": ProductID
         }

         if(Platform.OS=="ios")
         {
               setTimeout(()=>(AlertIOS.alert(
               ProductName,
               'Are you sure you want to delete this product',
               [
                 {
                   text: 'YES',onPress: () =>{this.PostToApiCalling('POST', 'DeleteProductByID', Constant.URL_DeleteProductByID, data)}

                 },
                 {
                   text: 'NO'
                 },

               ]
             )), 100)

             }
             else {
               Alert.alert(
                       ProductName,
                       'Are you sure you want to delete this product',
                       [
                         {
                           text: 'YES',onPress: () =>{this.PostToApiCalling('POST', 'DeleteProductByID', Constant.URL_DeleteProductByID, data)}
                         },
                         {
                           text: 'NO'
                         },

                       ],
                       { cancelable: false }
                     )

             }






       }



      /**
       * [FetchMyAuction calls GetProductList api for MyAuction products section]
       */
     FetchMyAuction() {


         this.setState({ _visible: true })
         var data = {
             "UserID": this.state.UserID,
             "PageNumber": 1,
             "PageSize": 1000,
             "Search":search
         }


         this.PostToApiCalling('POST', 'GetProductList', Constant.URL_GetProductList, data);
         this.setState({refreshing:false})
     }


     /**
     * API Calling
     */


     PostToApiCalling(method, apiKey, apiUrl, data) {
         new Promise(function (resolve, reject) {
             if (method == 'POST') {
                 resolve(WebServices.callWebService(apiUrl, data));
             } else {
                 resolve(WebServices.callWebService_GET(apiUrl, data));
             }
         }).then((jsonRes) => {
             if ((!jsonRes) || (jsonRes.ResponseCode == 0)) {
                 this.apiSuccessfullResponse(apiKey, jsonRes)
                 this.setState({ _visible: false })

             } else {
                 commonFunctions.message(jsonRes.ResponseMessage)
                 this.setState({ _visible: false })
             }
         }).catch((error) => {
             console.log("ERROR" + error);
         });
     }



     apiSuccessfullResponse(apiKey, jsonRes) {

         if (apiKey == 'GetProductList') {
             var jsonResponse = jsonRes.ResponsePacket;

             if (jsonResponse.length == 0)
             {
               _that.setState({dataSource:ds.cloneWithRows([])})
               search=''

                  _that.setState({ _visible: false })
                  _that.setState({ isDataAvailable: false })


             } else {
                 // commonFunctions.message(jsonRes.ResponseMessage)
                 this.setState({ _visible: false, dataSource: ds.cloneWithRows(jsonResponse), })
             }
         }

         else if (apiKey == 'DeleteProductByID'){

           _that.FetchMyAuction()

         }
     }

     /*
      method for refreshing detail after winning the product in BidderList
     */
     refresh=(product_id)=>{

       if(_that.state.isConnected)
       {
           this.FetchMyAuction();
           this.setState({WinProductId:product_id})
        }
     }

     /*
      method for updating product details after returning from update product
      */
      updateMyAuction=()=>{
        this.FetchMyAuction()
      }
 }

 export default MyAuctions
