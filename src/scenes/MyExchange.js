import React, { Component } from 'react';
import { AppStyles, AppSizes, AppColors } from '../themes/'
import Buttons, { NoRecordFound, LoginButton, Button2, Header, ShareBtn, BidderListBtn, Countdwn } from "../components/Buttons";
import Spinner from 'react-native-loading-spinner-overlay';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Button,
    ListView,
    Image,
    ScrollView,
    AsyncStorage,
    BackHandler,
    Alert,
    AlertIOS,
    TouchableOpacity,
    RefreshControl,
    SafeAreaView,
    DeviceEventEmitter,
    TouchableWithoutFeedback
} from 'react-native';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import SearchBar from 'react-native-material-design-searchbar';
import SegmentedControlTab from 'react-native-segmented-control-tab'
const bckimg = require('../themes/Images/bg-bottom.png')
const deleteimg = require('../themes/Images/close_red.png')
import * as commonFunctions from '../utils/CommonFunctions'
var Constant = require('../api/WebInteractor').Constant;
var WebServices = require('../api/WebInteractor').WebServices;
import TimerCountdown from 'react-native-timer-countdown'
const _that = '';
var data=[];
var ds;
var search='';
class MyExchange extends React.Component {
    constructor(props) {
        super(props);
        _that = this
        ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            dataSource: ds.cloneWithRows(data),
            _visible : false,
            searchText:'',
            selectedIndex:0,
            refreshing:false,
            isConnected : true,
            isDataAvailable:true,//intially its show LISTVIEW with zero Items

        };
    }

    componentWillMount() {

        if(this.props.navigation.state.params.ProductID==""){
          AsyncStorage.getItem('UserData')
              .then((res) => {
                  if (res) {
                      var data = JSON.parse(res)
                      if (data.isRemember) {
                          this.setState({
                              UserID: data.UserID
                          })
                          this.FetchMyExchange()
                      }
                  }
              });

        }else{
          AsyncStorage.getItem('UserData')
              .then((res) => {
                  if (res) {
                      var data = JSON.parse(res)
                      if (data.isRemember) {
                          this.setState({
                              UserID: data.UserID
                          })
                          this.setState({selectedIndex:2})
                          this.handleIndexChange(2);
                      }
                  }
              });


          }

            BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
            DeviceEventEmitter.addListener("isConnected",this.checkConnection.bind(this));
    }
    componentWillUnmount() {
        search=''
          BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
          DeviceEventEmitter.removeListener("isConnected",this.checkConnection.bind(this));
    }

 //GO BACK TO previous screen on device back button click
  handleBackButtonClick() {
      _that.props.navigation.goBack();
      return true;
   }

   checkConnection(data){

            this.setState({isConnected : data.isConnected});
      }



   /*
    Search MyExchange and Request Exchange Products with title
  */
   _searchProducts=(searchText)=>{
     this.setState({searchText:searchText})



   }

   _searchIconClicked=()=>{
     if(_that.state.isConnected){
     if(_that.state.isConnected){
     search = this.state.searchText
           if(this.state.selectedIndex==0)
           {

                 if(this.state.searchText=='')
                 {
                   this.setState({ _visible: false })
                   commonFunctions.message('Please enter search details')
                 }
                 else
                 {
                   this.FetchMyExchange()
                 }

           }
           else if(this.state.selectedIndex==1)
           {

                   if(this.state.searchText=='')
                   {
                       this.setState({ _visible: false })
                       commonFunctions.message('Please enter some text')
                   }
                   else
                   {
                      this.FetchRequestExchange()
                  }
           }

           else
           {
                 if(this.state.searchText=='')
                 {
                     this.setState({ _visible: false })
                     commonFunctions.message('Please enter some text')
                 }
                 else
                 {
                    this.GetMyAcceptedExchangeProductList()
                }
          }
        }
      }
   }

   _onRefresh() {
     if(_that.state.isConnected){
       this.setState({refreshing: true});
       if(this.state.selectedIndex==0){
         var arrValue = [];
         this.setState({dataSource:ds.cloneWithRows(arrValue)})
         this.FetchMyExchange()


       }
       else if(this.state.selectedIndex==1)
       {
             var arrValue = [];
             this.setState({dataSource:ds.cloneWithRows(arrValue)})

             this.FetchRequestExchange()
       }
       else
       {
               var arrValue = [];
               this.setState({dataSource:ds.cloneWithRows(arrValue)})
               this.GetMyAcceptedExchangeProductList()
       }
     }
   }

   handleIndexChange=(index)=>{

     if(_that.state.isConnected){

     if(index==0){
       var arrValue = [];
       this.setState({dataSource:ds.cloneWithRows(arrValue)})
        this.setState({isDataAvailable :true})
       this.FetchMyExchange()



     }
     else if(index==1)
     {
             var arrValue = [];
             this.setState({dataSource:ds.cloneWithRows(arrValue)})
             this.setState({isDataAvailable :true})
             this.FetchRequestExchange()

      }

     else{

                   var arrValue = [];
                   this.setState({dataSource:ds.cloneWithRows(arrValue)})
                    this.setState({isDataAvailable :true})
                   this.GetMyAcceptedExchangeProductList()

       }
     }
   }


    render() {


        return (
            <KeyboardAwareView animated={true}>
           <SafeAreaView style={{ flex: 1}}>
           <View style={{flex: 1, backgroundColor: AppColors.white, }}>
            <Image source={bckimg} style={{ width: AppSizes.screen.width, height: AppSizes.screen.height, position: 'absolute', top: 0, left: 0 }} />

          <Header text='Exchange Products' navigation = {this.props.navigation}/>
        <View style = {{alignSelf:'center',marginTop:170,width:AppSizes.screen.width-10,borderRadius:10,}}>
                    <SegmentedControlTab tabsContainerStyle={styles.tabsContainerStyle}
                    borderRadius={5}
                        tabStyle={styles.tabStyle}
                        tabTextStyle={styles.tabTextStyle}
                        activeTabStyle={styles.activeTabStyle}
                        activeTabTextStyle={styles.activeTabTextStyle}
                        selectedIndex={this.state.selectedIndex}
                        values={['Invites', 'Requests','Accepted']}
                        onTabPress={(index)=>{this.setState({selectedIndex:index}),this.handleIndexChange(index)}}
                      />
                  </View>

                  <View style = {{flexDirection:'row'}}>
                    <SearchBar
                        onSearchChange={(searchText) => this._searchProducts(searchText)}
                        height={50}
                        onFocus={() => console.log('On Focus')}
                        onBlur={() => console.log('On Blur')}
                        placeholder={'Search'}
                        autoCorrect={false}
                        inputStyle = {{marginTop:10,width:AppSizes.screen.width-60}}
                        returnKeyType={'done'}
                      />

                      <TouchableOpacity style = {{marginTop:15,height:49,width:40,justifyContent:'center',backgroundColor:'#ddd',marginRight:10}} onPress = {this._searchIconClicked}>
                         <Image source = {require('../themes/Images/search_icon.png')} style = {{width:25,height:25,alignSelf:'center',resizeMode:'contain'}}/>
                      </TouchableOpacity>

                    </View>

                  <ScrollView refreshControl={
                                   <RefreshControl
                                     refreshing={this.state.refreshing}
                                       onRefresh={this._onRefresh.bind(this)}
                                   />
                             }>
               {
                (this.state.isDataAvailable==true)?
                  <ListView
                      contentContainerStyle={[AppStyles.filemanager_list]}
                      dataSource={this.state.dataSource}
                      renderRow={(data, rowID) => this.renderRow(data, rowID)}
                      enableEmptySections={true}
                      style = {{marginTop:10}}

                  />:<NoRecordFound marginTopSpace = {AppSizes.screen.height/5}></NoRecordFound>
              }
                  </ScrollView>



                <Spinner visible={this.state._visible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />

            </View>
            </SafeAreaView>
            </KeyboardAwareView>
        );
    }
    /**
     * Display Products list  from API's
     */
    renderRow(data, rowID) {

      if(this.state.selectedIndex==0)
        {
          // MyExchange screen Products
          const profile_pic = (data.ExchangeProductImage) ? { uri: data.ExchangeProductImage } : '';
          const time = (data.TimeLeft * 1000)
        return (
          <View>

            <TouchableWithoutFeedback onPress = {()=>{if(_that.state.isConnected){this.goToDetail(data)}}}>
            <View style={[AppStyles.filemanager_list_item,{height:(Platform.OS == "ios"? 270 :220)},{paddingTop : 0},{marginTop : 10}]}>
             <View  onPress = {()=>{}} style = {{}}>


                <View style={[AppStyles.filemanager_folder_view,{paddingTop : 10}]}>
                      <Image
                          source={profile_pic}
                          style={AppStyles.filemanager_folder_icon}
                      />
                </View>



                <View style={[AppStyles.filemanager_folder_view,{paddingTop:10}]}>
                    <Text style={[AppStyles.filemanager_folder_title,{marginTop:2}]} numberOfLines={1}>Title : {data.ExchangeProductTitle} {"\n"}
                    </Text>
                <View style={[AppStyles.filemanager_folder_view,{paddingTop:3}]}>
                      <ShareBtn text="Details" onPress={()=>{if(_that.state.isConnected){this.goToDetail(data)}}} style={[AppStyles.Auctionlist_button,{fontSize : (Platform.OS == "ios")? 11 : 12}, {justifyContent:'center', fontWeight:'bold',backgroundColor: '#f77f83' },{width:100,height:38,paddingTop:10},{alignSelf: 'center'},{marginBottom:20}]} />
                    </View>

                </View>


             </View>
            </View>
             </TouchableWithoutFeedback>
                <TouchableOpacity onPress = {()=>{if(_that.state.isConnected){this.deleteProduct(data.ExchangeProductTitle,data.ExchangeProductID)}}} style={{ width: 30, height: 40, top: 15 ,position : 'absolute' ,alignSelf: "flex-end" }} >
                    <Image source={deleteimg} style ={{position : 'absolute' }} />
                 </TouchableOpacity>

            </View>

        )

      }
      else if(this.state.selectedIndex==1)
        {
          const profile_pic = (data.ProductImage) ? { uri: data.ProductImage } : '';
          const time = (data.TimeLeft * 1000)
        return (
          <View>

            <TouchableWithoutFeedback onPress = {()=>{if(_that.state.isConnected){this.goToRequestDetails(data)}}}>
            <View style={[AppStyles.filemanager_list_item,{height:(Platform.OS == "ios"? 285 :260)},{paddingTop : 0},{marginTop : 10}]}>
             <View  onPress = {()=>{}} style = {{}}>


                <View style={[AppStyles.filemanager_folder_view,{paddingTop : 10}]}>
                    <Image
                        source={profile_pic}
                        style={AppStyles.filemanager_folder_icon}
                    />
                </View>



                <View style={[AppStyles.filemanager_folder_view,{paddingTop:10}]}>
                    <Text style={[AppStyles.filemanager_folder_title,{paddingTop :10}]} numberOfLines={1}>Title : {data.Title} {"\n"}
                    </Text>

                    <Text style={AppStyles.filemanager_folder_title} numberOfLines={2}>Total Requests : {data.TotalExchange} {"\n"}
                    </Text>

                </View>
                <View style={[AppStyles.filemanager_folder_view,{paddingTop:3}]}>
                  <ShareBtn text="Requests" onPress={()=>{if(_that.state.isConnected){this.goToRequestDetails(data)}}} style={[AppStyles.Auctionlist_button,{fontSize : (Platform.OS == "ios")? 11 : 12}, {justifyContent:'center', fontWeight:'bold',backgroundColor: '#f77f83' },{width:100,height:38,paddingTop:10},{alignSelf: 'center'},{marginBottom:20}]} />
                </View>

             </View>
            </View>

            </TouchableWithoutFeedback>

            </View>

          )
        }

      else{

        const profile_pic = (data.ExchangeProductImage) ? { uri: data.ExchangeProductImage } : '';
        const time = (data.TimeLeft * 1000)
      return (
        <View>

          <TouchableWithoutFeedback onPress = {()=>{if(_that.state.isConnected){this.goToDetail(data)}}}>
          <View style={[AppStyles.filemanager_list_item,{height:(Platform.OS == "ios"? 270 :220)},{paddingTop : 0},{marginTop : 10}]}>
           <View  onPress = {()=>{}} style = {{}}>


              <View style={[AppStyles.filemanager_folder_view,{paddingTop : 10}]}>
                    <Image
                        source={profile_pic}
                        style={AppStyles.filemanager_folder_icon}
                    />
              </View>



              <View style={[AppStyles.filemanager_folder_view,{paddingTop:10}]}>
                  <Text style={[AppStyles.filemanager_folder_title,{marginTop:2}]} numberOfLines={1}>Title : {data.ExchangeProductTitle} {"\n"}
                  </Text>
                  <View style={[AppStyles.filemanager_folder_view,{paddingTop:3}]}>
                    <ShareBtn text="Details" onPress={()=>{if(_that.state.isConnected){this.goToDetail(data)}}} style={[AppStyles.Auctionlist_button,{fontSize : (Platform.OS == "ios")? 11 : 12}, {justifyContent:'center', fontWeight:'bold',backgroundColor: '#f77f83' },{width:100,height:38,paddingTop:10},{alignSelf: 'center'},{marginBottom:20}]} />
                  </View>

              </View>


           </View>
          </View>
          </TouchableWithoutFeedback>
              <TouchableOpacity onPress = {()=>{if(_that.state.isConnected){this.deleteProduct(data.ExchangeProductTitle,data.ExchangeProductID)}}} style={{ width: 30, height: 40, top: 15 ,position : 'absolute' ,alignSelf: "flex-end" }} >
                  <Image source={deleteimg} style ={{position : 'absolute' }} />
               </TouchableOpacity>

          </View>

      )
      }

    }
    goToDetail=(data)=>{_that.props.navigation.navigate('ExchangeProductDetails',{ExchangeProductID:data.ExchangeProductID})}

    goToRequestDetails=(data)=>{
      _that.props.navigation.navigate('ExchangeRequests',{data:data, fromWhere :"MyExchange"})
    }
    updateMyAuction=()=>{
    //here the code
    }


      deleteProduct = (ProductName,ExchangeProductID)=> {

        var data = {
          "ExchangeProductID": ExchangeProductID
        }

        if(Platform.OS=="ios")
        {
              setTimeout(()=>(AlertIOS.alert(
              ProductName,
              'Are you sure you want to delete this product',
              [
                {
                  text: 'YES',onPress: () =>{this.PostToApiCalling('POST', 'DeleteExchangeProductByID', Constant.URL_DeleteExchangeProductByID, data)}

                },
                {
                  text: 'NO'
                },

              ]
            )), 100)

            }
            else {
              Alert.alert(
                      ProductName,
                      'Are you sure you want to delete this product',
                      [
                        {
                          text: 'YES',onPress: () =>{this.PostToApiCalling('POST', 'DeleteExchangeProductByID', Constant.URL_DeleteExchangeProductByID, data)}
                        },
                        {
                          text: 'NO'
                        },

                      ],
                      { cancelable: false }
                    )

            }






      }

/**
    Fetches the Requested Exchange Products offer
*/
    FetchRequestExchange=()=>{
      this.setState({ _visible: true })
      var data = {
          "UserID": this.state.UserID,
          "PageNumber": 1,
          "PageSize": 1000,
          "Search":search
      }


      this.PostToApiCalling('POST', 'GetProductWithExchangeProduct', Constant.URL_GetProductWithExchangeProduct, data);
      this.setState({refreshing: false});
    }

    /**
        Fetches the My Exchange Products offer
    */

    FetchMyExchange() {
        this.setState({ _visible: true })
        var data = {
            "UserID": this.state.UserID,
            "PageNumber": 1,
            "PageSize": 1000,
            "Search":search
        }


        this.PostToApiCalling('POST', 'GetMyExchangeProductList', Constant.URL_GetMyExchangeProductList, data);
        this.setState({refreshing: false});

    }

    GetMyAcceptedExchangeProductList(){
      this.setState({ _visible: true })
          var data = {
            "UserID": this.state.UserID,
            "PageNumber": 1,
            "PageSize": 1000,
            "Search":search
          }
          this.PostToApiCalling('POST', 'GetMyAcceptedExchangeProductList', Constant.URL_GetMyAcceptedExchangeProductList, data);
          this.setState({refreshing: false});
    }


    /**
    * API Calling
    */


    PostToApiCalling(method, apiKey, apiUrl, data) {
        new Promise(function (resolve, reject) {
            if (method == 'POST') {
                resolve(WebServices.callWebService(apiUrl, data));
            } else {
                resolve(WebServices.callWebService_GET(apiUrl, data));
            }
        }).then((jsonRes) => {
            if ((!jsonRes) || (jsonRes.ResponseCode == 0)) {
                this.apiSuccessfullResponse(apiKey, jsonRes)
                this.setState({ _visible: false })

            } else {
                commonFunctions.message(jsonRes.ResponseMessage)
                this.setState({ _visible: false })
            }
        }).catch((error) => {
            console.log("ERROR" + error);
        });
    }



    apiSuccessfullResponse(apiKey, jsonRes) {

        if (apiKey == 'GetMyExchangeProductList') {
            var jsonResponse = jsonRes.ResponsePacket;

            if (jsonResponse.length == 0)
            {
                    _that.setState({dataSource:ds.cloneWithRows([])})
                    search=''

                   _that.setState({ _visible: false, })
                   _that.setState({ isDataAvailable: false })

            } else {

              this.setState({ _visible: false, dataSource: ds.cloneWithRows(jsonResponse), })

            }
        }

        else if (apiKey == 'GetProductWithExchangeProduct'){

          var jsonResponse = jsonRes.ResponsePacket;

          if (jsonResponse.length == 0)
          {
            _that.setState({dataSource:ds.cloneWithRows([])})
             search=''


             _that.setState({ isDataAvailable: false })
              _that.setState({ _visible: false })

          } else {

              this.setState({ _visible: false, dataSource: ds.cloneWithRows(jsonResponse), })
          }

        }
        else if(apiKey == 'GetMyAcceptedExchangeProductList')
        {
          var jsonResponse = jsonRes.ResponsePacket;

              if (jsonResponse.length == 0)
              {
                _that.setState({dataSource:ds.cloneWithRows([])})
                 search=''
                _that.setState({ _visible: false });
                _that.setState({ isDataAvailable: false });

              } else {

                this.setState({ _visible: false, dataSource: ds.cloneWithRows(jsonResponse), isDataAvailable: true })


              }
        }
        else if(apiKey == 'DeleteExchangeProductByID'){
          this.handleIndexChange(this.state.selectedIndex)
        }

}


}

export default MyExchange

/*
  Segmented control style
*/
const styles = StyleSheet.create({
          tabsContainerStyle: {
          backgroundColor: 'transparent',
          flexDirection: 'row',

          },
          tabStyle: {
            paddingVertical: 7,
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            borderColor: '#0c8580',
            // borderWidth: 1,
            // backgroundColor: 'white',
            // borderRadius:5,
            backgroundColor: '#89f4ef',
          },
          activeTabStyle: {
          backgroundColor: '#0c8580',
            borderColor: '#0c8580'
          },
          tabTextStyle: {
          color: 'black'
          },
          activeTabTextStyle: {
          color: 'white',
          fontWeight:'bold'
          },
          tabBadgeContainerStyle: {
          borderRadius: 20,
          backgroundColor: 'red',
          paddingLeft: 5,
          paddingRight: 5,
          marginLeft: 5,
          marginBottom: 3,
          },
          activeTabBadgeContainerStyle: {
          backgroundColor: 'white'
          },
          tabBadgeStyle: {
          color: 'white',
          fontSize: 11,
          fontWeight: 'bold',
          },
          activeTabBadgeStyle: {
          color: 'black'
          }
});
