import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Button,
    TouchableWithoutFeedback,
    Keyboard,
    Image,
    TouchableOpacity,
    TextInput,
    ScrollView,
    AsyncStorage,
    BackHandler,
    Alert,
    AlertIOS,
    SafeAreaView,
    StatusBar,
    DeviceEventEmitter,


} from 'react-native';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import Colors from '../themes/color';
import { AppStyles, AppSizes, AppColors } from '../themes/'
import Buttons, { LoginButton, Button2, Header } from "../components/Buttons";
import Spinner from 'react-native-loading-spinner-overlay';
var ImagePicker = require('react-native-image-picker');
import * as commonFunctions from '../utils/CommonFunctions'
var Constant = require('../api/WebInteractor').Constant;
var WebServices = require('../api/WebInteractor').WebServices;
var _that = "";
var key;
var minBidAmountString = " Min Bid Amount"
var maxBidAmountString = " Max Bid Amount"
var homeScreenKey ;
class AddNewProduct extends React.Component {
    constructor(props) {
        super(props)
        var {ProductImage}= (this.props.navigation.state.params.updateData==null)?'':this.props.navigation.state.params.updateData;
        this.state = {
            _title: (this.props.navigation.state.params.updateData==null)?"":this.props.navigation.state.params.updateData.Title,
            _minamount: (this.props.navigation.state.params.updateData==null)?"":this.props.navigation.state.params.updateData.MinBidAmount.toString(),
            _maxamount:(this.props.navigation.state.params.updateData==null)?"":(this.props.navigation.state.params.updateData.MAxBidAmount.toString()==9999999999999999.00)?"":this.props.navigation.state.params.updateData.MAxBidAmount.toString(),
            _duration: (this.props.navigation.state.params.updateData==null)?"":this.props.navigation.state.params.updateData.DurationOfAuctionInDays.toString(),
            _description: (this.props.navigation.state.params.updateData==null)?"":this.props.navigation.state.params.updateData.Description,
            _visible: false,
            avatarSource: (this.props.navigation.state.params.updateData==null)?"":{uri: ProductImage},
            _coreimage: "",
            UserID:"",
            tempMax:"",
            isSaved :false,
            imageHeight : (this.props.navigation.state.params.updateData==null)?0:150,
            isConnected : true,
            isShowSubmitBtn:true,
        }
    }

    componentWillMount() {


        //getting the key if coming from MyAuctionsDetail screen
        key = (this.props.navigation.state.params.updateData==null)?"":this.props.navigation.state.params.Routekey;
        AsyncStorage.getItem('UserData')
            .then((res) => {
                if (res) {
                    var data = JSON.parse(res)
                    if (data.isRemember) {
                        this.setState({
                            UserID: data.UserID,
                            CurrencyName:data.CurrencyName
                        })
                    }
                }
            });
            _that = this
            BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
            DeviceEventEmitter.addListener("isConnected",this.checkConnection.bind(this));
    }

        componentWillUnmount() {


        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
        DeviceEventEmitter.removeListener("isConnected",this.checkConnection.bind(this));
    }
    checkConnection(data){

          this.setState({isConnected : data.isConnected});
    }

    //GO BACK TO previous screen on device back button click


      handleBackButtonClick() {
          _that.props.navigation.goBack(null);
          return true;

      }

    render() {
        const { _title, _minamount, _maxamount, _duration, _description, avatarSource,_coreimage ,CurrencyName} = this.state
        var minBidPlaceholder = CurrencyName+minBidAmountString
        var maxBidPlaceholder = CurrencyName+maxBidAmountString


        return (
            <KeyboardAwareView animated={false}>
          <SafeAreaView style={{ flex: 1}}>

              <ScrollView  style = {{backgroundColor :"white"}} keyboardShouldPersistTaps={'always'} >
                  <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                  <View style={[AppStyles.container, { paddingLeft: AppColors.paddingLeft30, paddingRight: AppColors.paddingRight30 }]}>
                      {
                        (this.props.navigation.state.params.updateData==null)?<Header text='Add New Product' navigation = {this.props.navigation} />:<Header text='Update Product Info' navigation = {this.props.navigation}/>
                      }
                      <View style={[AppStyles.Input_iconcnt, { marginTop: 180 }]}>
                          <View style={AppStyles.Inputtxt}>
                              <TextInput
                                  autoCorrect={false}
                                  underlineColorAndroid="transparent"
                                  returnKeyType={"next"}
                                  blurOnSubmit={false}
                                  autoFocus={false}
                                  placeholder="Title"
                                  placeholderTextColor={AppColors.lightGray_text}
                                  style={AppStyles.InputCnt_txt}
                                  ref="_title"
                                  maxLength = {50}
                                  autoCapitalize = "none"
                                  onChangeText={_title => this.setState({ _title })}
                                  value={_title}
                                  onSubmitEditing={() => { this.refs._minamount.focus(); }}
                              />
                          </View>
                      </View>
                      <View style={AppStyles.Input_iconcnt}>
                          <View style={AppStyles.Inputtxt}>
                              <TextInput
                                  autoCorrect={false}
                                  underlineColorAndroid="transparent"
                                  returnKeyType={"next"}
                                  blurOnSubmit={false}
                                  keyboardType={'numeric'}
                                  autoFocus={false}
                                  placeholder={minBidPlaceholder}
                                  placeholderTextColor={AppColors.lightGray_text}
                                  style={AppStyles.InputCnt_txt}
                                  ref="_minamount"
                                  maxLength={7}
                                  onChangeText={_minamount => this.setState({ _minamount })}
                                  value={_minamount}
                                  onSubmitEditing={() => { this.refs._maxamount.focus(); }}
                              />
                          </View>
                      </View>
                      <View style={AppStyles.Input_iconcnt}>
                          <View style={AppStyles.Inputtxt}>
                              <TextInput
                                  autoCorrect={false}
                                  underlineColorAndroid="transparent"
                                  returnKeyType={"next"}
                                  blurOnSubmit={false}
                                  keyboardType={'numeric'}
                                  autoFocus={false}
                                  placeholder={maxBidPlaceholder}
                                  placeholderTextColor={AppColors.lightGray_text}
                                  style={AppStyles.InputCnt_txt}
                                  ref="_maxamount"
                                  maxLength={7}
                                  onChangeText={_maxamount => this.setState({ _maxamount })}
                                  value={_maxamount}
                                  onSubmitEditing={() => { this.refs._duration.focus(); }}
                              />
                          </View>
                      </View>
                      <View style={AppStyles.Input_iconcnt}>
                          <View style={AppStyles.Inputtxt}>
                              <TextInput
                                  autoCorrect={false}
                                  underlineColorAndroid="transparent"
                                  returnKeyType={"next"}
                                  blurOnSubmit={false}
                                  keyboardType={'numeric'}
                                  autoFocus={false}
                                  placeholder="Duration of Auction in days"
                                  placeholderTextColor={AppColors.lightGray_text}
                                  style={AppStyles.InputCnt_txt}
                                  ref="_duration"
                                  maxLength={1}
                                  onChangeText={_duration => this.setState({ _duration })}
                                  value={_duration}
                                  onSubmitEditing={() => { this.refs._description.focus(); }}
                              />
                          </View>
                      </View>
                          <View style={{
                          borderColor: Colors.lightGray_text,
                          borderWidth: 1,
                          borderRadius: 10,
                         marginTop: Colors.marginTop10}}>

                              <TextInput
                                  autoCorrect={false}
                                  underlineColorAndroid="transparent"
                                  returnKeyType={"done"}
                                  autoFocus={false}
                                  placeholder="Description"
                                  placeholderTextColor={AppColors.lightGray_text}
                                  style={{fontSize : 22,marginLeft : Colors.marginLeft10,height : 100
                                  }}
                                  ref="_description"
                                  multiline={true}
                                  blurOnSubmit = {true}
                                  numberOfLines={3}
                                  maxLength = {50}
                                  autoCapitalize = "none"
                                  onChangeText={_description => this.setState({ _description })}
                                  value={_description}

                              />

                      </View>
                      <LoginButton text={"Select Image"} onPress={this.PickerImage.bind()} style={AppStyles.green_round_button} />
                      <Image source={avatarSource} style={[AppStyles.login_logo,{height : this.state.imageHeight},{marginTop : 30}]} />
                    {
                      (this.state.isShowSubmitBtn)?
                      <LoginButton text={"Save"}  disabled={this.state.isSaved} onPress={()=>{if(this.state.isConnected){this.Save()}}} style={[AppStyles.green_round_button,{marginBottom : 40}]} />:null
                    }
                      <Spinner visible={this.state._visible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />
                  </View>
                  </TouchableWithoutFeedback>
              </ScrollView>

          </SafeAreaView>
          </KeyboardAwareView>
        );
    }
    PickerImage = () => {
        // More info on all the options is below in the README...just some common use cases shown here
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
        };

        /**
                 * The first arg is the options object for customization (it can also be null or omitted for default options),
                 * The second arg is the callback which sends object: response (more info below in README)
                 */
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                // let source = { uri: response.uri };

                // You can also display the image using data:
                let source = { uri: 'data:image/png;base64,' + response.data };
                this.setState({
                    avatarSource: source,
                    _coreimage: response.data
                });
                if(this.state.avatarSource != null)
                {
                        this.setState({imageHeight : 150});
                }
            }
        });
    }

    /*
        Adds the new Product
     */
    Save = () => {
      //set the save button state disable
      this.setState({isSaved:true});
        var data;

        const { _coreimage, _email, _title, _minamount, _maxamount, _duration, _description, avatarSource } = this.state
        // Loader show
        this.setState({ _visible: true })

        /**
         *  Form  Validations
         */


        if (_title == "") {
            this.setState({ _visible: false });
            commonFunctions.message("Please Enter Title of item.")
        } else if (commonFunctions.validateName(_title)) {
            this.setState({ _visible: false });
            commonFunctions.message("Please enter Correct Title of item.")
        }else if (_minamount == "") {
            this.setState({ _visible: false });
            commonFunctions.message("Please enter minimum bid amount for item.")
        } /**else if (_maxamount == "") {
            this.setState({ _visible: false });
            commonFunctions.message("Please enter maximum bid amount for item")
        }  */
        else if (!commonFunctions.validateAmount(_minamount)) {
            this.setState({ _visible: false });
            commonFunctions.message("Please enter correct minimum bid amount for item.")
        }
        else if(_maxamount!=""){

          if (!commonFunctions.validateAmount(_maxamount)) {
              this.setState({ _visible: false });
              commonFunctions.message("Please enter correct maximum bid amount for item.")
          }
          else if(parseFloat(_maxamount)<parseFloat(_minamount)){
            this.setState({ _visible: false });
            commonFunctions.message("Max Bid Amount is less than Min Bid Amount.")

          } else if(parseFloat(_maxamount)==parseFloat(_minamount)){
             this.setState({ _visible: false });
             commonFunctions.message("Max Bid Amount should not equal to Min Bid Amount.")
           }

           else {
             this.checkFurtherValidationAndApiCall();
           }
         }else {
          this. checkFurtherValidationAndApiCall();
         }


    }

  //Validation Checking on Screen and calling AddUpdateProduct API

    checkFurtherValidationAndApiCall(){

      var data;
      this.setState({isSaved : true});
      const { _coreimage, _email, _title, _minamount, _maxamount, _duration, _description, avatarSource } = this.state
       if (_duration == "") {
          this.setState({ _visible: false });
          commonFunctions.message("Please enter duration in days for item.")
      }
      else if (_duration == 0) {
         this.setState({ _visible: false });
         commonFunctions.message("Duration should be greater than zero.")
       }else if (parseFloat(_minamount) <= 0.0) {
       this.setState({ _visible: false });
       commonFunctions.message("Min Bid Amount should be greater than zero.")
      }else if (_description == "") {
          this.setState({ _visible: false });
          commonFunctions.message("Please enter short description of item.")
      } else if(this.props.navigation.state.params.updateData==null) {
            if (_coreimage == "") {
              this.setState({ _visible: false });
              commonFunctions.message("Please select an image for the item.")
          }else{
            var tempMax="";
            if(_maxamount==""){
              tempMax="9999999999999999.00"
            }else{
              tempMax=_maxamount
            }

              data = {
                 "UserID": this.state.UserID,
                 "Title": _title,
                 "MinBidAmount": _minamount,
                 "MAxBidAmount": tempMax,
                 "DurationOfAuctionInDays": _duration,
                 "Description": _description,
                 "ProductImage": _coreimage,
                 "ProductCategoryID": 1,
             }
             console.log('add new product is '+JSON.stringify(data));
             this.PostToApiCalling('POST', 'AddUpdateProduct', Constant.URL_AddUpdateProduct, data);
             this.setState({
               isShowSubmitBtn:false
             })
          }
    }
    else {

      if(this.props.navigation.state.params.updateData==null){

      }else{
        var tempMax="";
        if(_maxamount==""){
          tempMax="9999999999999999.00"
        }else{
          tempMax=_maxamount
        }
        //request for AddUpdateproduct API
       data = {
            "ID":this.props.navigation.state.params.updateData.ID,
            "UserID": this.state.UserID,
            "Title": _title,
            "MinBidAmount": _minamount,
            "MAxBidAmount": tempMax,
            "DurationOfAuctionInDays": _duration,
            "Description": _description,
            "ProductImage": _coreimage,
            "ProductCategoryID": 1,
        }

      }

 console.log('update new product is '+JSON.stringify(data));
          this.PostToApiCalling('POST', 'AddUpdateProduct', Constant.URL_AddUpdateProduct, data);

      }

    }

    emptyFields = () =>{

        this.setState({_coreimage:"",_title:"",_minamount:"",_maxamount:"",_duration:"",_description:"",avatarSource:""});
    }

    /**
     * API Calling
     */


    PostToApiCalling(method, apiKey, apiUrl, data) {
        new Promise(function (resolve, reject) {
            if (method == 'POST') {
                resolve(WebServices.callWebService(apiUrl, data));
            } else {
                resolve(WebServices.callWebService_GET(apiUrl, data));
            }
        }).then((jsonRes) => {
            this.setState({ _visible: false })
            if ((!jsonRes) || (jsonRes.ResponseCode == 0)) {
                this.apiSuccessfullResponse(apiKey, jsonRes)
                this.setState({ _visible: false,  isShowSubmitBtn:true })

            } else {

               this.setState({ _visible: false,  isShowSubmitBtn:true })
                commonFunctions.message(jsonRes.ResponseMessage)
                this.setState({ _visible: false })
            }
        }).catch((error) => {
            console.log("ERROR" + error);
        });
    }

    apiSuccessfullResponse(apiKey, jsonRes) {
      const { _coreimage, _email, _title, _minamount, _maxamount, _duration, _description, avatarSource } = this.state


          var productId = jsonRes.ResponsePacket.ID;
        if (apiKey == 'AddUpdateProduct') {
            var jsonResponse = jsonRes.ResponsePacket;
            console.log(jsonResponse)
            if (jsonRes.ResponseCode = "0") {
              this.setState({isSaved : true})
              if(Platform.OS == "ios")
              {
                _that.setState({ _visible: false })
                      setTimeout(()=>(AlertIOS.alert(
                    '',
                    jsonRes.ResponseMessage,
                    [
                      {

                        text: 'OK',
                        onPress: () => {_that.emptyFields();this.props.navigation.navigate("ShareAuction",{Routekey : this.props.navigation.state.key,ProductID:productId , fromWhere:"AddNewProduct",UserId : this.state.UserID})},

                      },

                    ]
                  )), 500)
                  _that.setState({ _visible: false })
              }
              else {
                Alert.alert(
                        '',
                        jsonRes.ResponseMessage,
                        [
                          {text: 'OK', onPress: () =>  { _that.emptyFields();this.props.navigation.navigate("ShareAuction", {Routekey : this.props.navigation.state.key,ProductID:productId , fromWhere:"AddNewProduct",UserId : this.state.UserID})},}

                        ],
                        { cancelable: false }
                      );

                  _that.setState({ _visible: false })
              }

            } else {

              if(Platform.OS == "ios")
              {
                _that.setState({ _visible: false })
                      setTimeout(()=>(AlertIOS.alert(
                    '',
                    jsonRes.ResponseMessage,
                    [
                      {
                        text: 'OK',
                        onPress: () => {_that.props.navigation.goBack(),_that.props.navigation.state.params.refreshingDetail()},

                      },

                    ]
                  )), 500)
                  _that.setState({ _visible: false })
              }
              else {
                Alert.alert(
                        '',
                        jsonRes.ResponseMessage,
                        [
                          {text: 'OK', onPress: () => {_that.setState({ _visible: false });_that.props.navigation.goBack()}},

                        ],
                        { cancelable: false }
                      );

                  _that.setState({ _visible: false })
              }

            }
        }
        //set the save button state enable
         this.setState({isSaved:false});

  }

}

export default AddNewProduct
