import React, { Component } from 'react';
import { DrawerNavigator, StackNavigator } from 'react-navigation';
import DatePicker from 'react-native-datepicker';
import OpenPdf from "./OpenPDF";

import {
    Platform,
    StyleSheet,
    Text,
    View,
    Button,
    TouchableWithoutFeedback,
    Keyboard,
    Alert,
    Image,
    TextInput,
    ScrollView,
    AsyncStorage,
    BackHandler,
    TouchableOpacity,
    SafeAreaView,
    AlertIOS,
    DeviceEventEmitter,
    Dimensions,
} from 'react-native';
import CountryPicker, { getAllCountries } from 'react-native-country-picker-modal';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import { AppStyles, AppSizes, AppColors } from '../themes/'
import Buttons, { RegisterButton, Button2, Header, webview } from "../components/Buttons";
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';

const Logo = require('../themes/Images/logo.png');
import Spinner from 'react-native-loading-spinner-overlay';
import * as commonFunctions from '../utils/CommonFunctions'
var Constant = require('../api/WebInteractor').Constant;
var WebServices = require('../api/WebInteractor').WebServices;
import CheckBox from 'react-native-check-box'
var _that = "";
const uncheck = require("../themes/Images/uncheck.png");
const check_select = require("../themes/Images/check.png");
const countryList = ['GB', 'US'];
var radio_props = [
    { label: 'UK        ', value: '£', color: '#ffffff' },
    { label: 'USA', value: '$' }
];
var currentDate = "";



var unixTime = "";
class Registration extends React.Component {
    constructor(props) {
        super(props)
        var m_names = new Array("Jan", "Feb", "Mar",
        "Apr", "May", "Jun", "Jul", "Aug", "Sep",
        "Oct", "Nov", "Dec");
        var tdate = new Date();
           var dd = tdate.getDate(); //yields day
           var MM = tdate.getMonth(); //yields month
           var yyyy = tdate.getFullYear(); //yields year
           //var todayDate= yyyy + "-" +( MM+1) + "-" + dd;
           var todayDate= dd+ "-"+ m_names[MM]+"-"+yyyy
           currentDate = todayDate.toString();


        const userCountryData = getAllCountries()
            .filter((country) => countryList.includes(country.cca2))
            .filter((country) => country.cca2 === 'GB').pop();
        let callingCode = null;
        let cca2 = 'GB';
        if (!cca2 || !userCountryData) {
            cca2 = 'GB';
            callingCode = '44';
        } else {
            callingCode = userCountryData.callingCode;
        }
        this.state = {
            full_name: '',
            mobile_number: '',
            Password: '',
            confirm_password: '',
            email_id: '',
            _visible: false,
            avatarSource: '',
            _coreimage: '',
            UserID: '',
            isChecked: false,
            currency: '£',
            cca2: 'GB',
            callingCode: '44',
            selectedIndex:'0',
            isConnected : true,
            date:"",
            timeStamp : "",
            tdyDate : currentDate,
        },
        _that = this;
    }

    componentWillMount() {

        //  commonFunctions.checkConnection();
        AsyncStorage.getItem('UserData')
            .then((res) => {
                if (res) {
                    var data = JSON.parse(res)
                    if (data.isRemember) {
                        this.setState({
                            UserID: data.UserID
                        })
                    }
                }
            });
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
        DeviceEventEmitter.addListener("isConnected",this.checkConnection.bind(this));
    }
    componentWillUnmount() {

        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
        DeviceEventEmitter.removeListener("isConnected",this.checkConnection.bind(this));
    }

    //GO BACK TO previous screen on device back button click
    handleBackButtonClick() {
        _that.props.navigation.goBack();
        return true;
    }

    //Checking the Internet Connection
    checkConnection(data){

          this.setState({isConnected : data.isConnected});
    }

    render() {

        const { full_name, mobile_number, Password, confirm_password, email_id, avatarSource, currency, isChecked,selectedIndex } = this.state
        return (
          <KeyboardAwareView animated={true}>
              <SafeAreaView style={{ flex: 1 }}>

                        <ScrollView style={{ backgroundColor: '#fff' }} keyboardShouldPersistTaps="always">
                            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>

                            <View style={[AppStyles.container, { paddingLeft: AppColors.paddingLeft30, paddingRight: AppColors.paddingRight30 }]}>
                                <Header text='Registration' navigation = {this.props.navigation}/>
                                <View style={[AppStyles.Input_iconcnt, { marginTop: 180 }]}>
                                    <View style={AppStyles.Inputtxt}>
                                        <TextInput
                                            autoCorrect={false}
                                            underlineColorAndroid="transparent"
                                            returnKeyType={"next"}
                                            blurOnSubmit={false}
                                            autoFocus={false}
                                            placeholder="Full Name"
                                            autoCapitalize="none"
                                            placeholderTextColor={AppColors.lightGray_text}
                                            style={AppStyles.InputCnt_txt}
                                            ref="full_name"
                                            maxLength={50}
                                            onChangeText={full_name => this.setState({ full_name })}
                                            value={full_name}
                                            onSubmitEditing={() => this.refs.mobile_number.focus()}
                                        />
                                    </View>
                                </View>


                                <View style={AppStyles.Input_iconcnt}>
                                  <View style={{flexDirection:'row',marginLeft:10,}}>
                                    <View style={{marginTop:5}}>
                                    <CountryPicker
                                        countryList={countryList}
                                        onChange={(value) => _that.onChange(value)}
                                        cca2={this.state.cca2}
                                        translation='eng'
                                        />
                                      </View>
                                      <View style={{ marginLeft: 3, width: 300 }}>
                                          <TextInput
                                              autoCorrect={false}
                                              underlineColorAndroid="transparent"
                                              returnKeyType={"next"}
                                              blurOnSubmit={false}
                                              keyboardType={'phone-pad'}
                                              autoFocus={false}
                                              placeholder="Mobile Number"
                                              placeholderTextColor={AppColors.lightGray_text}
                                              ref="mobile_number"
                                              maxLength={15}
                                              onChangeText={mobile_number => this.setState({ mobile_number })}
                                              value={mobile_number}
                                              onSubmitEditing={() => this.refs.password.focus()}
                                          />
                                        </View>
                                      </View>
                                </View>



                                <View style={AppStyles.Input_iconcnt}>
                                    <View style={AppStyles.Inputtxt}>
                                        <TextInput
                                            autoCorrect={false}
                                            underlineColorAndroid="transparent"
                                            returnKeyType={"next"}
                                            blurOnSubmit={false}
                                            autoFocus={false}
                                            placeholder="Password"
                                            maxLength={50}
                                            autoCapitalize="none"
                                            secureTextEntry={true}
                                            placeholderTextColor={AppColors.lightGray_text}
                                            style={AppStyles.InputCnt_txt}
                                            ref="password"
                                            onChangeText={Password => this.setState({ Password })}
                                            value={Password}
                                            onSubmitEditing={() => this.refs.confirm_password.focus()}
                                        />
                                    </View>
                                </View>

                                <View style={AppStyles.Input_iconcnt}>
                                    <View style={AppStyles.Inputtxt}>
                                        <TextInput
                                            autoCorrect={false}
                                            underlineColorAndroid="transparent"
                                            returnKeyType={"next"}
                                            maxLength={50}
                                            blurOnSubmit={false}
                                            autoFocus={false}
                                            secureTextEntry={true}
                                            placeholder="Confirm Password"
                                            autoCapitalize="none"
                                            placeholderTextColor={AppColors.lightGray_text}
                                            style={AppStyles.InputCnt_txt}
                                            ref="confirm_password"
                                            onChangeText={confirm_password => this.setState({ confirm_password })}
                                            value={confirm_password}
                                            onSubmitEditing={() => this.refs.email_id.focus()}

                                        />
                                    </View>
                                </View>

                                <View style={AppStyles.Input_iconcnt}>
                                    <View style={AppStyles.Inputtxt}>
                                        <TextInput
                                            autoCorrect={false}
                                            underlineColorAndroid="transparent"
                                            returnKeyType={"done"}
                                            autoFocus={false}
                                            placeholder="Email ID"
                                            maxLength={50}
                                            autoCapitalize="none"
                                            placeholderTextColor={AppColors.lightGray_text}
                                            style={[AppStyles.InputCnt_txt, { height: 50, paddingBottom: Platform.OS == "ios" ? 12 : 20 }]}
                                            ref="email_id"
                                            keyboardType={'email-address'}
                                            multiline={false}
                                            onChangeText={email_id => this.setState({ email_id })}
                                            value={email_id}
                                        />
                                    </View>
                                </View>
                                <View style = {{marginTop : 20, flexDirection : "row"}}>
                                  <Text style = {{marginTop : 10, marginRight: 10, fontSize : 17}}> DOB: </Text>
                                  <DatePicker
                                           style={{width: 200, borderRadius : 10}}
                                           date={this.state.date}
                                           mode="date"
                                           placeholder="select date"
                                           format="DD-MMM-YYYY"
                                           minDate="01-Jan-1900"
                                           maxDate={this.state.tdyDate}
                                           confirmBtnText="Confirm"
                                           cancelBtnText="Cancel"
                                           customStyles={{
                                             dateIcon: {
                                               position: 'absolute',
                                               left: 0,
                                               top: 4,
                                               marginLeft: 0
                                             },
                                             dateInput: {
                                               marginLeft: 36,
                                               borderRadius : 10,
                                               backgroundColor :"#DCDCDC"
                                             }
                                             // ... You can check the source to find the other keys.
                                           }}
                                           onDateChange={(date) => {this.setState({date: date})}}
                                          />

                                </View>

                                <View style={[AppStyles.Input_iconcnt, { borderWidth: 0 }]}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 15 }}>Choose your country</Text>
                                </View>
                                <RadioGroup
                                    size={24}
                                    selectedIndex={selectedIndex}
                                      onSelect = {(index, value) => this.setState({currency:value})}>

                                      <RadioButton value={'£'} >
                                        <Text>UK</Text>
                                      </RadioButton>

                                      <RadioButton value={'$'}>
                                        <Text>USA</Text>
                                      </RadioButton>

                                </RadioGroup>




                                <View style={[AppStyles.Input_iconcnt, { borderWidth: 0 }]}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 15 }}> Currency :  {currency}</Text>
                                </View>

                                <View style={[{ borderWidth: 0, height: 90, flexDirection: 'row', justifyContent: 'space-between', marginTop: 25 }]}>

                                    <View style={{}}>
                                        <CheckBox
                                            ischecked={isChecked}
                                            style={{ alignSelf: 'flex-start' }}
                                            onClick={(checked) => { this.changeCheckedState(checked) }}
                                        />
                                    </View>

                                    <View style={{ width: AppSizes.screen.width - 90 }}>
                                        <Text numberOfLines={6} style={{ fontSize: 10 }}>By completing this registration form you are agreeing to our Terms and Conditions and Privacy policy which can be found on our website at <Text style={{ fontSize: 10, borderBottomWidth: 1, color: 'blue' }} onPress={this._showWebView}> www.contactbids.com</Text> Please tick here to confirm you have read, understood and agreed with these (tick box)</Text>
                                    </View>

                                </View>

                                <RegisterButton text={"Register"} onPress={()=>{if(_that.state.isConnected){this.Save()}}} style={[AppStyles.green_round_button, { borderRadius: 10, marginBottom: 80, overflow: (Platform.OS == "ios" ? 'hidden' : null) }]} />
                                <Spinner visible={this.state._visible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />

                            </View>


                      </TouchableWithoutFeedback>
                      </ScrollView>
              </SafeAreaView>
          </KeyboardAwareView>

        );
    }


    /*
        show webView
     */

    _showWebView = () => {
      if(_that.state.isConnected){
            this.props.navigation.navigate('webview', { url: 'http://www.contactbids.com/#' })
      }
    }


    openPDF = ()=>{
          this.props.navigation.navigate("OpenPDF")

    }

    /*
        Changing state of CheckBox  for terms and condition
     */

    changeCheckedState(checked) {
        _that.setState({
            isChecked: !_that.state.isChecked,
        });
    }
    onChange(value){
      _that.setState({
          cca2: value.cca2,
          callingCode: value.callingCode,
  });
                value.cca2=='GB'?this.setState({selectedIndex:'0',currency:'£'}):this.setState({selectedIndex:'1',currency:'$'})

    }

    Save = () => {

        const { full_name, mobile_number, Password, confirm_password, email_id, selectedIndex,avatarSource, currency, isChecked, callingCode } = this.state
        // Loader show
        this.setState({ _visible: true })


        var date = this.state.date.toString();
        const dateTime = new Date(date).getTime();
        var timeStamp = (Math.floor(dateTime / 1000));
        unixTime = timeStamp;
        //_that.setState({timeStamp : timeStamp});
        /**
         *  Form  Validations
         */

        if (full_name == "") {
            this.setState({ _visible: false });
            commonFunctions.message("Please enter Name.")

        } else if (commonFunctions.validateName(full_name)) {
            this.setState({ _visible: false });
            commonFunctions.message("Please enter Correct Name.")
        } else if (mobile_number == "") {
            _that.setState({ _visible: false });
            commonFunctions.message("Please enter Mobile Number.")
        } else if (commonFunctions.validatePhoneNum(mobile_number, selectedIndex)==true) {
              this.setState({ _visible: false });
            commonFunctions.message("Please enter correct Mobile Number.")
        }
        else if (commonFunctions.validatePhoneNum(mobile_number, selectedIndex)=="UK") {
              this.setState({ _visible: false });
            commonFunctions.message("Mobile Number should not include country code (44).")
        }
        else if (commonFunctions.validatePhoneNum(mobile_number, selectedIndex)=="USA") {
              this.setState({ _visible: false });
            commonFunctions.message("Mobile Number should not include country code (1).")
        }
       else if (Password == "") {
            this.setState({ _visible: false });
            commonFunctions.message("Please enter password.")
        } else if (confirm_password == "") {
            this.setState({ _visible: false });
            commonFunctions.message("Please enter confirm password.")
        }
          else if (!commonFunctions.validatePassword(Password)) {
             this.setState({ _visible: false });
             commonFunctions.message("Password must be at least 6 characters.")
          }
         else if (!commonFunctions.validatePassword(confirm_password)) {
            this.setState({ _visible: false });
            commonFunctions.message("Confirm Password must be at least 6 characters.")
          }
          else if (Password != confirm_password) {
              this.setState({ _visible: false });
              commonFunctions.message("Confirm Password does not match.")
          }
         else if (email_id == "") {
            this.setState({ _visible: false });
            commonFunctions.message("Please enter Email.")
        }
        else if (!commonFunctions.validateEmail(email_id)) {
            this.setState({ _visible: false });
            Alert.alert("Enter valid Email.");
        }

        // else if (!commonFunctions.validatePhoneNum(mobile_number)) {
        //     this.setState({ _visible: false });
        //     commonFunctions.message("Please enter correct Mobile Number.")
        // }
        else if (currency == "") {
            this.setState({ _visible: false });
            commonFunctions.message("Please choose your country.")
        }
        else if ((currency =='$'&& selectedIndex==0) || (currency =='£'&& selectedIndex==1)) {
            this.setState({ _visible: false });
            commonFunctions.message("Please select appropriate country code.")
        }

        else if (isChecked == false) {
            this.setState({ _visible: false });
            commonFunctions.message("Please agree the terms and conditions.")
        }

        else {
          let {date} = this.state
          let selectedDate  = date
          if(date == currentDate)
          {
                selectedDate = ""
          }
            var data = {
                "Name": full_name,
                "PhoneNo": mobile_number,
                "Password": Password,
                "Email": email_id,
                "CountryName": (currency == '£') ? 'UK' : 'USA',
                "CurrencyName": currency,
                "CountryCode": callingCode,
                 "DOB" : selectedDate,

            }
            console.log('register request data is ' + JSON.stringify(data));
            this.PostToApiCalling('POST', 'MobileRegistration', Constant.URL_RegisterUser, data);

        }

    }
    /**
     * API Calling
     */


    PostToApiCalling(method, apiKey, apiUrl, data) {
        new Promise(function (resolve, reject) {
            if (method == 'POST') {
                resolve(WebServices.callWebService(apiUrl, data));
            } else {
                resolve(WebServices.callWebService_GET(apiUrl, data));
            }
        }).then((jsonRes) => {
            _that.setState({ _visible: false })
            if ((jsonRes.ResponseCode == 0)) {
                if (Platform.OS == "ios") {
                    _that.setState({ _visible: false })
                    setTimeout(() => (AlertIOS.alert(
                        '',
                        jsonRes.ResponseMessage,
                        [
                            {
                                text: 'OK',
                                onPress: () => { _that.props.navigation.goBack() },

                            },

                        ]
                    )), 500)
                    _that.setState({ _visible: false })
                }
                else {
                    Alert.alert(
                        '',
                        jsonRes.ResponseMessage,
                        [
                            { text: 'OK', onPress: () => { _that.setState({ _visible: false }); _that.props.navigation.goBack() } },

                        ],
                        { cancelable: false }
                    );

                    _that.setState({ _visible: false })
                }


            }
            else if (jsonRes.ResponseCode == 2) {

                if (Platform.OS == "ios") {
                    _that.setState({ _visible: false })
                    setTimeout(() => (AlertIOS.alert(
                        '',
                        jsonRes.ResponseMessage,
                        [
                            {
                                text: 'OK',
                                onPress: () => { },

                            },

                        ]
                    )), 500)
                    _that.setState({ _visible: false })
                }
                else {
                    Alert.alert(
                        '',
                        jsonRes.ResponseMessage,
                        [
                            { text: 'OK', onPress: () => { _that.setState({ _visible: false }) } },

                        ],
                        { cancelable: false }
                    );

                    _that.setState({ _visible: false })
                }



            } else {
                if (Platform.OS == "ios") {
                    _that.setState({ _visible: false })
                    setTimeout(() => (AlertIOS.alert(
                        '',
                        jsonRes.ResponseMessage,
                        [
                            {
                                text: 'OK',
                                onPress: () => { },

                            },

                        ]
                    )), 500)
                    _that.setState({ _visible: false })
                }
                else {
                    Alert.alert(
                        '',
                        jsonRes.ResponseMessage,
                        [
                            { text: 'OK', onPress: () => { _that.setState({ _visible: false }) } },

                        ],
                        { cancelable: false }
                    );

                    _that.setState({ _visible: false })
                }


            }

        }).catch((error) => {
            console.log("ERROR" + error);
        });
    }
}



export default Registration
