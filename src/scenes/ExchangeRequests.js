import React, { Component } from 'react';

import { AppStyles, AppSizes, AppColors } from '../themes/'
import Buttons, { LoginButton, Button2, Header, ShareBtn, BidderListBtn, Countdwn ,NoRecordFound} from "../components/Buttons";
import Spinner from 'react-native-loading-spinner-overlay';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Button,
    ListView,
    Image,
    ScrollView,
    AsyncStorage,
    BackHandler,
    Alert,
    AlertIOS,
    TouchableOpacity,
    RefreshControl,
    SafeAreaView,
    Modal,
    TextInput,
    TouchableWithoutFeedback,
    DeviceEventEmitter,

} from 'react-native';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import SearchBar from 'react-native-material-design-searchbar';
var folderIcon = require('../themes/Images/folder.png')
const bckimg = require('../themes/Images/bg-bottom.png')
import * as commonFunctions from '../utils/CommonFunctions'
var Constant = require('../api/WebInteractor').Constant;
var WebServices = require('../api/WebInteractor').WebServices;

var _that = '';
var data = [];
var ds;
var search = '';
class ExchangeRequests extends React.Component<{}>{

        constructor(props){
          super(props)
          ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
          _that=this
          //Checking from which screen we are coming either MyExchange OR details
          if(this.props.navigation.state.params.fromWhere == "MyExchange")
          {

                var {ID} = this.props.navigation.state.params.data
          }
          else if(this.props.navigation.state.params.fromWhere == "Details"){
                var {ID} = this.props.navigation.state.params
          }

          this.state={
            UserID:'',
            dataSource: ds.cloneWithRows(data),
            _visible : false,
            searchText:'',
            ProductID:ID,
            refreshing:false,
            isConnected : true,
            isDataAvailable:true,//intially its show LISTVIEW with zero Items
          }

        }




        componentWillMount(){


          AsyncStorage.getItem('UserData')
              .then((res) => {
                  if (res) {
                      var data = JSON.parse(res)
                      if (data.isRemember) {
                          this.setState({
                              UserID: data.UserID
                          })
                          this.FetchExchangeProducts()
                      }
                  }
              });
          BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
           DeviceEventEmitter.addListener("isConnected",this.checkConnection.bind(this));

        }

        componentWillUnmount() {

           search=''
            BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
            DeviceEventEmitter.removeListener("isConnected",this.checkConnection.bind(this));
        }

        //handles the back button in android

        handleBackButtonClick() {
            _that.props.navigation.goBack();
            return true;
         }

         //Checking the Internet Connection
         checkConnection(data){

          this.setState({isConnected : data.isConnected});
         }

         // searches the products with title
         _searchProducts=(searchText)=>{
           this.setState({searchText:searchText})

         }

         //ON Clicking seach icon
         _searchIconClicked=()=>{
           if(_that.state.isConnected){
               search = this.state.searchText
               if(this.state.searchText==''){
                 this.setState({ _visible: false })
                 commonFunctions.message('Please enter some text')
               }else{
                 this.FetchExchangeProducts()
               }
             }
         }

         /*
         Refreshing the product list
         */
         _onRefresh() {
           if(_that.state.isConnected){
             this.setState({refreshing: true});
             this.FetchExchangeProducts()
           }
         }


         /*
          Rendering the ExchangeRequests
          */
        render(){

            return(
              <KeyboardAwareView animated={true} style={{backgroundColor:'white'}}>
              <SafeAreaView style={{ flex: 1}}>
              <View style={{flex: 1, backgroundColor: AppColors.white, }}>
               <Image source={bckimg} style={{ width: AppSizes.screen.width, height: AppSizes.screen.height, position: 'absolute', top: 0, left: 0 }} />

                   <Header text='Exchange Requests' navigation = {this.props.navigation}/>

                   <View style = {{flexDirection:'row'}}>
                     <SearchBar
                         onSearchChange={(searchText) => this._searchProducts(searchText)}
                         height={50}
                         onFocus={() => console.log('On Focus')}
                         onBlur={() => console.log('On Blur')}
                         placeholder={'Search'}
                         autoCorrect={false}
                         padding={5}
                         inputStyle = {{marginTop:160,width:AppSizes.screen.width-60}}
                         returnKeyType={'done'}
                       />

                       <TouchableOpacity style = {{marginTop:165,height:49,width:40,justifyContent:'center',backgroundColor:'#ddd',marginRight:10}} onPress = {this._searchIconClicked}>
                          <Image source = {require('../themes/Images/search_icon.png')} style = {{width:25,height:25,alignSelf:'center',resizeMode:'contain'}}/>
                       </TouchableOpacity>

                       </View>
                       <ScrollView refreshControl={
                                        <RefreshControl
                                          refreshing={this.state.refreshing}
                                            onRefresh={this._onRefresh.bind(this)}
                                        />
                                  }>





                  {
                   (this.state.isDataAvailable==true)?
                     <ListView
                         contentContainerStyle={AppStyles.filemanager_list}
                         dataSource={this.state.dataSource}
                         renderRow={(data, rowID) => this.renderRow(data, rowID)}
                         enableEmptySections={true}

                     />:<NoRecordFound txtMsg = {msg} marginTopSpace = {AppSizes.screen.height/5}></NoRecordFound>
                 }


                   <Spinner visible={this.state._visible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />
                 </ScrollView>

               </View>
               </SafeAreaView>
               </KeyboardAwareView>

            )

        }

        /**
         * Display Exchange Products list  from API's
         */
        renderRow(data, rowID) {


            const profile_pic = (data.ExchangeProductImage) ? { uri: data.ExchangeProductImage } : '';
            const time = (data.TimeLeft * 1000)
            return (
              <View>
                <TouchableWithoutFeedback onPress = {()=>{if(_that.state.isConnected){this.goToDetail(data)}}}>

                <View style={[AppStyles.filemanager_list_item,{height:(Platform.OS == "ios"? 270 :250)},{paddingTop : 0},{marginTop : 10},]}>
                 <View  onPress = {()=>{}} style = {{}}>


                    <View style={[AppStyles.filemanager_folder_view,{paddingTop : 10}]}>
                        <Image
                            source={profile_pic}
                            style={AppStyles.filemanager_folder_icon}
                        />
                    </View>



                    <View style={[AppStyles.filemanager_folder_view]}>
                        <Text style={[AppStyles.filemanager_folder_title,{paddingTop :10}]} numberOfLines={1}>Title : {data.ExchangeProductTitle} {"\n"}
                        </Text>
                        <Text style={AppStyles.filemanager_folder_title} numberOfLines={2}>{data.ExchangeProductDescription} {"\n"}
                        </Text>

                    </View>

                    <View style={[AppStyles.filemanager_folder_view,]}>
                      <ShareBtn text="Details" onPress={()=>{if(_that.state.isConnected){this.goToDetail(data)}}} style={[AppStyles.Auctionlist_button,{fontSize : (Platform.OS == "ios")? 12 : 12}, {justifyContent:'center', fontWeight:'bold',backgroundColor: '#f77f83' },{width:100,height:38,paddingTop:10},{alignSelf: 'center'},{marginBottom:20}]} />
                    </View>

                 </View>
                </View>

              </TouchableWithoutFeedback>

                </View>

            )

        }
//{backgroundColor:(data.IsAccepted)?"#e0e0e0":(data.IsAccepted==null)?'white':"#e0e0e0"}
        /*
          Goto RequestExchangeDetails
        */

        goToDetail=(data)=>{
            _that.props.navigation.navigate('RequestExchangeDetails',{getAuctionDetail : this.props.navigation.state.params.getAuctionDetail,ExchangeProductID:data.ExchangeProductID})
        }
        /*
          fetches the Exchanged products
        */
        FetchExchangeProducts=()=> {

            this.setState({ _visible: true })
            var data = {
                 "ProductID":this.state.ProductID,
                "PageNumber": 1,
                "PageSize": 1000,
                "Search":search
            }


            this.PostToApiCalling('POST', 'ExchangeProductListByProductID', Constant.URL_ExchangeProductListByProductID, data);
            this.setState({refreshing:false})
        }

        /**
        * API Calling
        */


        PostToApiCalling(method, apiKey, apiUrl, data) {
            new Promise(function (resolve, reject) {
                if (method == 'POST') {
                    resolve(WebServices.callWebService(apiUrl, data));
                } else {
                    resolve(WebServices.callWebService_GET(apiUrl, data));
                }
            }).then((jsonRes) => {
                if ((!jsonRes) || (jsonRes.ResponseCode == 0)) {
                    this.apiSuccessfullResponse(apiKey, jsonRes)
                    this.setState({ _visible: false })

                } else {
                    commonFunctions.message(jsonRes.ResponseMessage)
                    this.setState({ _visible: false })
                }
            }).catch((error) => {
                console.log("ERROR" + error);
            });
        }

        apiSuccessfullResponse(apiKey, jsonRes) {

            if (apiKey == 'ExchangeProductListByProductID') {
                var jsonResponse = jsonRes.ResponsePacket;

                if (jsonResponse.length == 0)
                {
                  _that.setState({dataSource:ds.cloneWithRows([])})
                  search=''

                       _that.setState({ _visible: false })
                       _that.setState({ isDataAvailable: false })

                } else {

                    this.setState({ _visible: false, dataSource: ds.cloneWithRows(jsonResponse), })
                }
            }

            else if (apiKey == 'DeleteProductByID'){
              //Fetching the ExchangeProductList after deleting the product
              _that.FetchExchangeProducts()

            }
        }


}


export default ExchangeRequests
