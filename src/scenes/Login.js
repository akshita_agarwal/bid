



import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    ScrollView,
    Dimensions,
    TextInput,
    DatePickerIOS,
    TouchableOpacity,
    Alert,
    AlertIOS,
    Keyboard,
    TouchableWithoutFeedback,
    NetInfo,
    AsyncStorage,
    Modal,
    Platform,
    BackHandler,
    SafeAreaView,
    StatusBar,
    AppState,
    DeviceEventEmitter,
    Linking
} from 'react-native';
import CountryPicker, { getAllCountries } from 'react-native-country-picker-modal';
import DatePicker from 'react-native-datepicker';
import SplashScreen from 'react-native-splash-screen';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import FCM, { FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType, notificationUnsubscribe } from 'react-native-fcm';
import Spinner from 'react-native-loading-spinner-overlay';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import webview from "./webview";
import { AppStyles, AppSizes, AppColors } from '../themes/'
import Buttons, { LoginButton, Button2} from "../components/Buttons";
const bg_top = require('../themes/Images/bg-top.png');
const logo_textImg = require('../themes/Images/logo-text.png');
const password_icon = require('../themes/Images/password.png');
const Logo = require('../themes/Images/logo.png');
const fb_img = require('../themes/Images/fb.png')
const FBSDK = require('react-native-fbsdk');
import * as commonFunctions from '../utils/CommonFunctions';
import OpenFile from 'react-native-doc-viewer';
var Constant = require('../api/WebInteractor').Constant;
const iLogoRed = require('../themes/Images/iLogoRed.png');
var WebServices = require('../api/WebInteractor').WebServices;
const countryList = ['GB', 'US'];

const {
    AccessToken,
    GraphRequest,
    GraphRequestManager,
    LoginManager
} = FBSDK;

var _that;
var currentDate = "";
var radio_props = [
    { label: 'UK        ', value: '£', color: '#ffffff' },
    { label: 'USA', value: '$' }
];

var temp = 0;
class Login extends Component {
    constructor(props) {
        super(props)

        var tdate = new Date();
           var dd = tdate.getDate(); //yields day
           var MM = tdate.getMonth(); //yields month
           var yyyy = tdate.getFullYear(); //yields year
           var todayDate= yyyy + "-" +( MM+1) + "-" + dd;
           currentDate = todayDate.toString();

        const userCountryData = getAllCountries()
            .filter((country) => countryList.includes(country.cca2))
            .filter((country) => country.cca2 === 'GB').pop();
        let callingCode = null;
        let cca2 = 'GB';
        if (!cca2 || !userCountryData) {
            cca2 = 'GB';
            callingCode = '44';
        } else {
            callingCode = userCountryData.callingCode;
        }
        this.state = {
            _facbookId: '',
            _name: '',
            _email: '',
            _phoneNum: '',
            _password: '',
            _visible: false,
            _isRemember: true,
            _coreimage: '',
            modalVisible: false,
            mobile_number: '',
            isConnected: true,
            currency: '£',
            cca2: 'GB',
            callingCode: '44',
            selectedIndex:'0',
            appState: AppState.currentState,
            isConnected : true,
            date : "",
            tdyDate : currentDate,
            lodingTxt :"Loading..."
        },

            _that = this

    }
    _showModalLoadingSpinnerOverLay = () => {
        this._modalLoadingSpinnerOverLay.show()
        //simulate http request
        this.setTimeout(() => {
            this._modalLoadingSpinnerOverLay.hide()
        }, 3000)
    }


    componentWillMount() {



      setTimeout(this.FCMNotificationConfigure, 500);
        setTimeout(this.FCMgetInitialNotification, 1000);


        AsyncStorage.getItem('UserData', (err, res) => {
            if (res) {
                var data = JSON.parse(res)
                if (data.isRemember) {

                    _that.props.navigation.navigate('Home')

                }
            }
        });
        AsyncStorage.getItem('fcmToken', (err, res) => {
            if (res) {
                var data = res;
                console.log(data);
                this.state.fcm_token = data;
                console.log(this.state.fcm_token);
            }
        });
        DeviceEventEmitter.addListener("isConnected",this.checkConnection.bind(this));
    }

    FCMgetInitialNotification = () => {
        FCM.getInitialNotification().then(notif => {
            // DeviceEventEmitter.emit('LiveUpdateRequest', '');

            //DeviceEventEmitter.emit('Refresh', '');

            console.log('notification bundle is'+JSON.stringify(notif));

                  if((_that.props.navigation.state.params == undefined) && (temp != 1))
                  {
                    if (notif.targetScreen == 'AuctionsInvitesDetails') {

                        const { navigate } = _that.props.navigation;
                        navigate('AuctionsInvites', { ProductID: notif.id,fromWhere :"Notification" ,productDeleted :"No"})
                      }
                    else if (notif.targetScreen == 'AuctionInviteList') {
                        //DeviceEventEmitter.emit('Refresh', '');
                        const { navigate } = _that.props.navigation;
                        navigate('AuctionsInvites', { ProductID: notif.id,fromWhere :"Notification", productDeleted :"Yes" })
                      }
                    else if (notif.targetScreen == 'RequestExchangeDetails') {

                        const { navigate } = _that.props.navigation;
                        navigate('RequestExchangeDetails', { ExchangeProductID: notif.id })
                    }
                    else if (notif.targetScreen == 'BidderList') {

                        const { navigate } = _that.props.navigation;
                        navigate('BidderList', { ProductID: notif.id, from: 'BidderList' })
                    }
                    else if (notif.targetScreen == 'AuctionsWon') {

                        const { navigate } = _that.props.navigation;
                        navigate('AuctionsWon', { ProductID: notif.id })
                    }
                    else if (notif.targetScreen == 'MyExchange') {

                        const { navigate } = _that.props.navigation;
                        navigate('MyExchange', { ProductID: notif.id })
                    }
                  }

        });

    }

    //Checking the Internet Connection
       checkConnection(data){

         this.setState({isConnected : data.isConnected});
       }


    FCMNotificationConfigure() {

        FCM.requestPermissions(); // for iOS
        FCM.getFCMToken().then(token => {


            console.log("fcm Token wooflocal app " + token)
            mFcmToken = token;

            if (token.length > 0) {

                AsyncStorage.setItem("fcmToken", token);
            }
            // store fcm token in your server
        });

        if (Platform.OS === 'ios') {
            FCM.getAPNSToken().then(token => {
                console.log("APNS TOKEN (getFCMToken)", token);
                if (token.length > 0) {

                    AsyncStorage.setItem("fcmToken", token);
                }
                // store fcm token in your server
            });
        }


        this.notificationListener = FCM.on(FCMEvent.Notification, async (notif) => {

            console.log("fcm token Notification Response " + JSON.stringify(notif));

            // for live update Details of MyAuction section
            DeviceEventEmitter.emit('LiveUpdateRequest', '');
            if(Platform.OS == "ios")
            {
                  if(notif.targetScreen == 'AuctionInviteList'){
                     var msg = notif.aps.alert.body;
                     var data={
                       opened_from_tray:false,
                       msg:msg
                     }
                    DeviceEventEmitter.emit('Refresh',data);
                  }
            }
            else{

                if(notif.custom_notification != undefined){
                var jsonParse = JSON.parse(notif.custom_notification)
                if(jsonParse.targetScreen == 'AuctionInviteList'){
                   var msg = jsonParse.body;
                   var data={
                     opened_from_tray:false,
                     msg:msg
                   }
                  DeviceEventEmitter.emit('Refresh',data);
                }
              }
          }


            if (notif.opened_from_tray) {
                FCM.setBadgeNumber(0);

                temp = 1;

                // var response = JSON.stringify(notif);
                //alert("fcm token Type value+++ " + JSON.stringify(notif));

                if (notif.targetScreen == 'AuctionsInvitesDetails') {
                    const { navigate } = _that.props.navigation;
                    navigate('AuctionsInvites', { ProductID: notif.id,fromWhere :"Notification" })
                }
                else if (notif.targetScreen == 'AuctionInviteList') {
                  var data={
                    opened_from_tray:true,
                    msg:notif.body
                  }
                    DeviceEventEmitter.emit('Refresh', data);
                    const { navigate } = _that.props.navigation;
                    navigate('AuctionsInvites', { ProductID: notif.id,fromWhere :"Notification" , isProductDeleted :"Yes" })
                  }
                else if (notif.targetScreen == 'RequestExchangeDetails') {

                    const { navigate } = _that.props.navigation;
                    navigate('RequestExchangeDetails', { ExchangeProductID: notif.id })
                }
                else if (notif.targetScreen == 'BidderList') {

                    const { navigate } = _that.props.navigation;
                    navigate('BidderList', { ProductID: notif.id, from: 'BidderList' })
                }
                else if (notif.targetScreen == 'AuctionsWon') {

                    const { navigate } = _that.props.navigation;
                    navigate('AuctionsWon', { ProductID: notif.id })
                }
                else if (notif.targetScreen == 'MyExchange') {

                    const { navigate } = _that.props.navigation;
                    navigate('MyExchange', { ProductID: notif.id })
                }


            }
            else {

            }
            //await someAsyncCall();

            if (Platform.OS === 'ios') {
                //alert(JSON.stringify(notif))
                // optional
                // iOS requires developers to call completionHandler to end notification process. If you do not call it your background remote notifications could be throttled, to read more about it see the above documentation link.
                // This library handles it for you automatically with default behavior (for remote notification, finish with NoData; for WillPresent, finish depend on "show_in_foreground"). However if you want to return different result, follow the following code to override
                // notif._notificationType is available for iOS platfrom
                switch (notif._notificationType) {
                    case NotificationType.Remote:
                        notif.finish(RemoteNotificationResult.NewData) //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
                        break;
                    case NotificationType.NotificationResponse:
                        notif.finish();
                        break;
                    case NotificationType.WillPresent:
                        notif.finish(WillPresentNotificationResult.All) //other types available: WillPresentNotificationResult.None
                        break;
                }
            }



        });




        this.refreshTokenListener = FCM.on(FCMEvent.RefreshToken, (token) => {
            console.log("fcm Token wooflocal app refresh " + token)
            // fcm token may not be available on first load, catch it here
            if (token.length > 0) {

                AsyncStorage.setItem("fcmToken", token);

            }

        });
    }

    componentDidMount() {
        //    BackHandler.removeEventListener('hardwareBackPress',this.handleBackButton);
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        AppState.addEventListener('change', this._handleAppStateChange);

    }


    componentWillUnmount() {
        AppState.removeEventListener('change', this._handleAppStateChange);
        DeviceEventEmitter.removeListener("isConnected",this.checkConnection.bind(this));
      }

      _handleAppStateChange = (nextAppState) => {
          if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            console.log('App has come to the foreground!')
          }
          this.setState({appState: nextAppState});
        }

    handleBackButton() {
        return true;
    }


    render() {
        const { _facbookId, _name, _phoneNum, _password, currency,selectedIndex } = this.state
        return (
            <KeyboardAwareView animated={true}>
                <SafeAreaView style={{ flex: 1 }}>
                  <ScrollView  style = {{backgroundColor :"white"}}>

                    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>

                        <View style={AppStyles.login_container}>

                            <Image source={bg_top} style={AppStyles.bg_topImg} />

                            <View style={AppStyles.logo_login_cnt}>
                                <Image source={Logo} style={AppStyles.logo_login} />
                            </View>
                            <View style={AppStyles.logo_textImg_cnt}>
                                <Image source={logo_textImg} style={AppStyles.logo_textImg} />
                            </View>
                            <View>
                                <View style={[AppStyles.InputPasswordtxt_cnt, { paddingTop: 15, flexDirection: 'row' }]}>
                                    <CountryPicker
                                        countryList={countryList}
                                         onChange={(value) => _that.onChange(value)}
                                        cca2={this.state.cca2}
                                        translation='eng'
                                    />
                                    <View style={{ marginRight: 35, marginLeft: 10 }}>
                                        <TextInput style={[AppStyles.InputPasswordtxt, { paddingLeft: 5 }]}
                                            underlineColorAndroid='transparent'
                                            autoCorrect={false}
                                            returnKeyType={"next"}
                                            blurOnSubmit={false}
                                            autoFocus={false}
                                            maxLength={15}
                                            placeholder="Mobile Number"
                                            placeholderTextColor={AppColors.lightGray_text}
                                            keyboardType={Platform.OS == "ios" ? 'phone-pad' : 'numeric'}
                                            ref="_phoneNum"
                                            onChangeText={_phoneNum => this.setState({ _phoneNum })}
                                            value={_phoneNum}
                                            onSubmitEditing={() => this.refs._password.focus()}>
                                        </TextInput>
                                    </View>
                                </View>

                            </View>
                            <View style={{ paddingTop: 10 }}>
                                <View style={AppStyles.InputPasswordtxt_cnt}>
                                    <TextInput style={[AppStyles.InputPasswordtxt, { paddingLeft: 5 }]}
                                        underlineColorAndroid='transparent'
                                        autoCorrect={false}
                                        returnKeyType={"next"}
                                        autoFocus={false}
                                        autoCapitalize="none"
                                        placeholder="Password"
                                        placeholderTextColor={AppColors.lightGray_text}
                                        secureTextEntry
                                        maxLength={50}
                                        ref="_password"
                                        onChangeText={_password => this.setState({ _password })}
                                        value={_password}>
                                    </TextInput>
                                </View>
                            </View>
                            <View style={[AppStyles.loginbtn_cnt, { marginTop: 5 }]}>
                                <LoginButton style={[AppStyles.loginbtn]} text="LOG IN" onPress={()=>{if(_that.state.isConnected){this.btnLoginPress()}}}></LoginButton>
                            </View>
                            <TouchableOpacity style={{ width: 100, alignSelf: 'center' }} onPress={()=>{if(_that.state.isConnected){this.forgetPasswordPressed()}}}>
                                <View style={AppStyles.loginTextStyle_cnt}>
                                    <Text style={[AppStyles.forgetPasswordText,{fontWeight:"500"}]}>Forgot Password</Text>
                                </View>
                            </TouchableOpacity>
                          {  /*<View style={AppStyles.loginWithFbTextStyle_cnt}>
                                <Text style={{ fontSize: 10, textAlign: "center" }}> Log in with facebook</Text>
                            </View>
                            <View style={AppStyles.fb_img_cnt}>
                                <TouchableOpacity onPress={this.fetchFacbookInfo}>
                                    <Image style={AppStyles.fb_img} source={fb_img} />
                                </TouchableOpacity>
                            </View>*/}
                            <View style={{marginTop:30}}>
                                <View style={[AppStyles.SignUpBottomView,{position :"relative"}]}>
                                    <View style={AppStyles.loginWithViewCentre}>
                                        <Text style={AppStyles.loginWithText}>Don't have an account?</Text>
                                        <View style={AppStyles.signUpView}>
                                            <TouchableOpacity style={{ borderRadius: 7, overflow: (Platform.OS == "ios") ? 'hidden' : null }} activeOpacity={.8} onPress={()=>{if(_that.state.isConnected){this.signUpPressed()}}}>
                                                <Text style={[AppStyles.signUpText, { fontSize: 15 }]} onPress={this.onPressTitle}>
                                                    Sign Up
                                      </Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                                <View style={[AppStyles.SignUpBottomView,{bottom : 10, height : 50,position :"relative",marginTop : 20,paddingBottom : 20}]}>
                                    <View style={[AppStyles.loginWithViewCentre,{justifyContent :"center", alignItems :"center"}]}>
                                        <Text style={[AppStyles.loginWithText],{fontSize : 17, color:"black", textAlign :"center"}}>Contact Bids User Guide </Text>
                                      <View style={[AppStyles.signUpView,{ height : 60}]}>
                                            <TouchableOpacity style={{borderRadius: 7,height : 50,width: 50, justifyContent :"center", alignItems :"center",overflow: (Platform.OS == "ios") ? 'hidden' : null }} activeOpacity={.8} onPress={()=>{
                                                 //this.props.navigation.navigate("OpenPDF")
                                                // this.props.navigation.navigate('webview', { url: '../themes/document/guide.pdf' })
                                              // this.setState({isVisible : true})
                                             this.pdfOpen()
                                              }}>
                                                  <Image source={iLogoRed} style = {{height : 30,width: 30, }} />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>

                                <Modal visible={this.state.modalVisible} animationType={'slide'} onRequestClose={() => this.closeModal()}>
                                  <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                                    <View style={AppStyles.modalContainer}>
                                        <View style={{ height: 45, backgroundColor: '#89f4ef', justifyContent: 'center', borderBottomWidth: 1, borderBottomColor: 'black' }}>
                                            <Text style={{ fontSize: 25, fontWeight: 'bold', alignSelf: 'center' }}>Contact Bids</Text>
                                        </View>


                                        <View style={[AppStyles.innerContainer,{marginLeft:8},{marginRight:8}]}>

                                          <View style={[AppStyles.Input_iconcnt,{ borderColor: "black" },]}>
                                            <View style={{flexDirection:'row',marginLeft:10,flex:1}}>

                                                <CountryPicker
                                                  countryList={countryList}
                                                  onChange={(value) => _that.onChange(value)}
                                                  cca2={this.state.cca2}
                                                  translation='eng'
                                                  />
                                                <View style={{ marginLeft: 3, width: 300 }}>
                                                  <TextInput
                                                      autoCorrect={false}
                                                      underlineColorAndroid="transparent"
                                                      returnKeyType={"next"}
                                                      keyboardType={'numeric'}
                                                      autoFocus={false}
                                                      placeholder="Mobile Number"
                                                      maxLength={15}
                                                      placeholderTextColor={AppColors.lightGray_text}
                                                      style={[AppStyles.InputCnt_txt, { borderColor: 'black' }]}
                                                      onChangeText={mobile_number => this.setState({ mobile_number })}
                                                      value={this.state.mobile_number}
                                                  />
                                              </View>

                                            </View>
                                          </View>
                                        </View>

                                        <View style = {{marginTop : 20, flexDirection : "row"}}>
                                          <Text style = {{marginTop : 10, marginRight: 10, fontSize : 17}}> DOB: </Text>
                                          <DatePicker
                                                   style={{width: ((AppSizes.screen.width/2)-20), borderRadius : 10}}
                                                   date={this.state.date}
                                                   mode="date"
                                                   placeholder="select date"
                                                   format="YYYY-MM-DD"
                                                   minDate="01-Jan-1900"
                                                   maxDate={this.state.tdyDate}
                                                   confirmBtnText="Confirm"
                                                   cancelBtnText="Cancel"
                                                   customStyles={{
                                                     dateIcon: {
                                                       position: 'absolute',
                                                       left: 0,
                                                       top: 4,
                                                       marginLeft: 0
                                                     },
                                                     dateInput: {
                                                       marginLeft: 36,
                                                       borderRadius : 10,
                                                       backgroundColor :"#DCDCDC"
                                                     }
                                                     // ... You can check the source to find the other keys.
                                                   }}
                                                   onDateChange={(date) => {this.setState({date: date})}}
                                                  />

                                        </View>

                                          <View style={{ marginTop: 15, borderWidth: 0, width: 200,marginLeft:8}}>
                                                <Text style={{ fontWeight: 'bold', fontSize: 15 }}>Choose your country</Text>
                                            </View>

                                                <RadioGroup
                                                  style={{width:100,height: 90}}
                                                    size={24}
                                                    selectedIndex={this.state.selectedIndex}
                                                    onSelect = {(index, value) => {this.changeCurrencyData(index,value)}}>

                                                      <RadioButton value={'£'} >
                                                        <Text>UK</Text>
                                                      </RadioButton>

                                                      <RadioButton value={'$'}>
                                                        <Text>USA</Text>
                                                      </RadioButton>

                                                </RadioGroup>
                                            <View style={[{ marginTop: 8, borderWidth: 0, width: 200,marginLeft:8 }]}>
                                                <Text style={{ fontWeight: 'bold', fontSize: 15 }}>Currency :  {currency}</Text>
                                            </View>

                                            <View style={{ width: 100, height: 90, justifyContent: 'center', alignItems: 'center' }}>
                                                <TouchableOpacity style={[{ width: 70, height: 38, backgroundColor: '#89f4ef', justifyContent: 'center', borderRadius: 10 }]} onPress={() => {if(_that.state.isConnected){this.closeModal()}}}>
                                                    <Text style={{ textAlign: 'center', fontSize: 12, fontWeight: "bold", color: "black" }}> Submit </Text>
                                                </TouchableOpacity>
                                            </View>

                                    </View>
                                    </TouchableWithoutFeedback>
                                </Modal>
                            </View>
                            <Spinner visible={this.state._visible} textContent={this.state.loadingTxt} textStyle={{ color: '#FFF' }} />
                            {this.state.country &&
                                <Text>
                                    {JSON.stringify(this.state.country, null, 2)}
                                </Text>
                            }
                        </View></TouchableWithoutFeedback>

                   </ScrollView>

                </SafeAreaView>

            </KeyboardAwareView>
        )
    }

  /*
      Show the PDF In WebView in IOS and show the PDF in PDF view in Android
   */

    pdfOpen=()=>{

      if (Platform.OS == 'ios') {
        this.props.navigation.navigate('webview', { url: 'http://app.contactbids.com/Content/pdf/guide.pdf', headerTxt :"User Guide" })
      }else {

          _that.setState({ _visible: true,loadingTxt :"User guide loading.." })

        OpenFile.openDoc([{
                  url: 'http://app.contactbids.com/Content/pdf/guide.pdf',
                  fileName: "sample",
                  cache: false,
                  fileType: ""
              }], (error, url) => {
                  if (error) {
                      Alert.alert("URL not found");
                  } else {
                      console.log(url)
                        _that.setState({ _visible: false, loadingTxt :"Loading..." });
                  }
              })
      }


    }


//Change Country functionality

    changeCurrencyData = (index,value) =>{

      if(value == "£")
      {
            _that.setState({currency:value,cca2:"GB"})
        }
      else {
        _that.setState({currency:value,cca2:"US"})
      }
    }

    openModal() {
        _that.setState({ _visible: false });
        this.setState({ modalVisible: true });
    }

    onChange(value){
      _that.setState({
          cca2: value.cca2,
          callingCode: value.callingCode,
  });
    value.cca2=='GB'?this.setState({selectedIndex:'0',currency:'£'}):this.setState({selectedIndex:'1',currency:'$'})

    }

    closeModal() {

      const {mobile_number,selectedIndex} = this.state;
      if (mobile_number == "") {
          _that.setState({ _visible: false });
          commonFunctions.message("Please enter Mobile Number.")
      } else if (commonFunctions.validatePhoneNum(mobile_number, selectedIndex)==true) {
            this.setState({ _visible: false });
          commonFunctions.message("Please enter correct Mobile Number.")
      }
      else if (commonFunctions.validatePhoneNum(mobile_number, selectedIndex)=="UK") {
            this.setState({ _visible: false });
          commonFunctions.message("Mobile Number should not include country code (44) .")
      }
      else if (commonFunctions.validatePhoneNum(mobile_number, selectedIndex)=="USA") {
            this.setState({ _visible: false });
          commonFunctions.message("Mobile Number should not include country code (1) .")
      }
        else {
          this.setState({ modalVisible: false });
            _that.setState({ _visible: false })

          _that.SaveUserdtl();
         }
      }

 //Sign UP Btn functionality
    signUpPressed = () => {
        //this.nofificationSetup();
        this.setState({ _phoneNum: '', _password: '' })
        const { navigate } = this.props.navigation;
        navigate('Registration')
    }
    forgetPasswordPressed = () => {
        this.setState({ _phoneNum: '', _password: '' })
        const { navigate } = this.props.navigation;
        navigate('ForgotPassword', { name: 'ForgotPassword' })
    }
    /**
     * LogIn click functionality
     */


    fetchFacbookInfo = () => {
        LoginManager.logOut();
        if (Platform.OS == "ios") {
            LoginManager.setLoginBehavior('web');
        }
        else {
            LoginManager.setLoginBehavior('WEB_ONLY');
        }

        const infoRequest = new GraphRequest('/me', {
            httpMethod: 'GET',
            version: 'v2.5',
            parameters: {
                'fields': {
                    'string': 'email,name,friends'
                }
            }
        }, this.responseInfoCallback, );


        LoginManager.logInWithReadPermissions(['public_profile','email']).then(
            function (result) {
                if (result.isCancelled) {
                } else {
                    new GraphRequestManager().addRequest(infoRequest).start();
                }
            },
            function (error) {
            }
        );
    }

    responseInfoCallback(error: ?Object, result: ?Object) {

        if (error) {
        } else {
            _that.setState({

                _facbookId: result.id,
                _name: result.name,
                _email : result.email,
            })
            _that.openModal()
        }
    }

    /**
     * facebook login
     */

    SaveUserdtl = () => {
          _that.setState({ _visible: false })
        FCM.getFCMToken().then(token => {
            console.log("fcm Token ContactBids app " + token)

            _that.setState({ fcm_token: token });

        });
        const { _facbookId, _name, _isRemember, _visible, currency,callingCode,_email } = this.state
        // Loader show
        var date = this.state.date.toString();
        const dateTime = new Date(date).getTime();
        var timeStamp = (Math.floor(dateTime / 1000));


        let selectedDate  = this.state.date
        if(this.state.date == currentDate)
        {
              selectedDate = ""
        }

        var data = {
            "FacebookID": _facbookId,
            "PhoneNo": this.state.mobile_number,
            "Name": _name,
            "Email": _email,
            "DeviceToken": this.state.fcm_token,
            "DeviceType": (Platform.OS == "ios") ? 1 : 2,
            "CountryName": (this.state.currency == '£') ? 'UK' : 'USA',
            "CurrencyName": this.state.currency,
            "CountryCode": this.state.callingCode,
            "DOB" : selectedDate,
        }
        console.log('facebook data is '+JSON.stringify(data));
          _that.setState({mobile_number:""});
        this.PostToApiCalling('POST', 'LoginWithFacebook', Constant.URL_UserLoginWithFacebook, data);
    }

    /**
           * API Calling
           */

    PostToApiCalling(method, apiKey, apiUrl, data) {
        new Promise(function (resolve, reject) {
            if (method == 'POST') {
                resolve(WebServices.callWebService(apiUrl, data));
            } else {
                resolve(WebServices.callWebService_GET(apiUrl, data));
            }
        }).then((jsonRes) => {
            _that.setState({ _visible: false })
            if ((!jsonRes) || (jsonRes.ResponseCode == 0)) {
                // Loader hide

                _that.setState({ _visible: false })
                _that.apiSuccessfullResponse(apiKey, jsonRes)
            }else{
              _that.setState({ _visible: false })
              setTimeout(()=>{Alert.alert(jsonRes.ResponseMessage)},500)

            }
        }).catch((error) => {
            console.log("ERROR" + error);
        });
    }



    apiSuccessfullResponse(apiKey, jsonRes) {
        _that.setState({ _visible: false })
        const { _email, _password, _isRemember } = this.state
        var jsonResponse = jsonRes.ResponsePacket;
        if (apiKey == 'LoginWithFacebook') {

          _that.setState({ _visible: false })
            if (jsonRes.ResponseCode == 0) {
                AsyncStorage.setItem("isFaceBookLogin", "1");
                this.setState({ _visible: false })
                _that.props.navigation.navigate('Home')

                var data = {
                    "EmailId": jsonResponse.Email,
                    "FacebookID": jsonResponse.FacebookID,
                    "Name": jsonResponse.Name,
                    "UserID": jsonResponse.UserID,
                    "CurrencyName": jsonResponse.CurrencyName,
                    "isRemember": _isRemember,

                }
                AsyncStorage.setItem("UserData", JSON.stringify(data))
                this.setState({ _visible: false })
            } else {
                this.setState({ _visible: false })
            }
        }else if (apiKey == 'LoginWithMobile') {
          var jsonResponse = jsonRes.ResponsePacket;
            if (jsonRes.ResponseCode == 0) {


                var data = {
                    "EmailId": jsonResponse.Email,
                    "Name": jsonResponse.Name,
                    "UserID": jsonResponse.UserID,
                    "CurrencyName": jsonResponse.CurrencyName,
                    "isRemember":this.state._isRemember,
                }
                  AsyncStorage.setItem("isFaceBookLogin", "0");
                AsyncStorage.setItem("UserData", JSON.stringify(data))
                _that.props.navigation.navigate('Home')


                this.setState({ _visible: false })
            } else {
                this.setState({ _visible: false })

            }

        }
        else {

            if (jsonRes.ResponseCode == 1) {
                if (Platform.OS == "ios") {
                    _that.setState({ _visible: false })
                    setTimeout(() => (AlertIOS.alert(
                        '',
                        jsonRes.ResponseMessage,
                        [
                            {
                                text: 'OK',
                                onPress: () => { this.setState({ _visible: false })},

                            },

                        ]
                    )), 700)
                    _that.setState({ _visible: false })
                }
                else {
                  this.setState({ _visible: false })
                    Alert.alert(
                        '',
                        jsonRes.ResponseMessage,
                        [
                            { text: 'OK', onPress: () => { _that.setState({ _visible: false }) } },

                        ],
                        { cancelable: false }
                    );

                    _that.setState({ _visible: false })
                }

            }
        }
    }
    /**
     * User manual login
     */

    btnLoginPress = () => {
        FCM.getFCMToken().then(token => {

            console.log("fcm Token wooflocal app " + token)
            _that.setState({ fcm_token: token });
            _that.userLoggedIn();
        });
    }

    userLoggedIn = () => {

        Keyboard.dismiss();


        _that.setState({ _visible: true })
        const { _phoneNum, _password, _isRemember, callingCode, _visible, selectedIndex } = _that.state
        if (_phoneNum == "") {
            _that.setState({ _visible: false });
            commonFunctions.message("Please enter Mobile Number.")
        } else if (commonFunctions.validatePhoneNum(_phoneNum, selectedIndex)==true) {
              this.setState({ _visible: false });
            commonFunctions.message("Please enter correct Mobile Number.")
        }
        else if (commonFunctions.validatePhoneNum(_phoneNum, selectedIndex)=="UK") {
              this.setState({ _visible: false });
            commonFunctions.message("Mobile Number should not include country code (44) .")
        }
        else if (commonFunctions.validatePhoneNum(_phoneNum, selectedIndex)=="USA") {
              this.setState({ _visible: false });
            commonFunctions.message("Mobile Number should not include country code (1) .")
        } else if (_password == "") {
            _that.setState({ _visible: false });
            commonFunctions.message("Please enter Password.")
        }

        else {

            var data = {
                "PhoneNo": _phoneNum,
                "Password": _password,
                "DeviceToken": this.state.fcm_token,
                "CountryCode": callingCode,
                "DeviceType": (Platform.OS == "ios") ? 1 : 2,


            }
            console.log('ios token' + this.state.fcm_token)
            console.log('login data is  ' + JSON.stringify(data));
        }
        _that.PostToApiCalling('POST', 'LoginWithMobile', Constant.URL_UserLoginWithMobile, data);
    }
}



export default Login
