/**
 * Created by Rishi Paliwal
 * 9 Nov 2017
 * FileManager Class
 */

 /**
  * Created by Rishi Paliwal
  * 9 Nov 2017
  * FileManager Class
  */

  import React, { Component } from 'react';
  import { DrawerNavigator, StackNavigator, } from 'react-navigation';
  import { AppStyles, AppSizes, AppColors } from '../themes/'
  import Buttons, { LoginButton, Button2, Header, ShareBtn, BidderListBtn ,Countdwn,NoRecordFound} from "../components/Buttons";
  import Spinner from 'react-native-loading-spinner-overlay';
  import {
      Platform,
      StyleSheet,
      Text,
      View,
      Button,
      ListView,
      Image,
      ScrollView,
      Modal,
      AsyncStorage,
      TextInput,
      TouchableOpacity,
      BackHandler,
      RefreshControl,
      TouchableWithoutFeedback,
      Alert,
      AlertIOS,
      SafeAreaView,
      DeviceEventEmitter,
  } from 'react-native';
  import Model from './Model';
  import { NavigationActions } from 'react-navigation';
  import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
  var folderIcon = require('../themes/Images/folder.png')
  const bckimg = require('../themes/Images/bg-bottom.png')
  var Constant = require('../api/WebInteractor').Constant;
  var WebServices = require('../api/WebInteractor').WebServices;
  import * as commonFunctions from '../utils/CommonFunctions'
  const deleteimg = require('../themes/Images/close_red.png')

  var _that = '';
  var data = [];
  var ds;
  var numberOfTap = 1;

  class AuctionsWon extends React.Component {


       constructor(props) {
             super(props);
             _that = this
             ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
             this.state = {
                 dataSource: ds.cloneWithRows(data),
                 modalVisible:false,
                 _visible:false,
                 bidAmount:'',
                 UserID:'',
                 product_id:'',
                 minBidAmount:'',
                 maxBidAmount:'',
                 isTimeLeft:false,
                 refreshing:false,
                 isConnected : true,
                 isDataAvailable:true,//intially its show LISTVIEW with zero Items

             };
       }

       componentWillMount()
       {
         AsyncStorage.getItem('UserData')
             .then((res) => {
                 if (res) {
                     var data = JSON.parse(res)
                     if (data.isRemember) {
                         this.setState({
                             UserID: data.UserID
                         })
                         this.FetchAuctionWonList()
                     }
                 }
             });
            //commonFunctions.checkConnection();
             BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
               DeviceEventEmitter.addListener("isConnected",this.checkConnection.bind(this));
       }
       componentWillUnmount() {

           BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
             DeviceEventEmitter.removeListener("isConnected",this.checkConnection.bind(this));
       }

         //GO BACK TO previous screen on device back button click
       handleBackButtonClick() {
         _that.props.navigation.dispatch({
         type: NavigationActions.NAVIGATE,
         routeName: 'Home',
         action: {
           type: NavigationActions.RESET,
           index: 1,
           actions: [{type: NavigationActions.NAVIGATE, routeName: 'Home'}]
         }})
           return true;
       }
       /*
       Refreshing the product list
       */
       _onRefresh() {
           this.setState({refreshing: true});
           this.FetchAuctionWonList()
       }

       //Checking the Internet Connection
       checkConnection(data){

             this.setState({isConnected : data.isConnected});
       }

     render() {
         return (
           <KeyboardAwareView animated={true} >
          <SafeAreaView style = {{flex:1}}>
          <View style={{ backgroundColor: AppColors.white,flex:1 }}>
          <Image source={bckimg} style={{ width: AppSizes.screen.width, height: AppSizes.screen.height, position: 'absolute', top: 0, left: 0 }} />
        <ScrollView style = {{backgroundColor :"white"}}
                      refreshControl={
                                       <RefreshControl
                                         refreshing={this.state.refreshing}
                                           onRefresh={this._onRefresh.bind(this)}
                                       />
                                 }>
           <Header text='Auction Won' navigation = {this.props.navigation}/>
         {
           (this.state.isDataAvailable==true)?
                  <ListView
                     contentContainerStyle={AppStyles.filemanager_list}
                     dataSource={this.state.dataSource}
                     renderRow={(data, rowID) => this.renderRow(data, rowID)}
                     enableEmptySections={true}
                     style={{marginTop:180}}
                     renderEmptyListComponent={() => <Text>hello</Text>}
                 />:<NoRecordFound></NoRecordFound>
             }

                 <Spinner visible={this.state._visible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />
            </ScrollView>
             </View>
             </SafeAreaView>
             </KeyboardAwareView>
         );
     }
     /**

      * Display Folders list  from API's
      */
      renderRow(data, rowID) {



        const profile_pic = (data.ProductImage) ? { uri: data.ProductImage } : '';
        const time = (data.TimeLeft * 1000)


          return (
            <View>
              <TouchableWithoutFeedback onPress = {()=>this.goToDetail(data)}>
              <View style={[AppStyles.filemanager_list_item,{height:(Platform.OS == "ios"? 295:310)}]}>
                  <View style={[AppStyles.filemanager_folder_view,{paddingTop : 10}]}>
                      <Image
                          source={profile_pic}
                          style={AppStyles.filemanager_folder_icon}
                      />
                  </View>
                  <View style={[AppStyles.filemanager_folder_view,{paddingTop:10,marginBottom : 2}]}>
                      <Text style={AppStyles.filemanager_folder_title} numberOfLines={2}>Title:  {data.Title} {"\n"}
                          <Text numberOfLines={2}>Total Bid:{data.TotalBid}</Text>
                      </Text>

                      <Text style={AppStyles.filemanager_folder_title} numberOfLines={2}>Bidders : {data.Bidders} {"\n"}
                          <Text numberOfLines={2}>Bid Amount : £{data.BidAmount}</Text>
                      </Text>

                      <Text style={AppStyles.filemanager_folder_title} numberOfLines={1}>{data.Description}
                      </Text>
                  </View>
                  <View style={[AppStyles.filemanager_folder_view,{paddingTop:3,marginBottom:10,marginTop:0}]}>
                    <ShareBtn text="Details" onPress={()=>this.goToDetail(data)} style={[AppStyles.Auctionlist_button,{fontSize : (Platform.OS == "ios")? 11 : 12}, {justifyContent:'center', fontWeight:'bold',backgroundColor: '#f77f83' },{width:100,height:38,paddingTop:10},{alignSelf: 'center'},{marginBottom:20}]} />
                  </View>
                  </View>
                </TouchableWithoutFeedback>
                  <TouchableOpacity onPress = {()=>{if(_that.state.isConnected){this.deleteProduct(data.Title,data.ProductID)}}} style={{ width: 30, height: 40, top: -1 ,position : 'absolute' ,alignSelf: "flex-end" }} >
                      <Image source={deleteimg} style ={{position : 'absolute' }} />
                   </TouchableOpacity>
              </View>

          )
      }
   //Navigate TO WonDetails SCREEN
      goToDetail=(data)=>{
        if(_that.state.isConnected){
        _that.props.navigation.navigate('WonDetails',{ProductID:data.ProductID})
       }
      }

   //Fetch auction List
      FetchAuctionWonList=()=>{

        this.setState({ _visible: true })

        var data = {
            "UserID": this.state.UserID,
            "PageNumber": 1,
            "PageSize": 1000
        }

        this.PostToApiCalling('POST', 'AuctionWonListByBidder', Constant.URL_AuctionWonListByBidder, data);
        this.setState({refreshing:false})
      }

   //deleteProduct function and delete product Api Call
      deleteProduct = (ProductName,ProductID)=> {

        var data = {
          "ProductID": ProductID,
          "UserID":this.state.UserID,
        }

        if(Platform.OS=="ios")
        {
              setTimeout(()=>(AlertIOS.alert(
              ProductName,
              'Are you sure you want to delete this product',
              [
                {
                  text: 'YES',onPress: () =>{this.PostToApiCalling('POST', 'AuctionWonDelete', Constant.URL_AuctionWonDelete, data)}

                },
                {
                  text: 'NO'
                },

              ]
            )), 100)

            }
            else {
              Alert.alert(
                      ProductName,
                      'Are you sure you want to delete this product',
                      [
                        {
                          text: 'YES',onPress: () =>{this.PostToApiCalling('POST', 'AuctionWonDelete', Constant.URL_AuctionWonDelete, data)}
                        },
                        {
                          text: 'NO'
                        },

                      ],
                      { cancelable: false }
                    )

            }

      }

      /**
      * API Calling
      */


      PostToApiCalling(method, apiKey, apiUrl, data) {
          new Promise(function (resolve, reject) {
              if (method == 'POST') {
                  resolve(WebServices.callWebService(apiUrl, data));
              } else {
                  resolve(WebServices.callWebService_GET(apiUrl, data));
              }
          }).then((jsonRes) => {
              if ((!jsonRes) || (jsonRes.ResponseCode == 0))
                  this.setState({ _visible: false })
                  this.apiSuccessfullResponse(apiKey, jsonRes)


            }
          )
          .catch((error) => {
              console.log("ERROR" + error);
          })
      }


      apiSuccessfullResponse(apiKey, jsonRes) {

          if (apiKey == 'AuctionWonListByBidder') {
              var jsonResponse = jsonRes.ResponsePacket;

              if (jsonResponse.length == 0)
              {
                _that.setState({dataSource:ds.cloneWithRows([])})
                search=''

                         _that.setState({ _visible: false })
                        _that.setState({ isDataAvailable: false })


              } else {
                  // commonFunctions.message(jsonRes.ResponseMessage)
                  this.setState({ _visible: false, dataSource: ds.cloneWithRows(jsonResponse), })
              }
          }

          else if (apiKey == 'AuctionWonDelete'){

            _that.FetchAuctionWonList()

          }
      }

}
  export default AuctionsWon
