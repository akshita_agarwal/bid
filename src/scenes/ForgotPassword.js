import React, { Component } from 'react';
import { DrawerNavigator, StackNavigator } from 'react-navigation';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Button,
    TouchableWithoutFeedback,
    Keyboard,
    Image,
    TouchableOpacity,
    TextInput,
    ScrollView,
    AsyncStorage,
    BackHandler,
    Alert,
    AlertIOS,
    SafeAreaView,
    DeviceEventEmitter
} from 'react-native';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import { AppStyles, AppSizes, AppColors } from '../themes/'
import Buttons, { ForgotButton, Button2, Header } from "../components/Buttons";
const Logo = require('../themes/Images/logo.png');
import Spinner from 'react-native-loading-spinner-overlay';
var ImagePicker = require('react-native-image-picker');
import * as commonFunctions from '../utils/CommonFunctions'
var Constant = require('../api/WebInteractor').Constant;
var WebServices = require('../api/WebInteractor').WebServices;
var _that = "";
class ForgotPassword extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            full_name: '',
            mobile_number: '',
            Password: '',
            confirm_password: '',
            email_id: '',
            _visible: false,
            avatarSource: '',
            _coreimage: '',
            UserID: '',
            isConnected : true,
        }
        _that = this;
    }
    static navigationOptions = ({navigation, screenProps}) => ({

                 headerStyle : {

                          backgroundColor: '#89f4ef'
                 },
                 headerTintColor : "white",

                 headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.goBack()}}>
                     <Image source = {require('../themes/Images/back-arrow.png')}/>
                   </TouchableOpacity>,


    });
    componentWillMount() {
      //commonFunctions.checkConnection();
        AsyncStorage.getItem('UserData')
            .then((res) => {
                if (res) {
                    var data = JSON.parse(res)
                    if (data.isRemember) {
                        this.setState({
                            UserID: data.UserID
                        })
                    }
                }
            });
              BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
              DeviceEventEmitter.addListener("isConnected",this.checkConnection.bind(this));
    }
    componentWillUnmount() {

        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
        DeviceEventEmitter.removeListener("isConnected",this.checkConnection.bind(this));
    }
  //handles the back button in android
  handleBackButtonClick() {
      _that.props.navigation.goBack();
      return true;
   }
   //Checking the Internet Connection
   checkConnection(data){

          this.setState({isConnected : data.isConnected});
    }
    render() {
        const {  email_id } = this.state
        return (
            <KeyboardAwareView animated={true}>
           <SafeAreaView style={{ flex: 1}}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>

              <ScrollView style={{backgroundColor:'#fff'}}>
                    <View style={[AppStyles.container, { paddingLeft: AppColors.paddingLeft30, paddingRight: AppColors.paddingRight30 }]}>
                        <Header text='Forgot Password' navigation = {this.props.navigation}/>



                        <View style={[AppStyles.Input_iconcnt, { marginTop: 270 }]}>
                            <View style={AppStyles.Inputtxt}>
                                <TextInput
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    returnKeyType={"done"}
                                    autoFocus={false}
                                    placeholder="Email ID"

                                    autoCapitalize = "none"
                                    keyboardType={'email-address'}
                                    placeholderTextColor={AppColors.lightGray_text}
                                    style={[AppStyles.InputCnt_txt, { height: 40 },{}]}
                                    multiline={false}
                                    numberOfLines={3}

                                    onChangeText={email_id => this.setState({ email_id })}
                                    value={email_id}
                                />
                            </View>
                        </View>

                        <ForgotButton text={"Forgot Password"} onPress={()=>{if(_that.state.isConnected){this.Save()}}} style={AppStyles.green_round_button} />
                        <Spinner visible={this.state._visible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />
                    </View>
                  </ScrollView>
            </TouchableWithoutFeedback >
             </SafeAreaView>
             </KeyboardAwareView>
        );
    }
    PickerImage = () => {
        // More info on all the options is below in the README...just some common use cases shown here
        const options = {
            quality: 1.0,
            maxWidth: 200,
            maxHeight: 200,
        };

        /**
                 * The first arg is the options object for customization (it can also be null or omitted for default options),
                 * The second arg is the callback which sends object: response (more info below in README)
                 */
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                // let source = { uri: response.uri };

                // You can also display the image using data:
                let source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.setState({
                    avatarSource: source,
                    _coreimage: response.data
                });
            }
        });
    }
    Save = () => {
        const { _coreimage, _email, full_name, mobile_number, Password, confirm_password, email_id, avatarSource } = this.state
        // Loader show
        this.setState({ _visible: true })

        /**
         *  Form  Validations
         */

         if (email_id == "") {
            this.setState({ _visible: false });
            commonFunctions.message("Please enter Email ID")
          }
          else if(!commonFunctions.validateEmail(email_id)){
              this.setState({ _visible: false });
            Alert.alert("Enter Valid email");
          }

          else {
            var data = {
                "Email": email_id,
            }
             this.PostToApiCalling('POST', 'ForgotPassword', Constant.URL_ForgotPassword, data);
         }
       }



    /**
     * API Calling
     */


    PostToApiCalling(method, apiKey, apiUrl, data) {
        new Promise(function (resolve, reject) {
            if (method == 'POST') {
                resolve(WebServices.callWebService(apiUrl, data));
            } else {
                resolve(WebServices.callWebService_GET(apiUrl, data));
            }
        }).then((jsonRes) => {

            if ((!jsonRes) || (jsonRes.ResponseCode == 0)) {
                this.setState({ _visible: false })
                this.apiSuccessfullResponse(apiKey, jsonRes)


            } else {

              if(Platform.OS == "ios")
              {
                _that.setState({ _visible: false })
                      setTimeout(()=>(AlertIOS.alert(
                    '',
                    jsonRes.ResponseMessage,
                    [
                      {
                        text: 'OK',
                        onPress: () => {this.setState({email_id:''})},

                      },

                    ]
                  )), 500)
                  _that.setState({ _visible: false })
              }
              else {
                Alert.alert(
                        '',
                        jsonRes.ResponseMessage,
                        [
                          {text: 'OK', onPress: () => {_that.setState({ _visible: false });this.setState({email_id:''})}},

                        ],
                        { cancelable: false }
                      );

                  _that.setState({ _visible: false })
              }
            }
        }).catch((error) => {
            console.log("ERROR" + error);
        });
    }

    apiSuccessfullResponse(apiKey, jsonRes) {
        if (apiKey == 'ForgotPassword') {

            var jsonResponse = jsonRes.ResponsePacket;
            if (jsonRes.ResponseCode = "0") {

                this.setState({ _visible: false })
                if(Platform.OS == "ios")
                {
                  _that.setState({ _visible: false })
                        setTimeout(()=>(AlertIOS.alert(
                      '',
                      jsonRes.ResponseMessage,
                      [
                        {
                          text: 'OK',
                          onPress: () => {_that.props.navigation.goBack();this.setState({email_id:''})},

                        },

                      ]
                    )), 500)
                    _that.setState({ _visible: false })
                }
                else {
                  Alert.alert(
                          '',
                          jsonRes.ResponseMessage,
                          [
                            {text: 'OK', onPress: () => {_that.setState({ _visible: false });_that.props.navigation.goBack();this.setState({email_id:''})}},

                          ],
                          { cancelable: false }
                        );

                    _that.setState({ _visible: false })
                }
            } else {

              if(Platform.OS == "ios")
              {
                _that.setState({ _visible: false })
                      setTimeout(()=>(AlertIOS.alert(
                    '',
                    jsonRes.ResponseMessage,
                    [
                      {
                        text: 'OK',
                        onPress: () => {this.setState({email_id:''})},

                      },

                    ]
                  )), 500)
                  _that.setState({ _visible: false })
              }
              else {
                Alert.alert(
                        '',
                        jsonRes.ResponseMessage,
                        [
                          {text: 'OK', onPress: () => {_that.setState({ _visible: false });this.setState({email_id:''})}},

                        ],
                        { cancelable: false }
                      );

                  _that.setState({ _visible: false })
              }

            }
        }
    }
}

export default ForgotPassword
