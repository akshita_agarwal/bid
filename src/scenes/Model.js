import React, { Component } from 'react';
import { TouchableOpacity,Keyboard, Text, View, TextInput,Button, Modal, StyleSheet } from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../themes/';

export default class Model extends Component<{}> {
  constructor(){
      super();
    this.state = {
            modalVisible: false,
            mobile_number: '',
  };
}

  openModal() {
    this.setState({modalVisible:true});
  }

  closeModal() {
    this.setState({modalVisible:false});
  }

  render() {
    return (
        <View style={styles.container}>
          <Modal visible={this.state.modalVisible} animationType={'slide'} onRequestClose={() => this.closeModal()}>

              <View style={styles.modalContainer}>
                  <View style={styles.innerContainer}>

               <View style={[AppStyles.Input_iconcnt ,{borderColor: "white"},{marginLeft : 20},{marginRight : 20}]}>
                    <View style={AppStyles.Inputtxt}>
                        <TextInput
                            autoCorrect={false}
                            underlineColorAndroid="transparent"
                            returnKeyType={"next"}
                            keyboardType={'numeric'}
                            autoFocus={false}
                            placeholder="Mobile Number"
                            placeholderTextColor={AppColors.lightGray_text}
                            style={AppStyles.InputCnt_txt}
                            onChangeText={mobile_number => this.setState({mobile_number })}
                            value={this.state.mobile_number}
                        />
                    </View>
                </View>
                <TouchableOpacity style = {AppStyles.green_round_button} onPress={() => this.closeModal()}>
                    <Text style = {{fontSize : 20,fontWeight : "bold", color : "white"}}> Close PopUp </Text>
                </TouchableOpacity>

                <TouchableOpacity style = {[AppStyles.green_round_button,{marginBottom : 30}]} onPress={() => this.closeModal()}>
                    <Text style = {{fontSize : 20,fontWeight : "bold", color : "white"}}> Close PopUp </Text>
                </TouchableOpacity>

              </View>
            </View>
          </Modal>
          <TouchableOpacity  onPress={() => this.openModal()}>
            <Text style = {{fontSize : 40 ,color : "pink", textAlign : "center",fontWeight : "bold", backgroundColor : "red"}}> Show popup </Text>
          </TouchableOpacity>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor : "yellow",
    alignItems : "center",
    justifyContent : "center"

  },
  modalContainer: {
    marginTop : (AppSizes.screen.height /4),
    alignSelf : "center",
    backgroundColor: '#D3D3D3',
    width : (AppSizes.screen.width - 10),

  },
  innerContainer: {
    alignItems: 'center',

  //  backgroundColor : "yellow",
    marginTop : 20,
  },
});
