import React, { Component } from 'react';
import { DrawerNavigator, StackNavigator } from 'react-navigation';
import Buttons, { ForgotButton, Button2, Header } from "../components/Buttons";
import * as commonFunctions from '../utils/CommonFunctions'
import {NetInfo} from 'react-native';
var Constant = require('../api/WebInteractor').Constant;
var WebServices = require('../api/WebInteractor').WebServices;

import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    Button, TouchableWithoutFeedback,
    Keyboard,
    ScrollView,
    Alert,
    AlertIOS,
    BackHandler,
    SafeAreaView,
    DeviceEventEmitter,
} from 'react-native';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import { createFilter } from 'react-native-search-filter';
import NestedScrollView from 'react-native-nested-scroll-view';
import Spinner from 'react-native-loading-spinner-overlay';
import SearchBar from 'react-native-material-design-searchbar';
import { AppStyles, AppSizes, AppColors } from '../themes/';
import SelectMultiple from 'react-native-select-multiple'
import Colors from '../themes/color';
import SendSMS from 'react-native-sms';
import sortObjectList from 'sort-object-list';
var that = "";
var arrValue = [];
var key;
var Contacts = require('react-native-contacts')
const contacts_lst = [];
const KEYS_TO_FILTERS = ['label', 'value'];
const renderLabel = (label, style) => {
    return (
        <View style={{ top: 0, left: 30 }}>
            <View style={{}}>
                <Text style={{}}>{label}</Text>
            </View>
        </View>
    )
}

class ShareAuction extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            _visible : false,
            searchText:'',
            selectedcontacts_lst: [],
            ContactsList: [],
            UserID: this.props.navigation.state.params.UserId,
            isConnected : true,

        }
        that = this;
        arrValue = [];

    }
    static navigationOptions = ({navigation, screenProps}) => ({

                 headerStyle : {

                          backgroundColor: '#89f4ef'
                 },
                 headerTintColor : "white",

                 headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.goBack()}}>
                     <Image source = {require('../themes/Images/back-arrow.png')}/>
                   </TouchableOpacity>,


    });
    componentWillMount() {

      //getting MyAuctions route key

        key = that.props.navigation.state.params.Routekey
        console.log(key);
        contacts_lst = [];
        Contacts.getAll((err, contacts) => {
            if (err === 'denied') {
              console.log("error ")
                // error
            } else {
              console.log(' intial contacts length is '+contacts.length);
                contacts.forEach(function (element) {

                    if(element.phoneNumbers.length != 0)
                    {
                          var num = element.phoneNumbers[0].number;
                          strNum = num.toString()
                          number = strNum.replace(/\s+/g,"");
                          if(commonFunctions.validateContact(number))
                          {
                            var displayName = element.givenName+" "+((element.middleName==null)?'':element.middleName)+" "+((element.familyName==null)?'':element.familyName)
                            if(!arrValue.includes(number)&& displayName!='')
                            {

                                contacts_lst.push({ label:displayName, value: number });
                                arrValue.push(number);
                            }
                          }

                    }

                });
                console.log(' contacts length is '+contacts_lst.length);

                var sortedContacts = contacts_lst.sort(function (a, b) {
                          return  a.label.toLowerCase().localeCompare(b.label.toLowerCase());
                      });

                console.log('contacts are....'+sortedContacts);
                console.log('sorted contacts length is '+sortedContacts.length);

                this.setState({
                    ContactsList: sortedContacts
                })

            }
        })
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
        DeviceEventEmitter.addListener("isConnected",this.checkConnection.bind(this));
    }
    componentWillUnmount() {

        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
        DeviceEventEmitter.removeListener("isConnected",this.checkConnection.bind(this));
    }

    //Checking the Internet Connection
    checkConnection(data){

         this.setState({isConnected : data.isConnected});
   }

   //GO BACK TO previous screen on device back button click
    handleBackButtonClick() {
        that.props.navigation.goBack();
        return true;
     }

     /*functionality of contact selection
     */

    onSelectionsChange = (selectedcontacts_lst) => {

        if (selectedcontacts_lst.length < 16) {
            this.setState({ selectedcontacts_lst })

        } else {
            alert("Maximum invitation 15 user at a time")
        }
    }

    _searchContacts=(searchText)=>{
      this.setState({searchText:searchText})


    }

    render() {
      const {ContactsList} = this.state
      var filteredContacts=[];
      if(this.state.searchText!=''){
      filteredContacts  = ContactsList.filter(createFilter(this.state.searchText, KEYS_TO_FILTERS))
    }else{
      filteredContacts=ContactsList;
    }
    console.log('filter array list length '+filteredContacts.length);
        return (
          <KeyboardAwareView animated={true} style={{backgroundColor:'white'}}>
          <SafeAreaView style={{ flex: 1,backgroundColor:'white'}}>


                <View style={[{ flex:1, backgroundColor:'white', paddingLeft: 10, paddingRight: 10}]}>
                    <Header text='SHARE AUCTION' navigation = {this.props.navigation}/>
                    <SearchBar
                        onSearchChange={(searchText) => this._searchContacts(searchText)}
                        height={40}
                        onFocus={() => console.log('On Focus')}
                        onBlur={() => console.log('On Blur')}
                        placeholder={'Search'}
                        autoCorrect={false}
                        padding={5}
                        inputStyle = {{marginTop:160}}
                        returnKeyType={'search'}
                      />

                  <View style={[{ paddingLeft: AppColors.paddingLeft10, paddingRight: AppColors.paddingRight10 }]}>

                    <View style={{ height:Platform.OS == "ios"? AppSizes.screen.height - 400:AppSizes.screen.height - 390, marginTop: 10, marginBottom: 10, borderColor: "black", borderWidth: 2 }}>
                        <ScrollView>
                          <View>
                            <View style={{ top: 5, bottom: 35, left: 10 }}>
                                <Text style={{ fontSize: 22, fontWeight: "bold", color: "black" }}> Contacts List </Text>
                            </View>
                            <SelectMultiple
                                checkboxStyle={{ left: 10, top: 0 }}
                                renderLabel={renderLabel}
                                items={filteredContacts}
                                selectedItems={this.state.selectedcontacts_lst}
                                onSelectionsChange={this.onSelectionsChange} />
                            </View>
                        </ScrollView>
                    </View>
                    <TouchableOpacity onPress={()=>{if(that.state.isConnected){this.Save()}}} style={{ borderRadius : 10,marginTop: 10, marginBottom: 15, alignItems: "center", justifyContent: "center", backgroundColor: '#f77f83', height: 40, marginTop: 10,overflow: (Platform.OS == "ios")?'hidden': null }}>
                        <Text style={{ fontSize: 20 }}> Share </Text>
                    </TouchableOpacity>
                      <Spinner visible={this.state._visible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />

                </View>
            </View>

            </SafeAreaView>
            </KeyboardAwareView>
        );
    }



    /*
          Share Btn functionality
     */
    Save = () => {
        if(this.state.selectedcontacts_lst.length > 0)
        {
              const { coreimage, email, full_name, mobile_number, Password, confirm_password, email_id, avatarSource } = this.state

                    var numberArr = [];
                    for (var i = 0; i < this.state.selectedcontacts_lst.length; i++) {
                      numberArr[i] = this.state.selectedcontacts_lst[i].value
                    }
                    console.log(numberArr)
                    var data = {
                      "MobileNumberList": numberArr.toString(),
                      "ProductID": that.props.navigation.state.params.ProductID,
                      "UserID": that.props.navigation.state.params.UserId,
                      "Plateform" : Platform.OS,
                    }
                    console.log('request data is '+JSON.stringify(data));
                    this.PostToApiCalling('POST', 'ShareAuction', Constant.URL_ShareAuction, data);

        }

      }


    /**
     * API Calling
     */


        PostToApiCalling(method, apiKey, apiUrl, data) {
            new Promise(function (resolve, reject) {
                if (method == 'POST') {
                    resolve(WebServices.callWebService(apiUrl, data));
                } else {
                    resolve(WebServices.callWebService_GET(apiUrl, data));
                }
                }).then((jsonRes) => {

                    that.setState({ _visible: false })
                    that.apiSuccessfullResponse(apiKey, jsonRes)



            }).catch((error) => {
                console.log("ERROR" + error);
            });
        }



        apiSuccessfullResponse(apiKey, jsonRes) {

            if (apiKey == 'ShareAuction') {

                var jsonResponse = jsonRes.ResponsePacket;
                if (jsonRes.ResponseCode == "0") {

                      setTimeout(()=>(Alert.alert(
                      'Share Auction',
                      jsonRes.ResponseMessage,
                      [
                        {
                          text: 'OK',onPress:()=>{that.props.navigation.goBack(key),(that.props.navigation.state.params.refreshingDetail==null)?null:that.props.navigation.state.params.refreshingDetail()}


                        },

                      ]
                    )), 500)



              }
              else if(jsonRes.ResponseCode == "1"){


                          Alert.alert(
                          'Share Auction',
                          jsonRes.ResponseMessage,
                          [
                            {
                              text: 'OK',onPress:()=>{}

                            },

                          ]
                        )

              }

              else if(jsonRes.ResponseCode == "3"){


                            setTimeout(()=>(Alert.alert(
                          'Share Auction',
                          jsonRes.ResponseMessage,
                          [
                            {
                              text: 'OK',onPress:()=>{this.SendSMS(jsonRes)}

                            },

                          ]
                        )), 500)

              }

            }
        }

        /*
          Msg Sending functionality
         */

        SendSMS = (jsonRes)=>{
          var jsonResponse = jsonRes.ResponsePacket;
          if(jsonResponse.phoneNumberList.length != 0 )
            {

                that.sendMsg(jsonRes)

            }
        }


        sendMsg = (jsonRes)=>{
          console.log("json ressponse is "+JSON.stringify(jsonRes));
          SendSMS.send({
             body: jsonRes.ResponsePacket.SMSMessage,
             recipients: jsonRes.ResponsePacket.phoneNumberList,
             successTypes: ['sent', 'queued'],
           }, (completed, cancelled, error) => {
             if(completed){
               that.SmsApi(jsonRes.ResponsePacket.Password);
               this.setState({selectedcontacts_lst:""})
             }
             console.log('SMS Callback: completed: ' + completed + ' cancelled: ' + cancelled + 'error: ' + error);
             that.props.navigation.goBack(key),(that.props.navigation.state.params.refreshingDetail==null)?null:that.props.navigation.state.params.refreshingDetail()
           });

        }

    //// Calling SmsApi

      SmsApi = (password) => {
        if(this.state.selectedcontacts_lst.length > 0)
        {
              const { coreimage, email, full_name, mobile_number, Password, confirm_password, email_id, avatarSource } = this.state
                // Loader show
                    /**
                    *  Form  Validations
                    */

                    var numberArr = [];
                    for (var i = 0; i < this.state.selectedcontacts_lst.length; i++) {
                      numberArr[i] = this.state.selectedcontacts_lst[i].value
                    }
                    console.log(numberArr)
                    var data = {
                      "MobileNumberList": numberArr.toString(),
                      "ProductID": that.props.navigation.state.params.ProductID,
                      "UserID": this.state.UserID,
                      "PlateForm": Platform.OS,
                      "Password": password,
                    }
                    this.PostToApiCalling('POST', 'InvitedUserBySMS', Constant.URL_InvitedUserBySMS, data);
      }
    }


}



export default ShareAuction;
