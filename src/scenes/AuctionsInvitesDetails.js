import React, { Component } from 'react';

import { AppStyles, AppSizes, AppColors } from '../themes/'
import Buttons, { LoginButton, Button2, Header, ShareBtn, BidderListBtn, Countdwn } from "../components/Buttons";
import Spinner from 'react-native-loading-spinner-overlay';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Button,
    ListView,
    Image,
    ScrollView,
    AsyncStorage,
    BackHandler,
    Alert,
    AlertIOS,
    TouchableOpacity,
    RefreshControl,
    SafeAreaView,
    DeviceEventEmitter,
    Modal,
    TextInput
} from 'react-native';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
var TimerMixin = require('react-timer-mixin');
var folderIcon = require('../themes/Images/folder.png')
const deleteimg = require('../themes/Images/close_red.png')
const bckimg = require('../themes/Images/bg-bottom.png')
import * as commonFunctions from '../utils/CommonFunctions'
var Constant = require('../api/WebInteractor').Constant;
var WebServices = require('../api/WebInteractor').WebServices;
import PhotoView from 'react-native-photo-view';
import TimerCountdown from 'react-native-timer-countdown'
const _that = '';
var data = [] ;
var ds;
var day = 'day';
var days = 'days';
var bidAmountString="Enter Bid Amount  ";
var product_id;


class AuctionsInvitesDetails extends React.Component {
    constructor(props) {
        super(props);
        _that = this

        this.state = {

            modalVisible:false,
            bidAmount:'',
            UserID:'',
            product_id:'',
            minBidAmount:'',
            maxBidAmount:'',
            isTimeLeft:false,
            refreshing:false,
            _visible:false,
            data:[],
            imageVisible:false,
            isConnected : true,
        };
    }
    static navigationOptions = ({navigation, screenProps}) => ({

                 headerStyle : {

                          backgroundColor: '#89f4ef'
                 },
                 headerTintColor : "white",

                 headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.goBack()}}>
                     <Image source = {require('../themes/Images/back-arrow.png')}/>
                   </TouchableOpacity>,


    });

    mixins: [TimerMixin];

    componentWillMount() {

      product_id = _that.props.navigation.state.params.ProductID
        AsyncStorage.getItem('UserData')
            .then((res) => {
                if (res) {
                    var data = JSON.parse(res)
                    if (data.isRemember) {
                        this.setState({
                            UserID: data.UserID,

                        })
                        this.FetchAuctionInvitesListDetail()
                    }
                }
            });

            BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
            DeviceEventEmitter.addListener("isConnected",this.checkConnection.bind(this));
           //called after every 3 seconds
            this.interval = setInterval(() => {

                this.LiveUpdateAuctionInvites()

          }, 3000);


    }
    componentWillUnmount() {

        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
        DeviceEventEmitter.removeListener("isConnected",this.checkConnection.bind(this));
    }


  //GO BACK TO previous screen on device back button click
  handleBackButtonClick() {
      _that.props.navigation.goBack();
      return true;
   }
   /*
   Refreshing the product list
   */

   _onRefresh() {
       this.setState({refreshing: true});
       this.FetchAuctionInvitesListDetail();
       _that.props.navigation.state.params.updateAuctionInvites()
       this.setState({refreshing: false});
   }

    //Checking the Internet Connection
   checkConnection(data){

         this.setState({isConnected : data.isConnected});
   }

    render() {

      const {data,bidAmount} = this.state
      const profile_pic = (data.ProductImage) ? { uri: data.ProductImage } : '';
      const time = (data.TimeLeft * 1000)
      var bidAmountPlaceholder = bidAmountString+"("+data.CurrencyName+")"
        return (
          <KeyboardAwareView animated={true}>
        <SafeAreaView style={{ flex: 1}}>

        <View style={{flex: 1, backgroundColor: AppColors.white, }}>
          <Image source={bckimg} style={{ width: AppSizes.screen.width, height: AppSizes.screen.height, position: 'absolute', top: 0, left: 0 }} />
          <ScrollView style={{flex: 1 }}
                      refreshControl={
                                       <RefreshControl
                                         refreshing={this.state.refreshing}
                                           onRefresh={this._onRefresh.bind(this)}
                                       />
                                 }>
              <Header text='Product Details' navigation = {this.props.navigation}/>

                    <View style={[{marginTop: 170},]}>
                        <TouchableOpacity style={AppStyles.detail_filemanager_folder_view} onPress = {()=>this.zoomImage()}>
                            <Image
                                source={profile_pic}
                                style={[AppStyles.filemanager_folder_icon,{marginLeft :5,resizeMode:'contain'},{width :AppSizes.screen.width - 30},{height:150}]}
                            />
                        </TouchableOpacity>
                        <View style={[AppStyles.detail_filemanager_folder_view,{paddingTop:10},{width : AppSizes.screen.width-80},{marginBottom : 20}]}>
                            <Text style={[AppStyles.filemanager_folder_title,{width : AppSizes.screen.width-80}]} numberOfLines={4}>Title : {data.Title} {"\n"}
                            </Text>
                            <Text style={[AppStyles.filemanager_folder_title,{width : AppSizes.screen.width-80}]} numberOfLines={4}>Total Bid : {data.TotalBid} {"\n"}
                            </Text>
                            <Text style={[AppStyles.filemanager_folder_title,{width : AppSizes.screen.width-80}]} numberOfLines={2}>Invited By User : {data.InvitedByName} {"\n"}
                            </Text>
                            <Text style={[AppStyles.filemanager_folder_title,{width : AppSizes.screen.width-80}]} numberOfLines={2}>Invited By PhoneNo. : {data.InvitedByPhoneNo} {"\n"}
                            </Text>
                            <Text style={[AppStyles.filemanager_folder_title,{width : AppSizes.screen.width-80}]} numberOfLines={4}>{data.Description} {"\n"}
                            </Text>
                            <Text style={[AppStyles.filemanager_folder_title,{width : AppSizes.screen.width-80}]} numberOfLines={4}>Min Bid Amount : {data.CurrencyName}{data.MinBidAmount} {"\n"}
                            </Text>
                            {
                              (data.MAxBidAmount==9999999999999999.00)?null
                              :<Text style={[AppStyles.filemanager_folder_title,{width : AppSizes.screen.width-80}]} numberOfLines={2}>Max Bid Amount: {data.CurrencyName}{data.MAxBidAmount} {"\n"}
                              </Text>
                            }

                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={4}>Auction Status : {(data.AuctionStatusId!=3)?"Not won":data.WonType} {"\n"}
                            </Text>
                            <Text style={[AppStyles.detail_filemanager_folder_title,{fontSize : (data.UserStatus == "Lost")?15:15},{fontWeight : (data.UserStatus == "Lost")? "bold" : "bold"}]} numberOfLines={4}>Your Status : {data.UserStatus} {"\n"}
                            </Text>
                            <Text style={[AppStyles.filemanager_folder_title,{width : AppSizes.screen.width-80}]} numberOfLines={2}>Bidders : {data.Bidders} {"\n"}
                            </Text>
                            <Text style={[AppStyles.filemanager_folder_title,{width : AppSizes.screen.width-80}]} numberOfLines={2}>Highest Bid Amount : {data.CurrencyName}{data.HighestBidAmount} {"\n"}
                            </Text>
                            {
                            (data.UserBidStatus != "")?<Text style={[AppStyles.filemanager_folder_title,{fontSize : 15,fontWeight : "bold",width : AppSizes.screen.width-80}]} numberOfLines={2}>{data.UserBidStatus} {"\n"}
                            </Text>:null
                            }
                            <Text style={[AppStyles.filemanager_folder_title,{width : AppSizes.screen.width-80}]} numberOfLines={2}>Duration : {data.Duration} {(data.Duration!=1)?days:day} {"\n"}
                            </Text>
                            {
                            (data.AuctionStatusId == 3)?
                            <View>
                            <Text style={AppStyles.detail_filemanager_folder_title}>Winner : {data.WonUserName} {"\n"}
                            </Text>
                            {
                              (data.WonUserPhoneNO != "")?
                            <Text style={AppStyles.detail_filemanager_folder_title}>Winner Mobile Number : {data.WonUserPhoneNO } {"\n"}
                            </Text>:null
                            }
                            </View>:null
                            }

                          {
                            (data.AuctionStatusId != 3)?<Text style={[AppStyles.filemanager_folder_title,{width : AppSizes.screen.width-80}]} numberOfLines={2}>Time Left : <Countdwn time={time} />  </Text>:null
                          }
                        </View>
                        <View style={{flexDirection:'row',justifyContent:"space-around"}}>
                        {
                            (data.TimeLeft != 0 && data.AuctionStatusId != 3)?(data.ReBid!=true)?<ShareBtn text="Bid" style={[AppStyles.Auctionlist_button,{fontSize : (Platform.OS == "ios")? 12 : 13},{width:100,paddingTop:10,fontWeight:'bold'},{marginBottom : 20}]} onPress = {()=>{this.setBidAmount(data.ProductID,data.MinBidAmount,data.MAxBidAmount)}} />:<ShareBtn text="Re-Bid" style={[AppStyles.Auctionlist_button,{fontSize : (Platform.OS == "ios")? 12 : 13},{width:100,paddingTop:10,fontWeight:'bold'},{marginBottom : 20}]} onPress = {()=>{if(this.state.isConnected){this.setBidAmount(data.ProductID,data.MinBidAmount,data.MAxBidAmount)}}} />:null
                        }

                       {/* {
                              (data.TimeLeft != 0 && data.AuctionStatusId != 3)?<ShareBtn text="Share Auction" onPress={() => { this.ShareAuction(data.ProductID) }} style={[AppStyles.Auctionlist_button,{fontSize : (Platform.OS == "ios")? 11 : 12}, {justifyContent:'center', backgroundColor: '#f77f83',fontWeight:'bold'},{width:100,height:38,paddingTop:10},{alignSelf: 'center'},{marginBottom:20}]} />: null
                       } */}
                      {
                        (data.TimeLeft != 0 && data.AuctionStatusId != 3)?<ShareBtn text="Exchange" onPress={() => { if(this.state.isConnected){this.exchangeProduct(data.ProductID) }}} style={[AppStyles.Auctionlist_button,{fontSize : (Platform.OS == "ios")? 11 : 12}, {justifyContent:'center', backgroundColor: '#f77f83',fontWeight:'bold' },{width:100,height:38,paddingTop:10},{alignSelf: 'center'},{marginBottom:20}]} />: null
                      }
                       </View>
                    </View>

                    <Modal visible={this.state.modalVisible} onPress = {()=>this.setState({modalVisible:!this.state.modalVisible})} animationType={'slide'} onRequestClose={() => this.closeModalNoBid()}>

                        <View style={[AppStyles.modalContainer]}>
                        <View style = {{height:45,backgroundColor:'#89f4ef',justifyContent:'center',borderBottomWidth:1,borderBottomColor:'black'}}>
                          <Text style = {{fontSize:25,fontWeight:'bold',alignSelf:'center'}}>BID</Text>
                        </View>
                            <View style={AppStyles.innerContainer}>
                            <View style={[AppStyles.filemanager_folder_view,{paddingTop:10}]}>
                                {/*<Text style={{width:200}} numberOfLines={2}>Min Bid Amount:  {data.CurrencyName}{data.MinBidAmount} {"\n"}
                              </Text>*/}
                                {
                                  (data.MAxBidAmount==9999999999999999.00)?null
                                  :  <Text style={{width:200}} numberOfLines={2}>Max Bid Amount : {data.CurrencyName}{data.MAxBidAmount} {"\n"}
                                    </Text>
                                }

                              <Text style={{width:200}} numberOfLines={2}>Min Bid Amount : {data.CurrencyName}{data.MinBidAmount} {"\n"}</Text>

                                <Text style={{width:200}} numberOfLines={2}>Last Bid Amount : {data.CurrencyName}{data.HighestBidAmount} {"\n"}</Text>



                            </View>
                                <View style={[AppStyles.Input_iconcnt, { borderColor: "black" }, { marginLeft: 20 }, { marginRight: 20 }]}>
                                    <View style={AppStyles.Inputtxt}>
                                        <TextInput
                                            autoCorrect={false}
                                            underlineColorAndroid="transparent"
                                            returnKeyType={"next"}
                                            maxLength={10}
                                            keyboardType={'numeric'}
                                            autoFocus={false}
                                            placeholder={bidAmountPlaceholder}
                                            placeholderTextColor={AppColors.lightGray_text}
                                            style={[AppStyles.InputCnt_txt, { borderColor: 'black' }]}
                                            onChangeText={bidAmount => this.setState({bidAmount})}
                                            value={bidAmount}
                                        />
                                    </View>
                                </View>

                                <View style = {{flexDirection:'row', justifyContent:'space-between',width:160,height:100,alignItems:'center'}}>
                                  <TouchableOpacity style={[{ width: 72,height:38, backgroundColor: '#f77f83' ,justifyContent:'center',borderRadius: 10}]} onPress={() => {if(this.state.isConnected){this.closeModal(bidAmount)}}}>
                                      <Text style={{ textAlign: 'center', fontSize: 12, fontWeight: "bold", color: "black" }}> Place Bid </Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity style={[{ width: 70,height:38, backgroundColor: '#f77f83' ,justifyContent:'center',borderRadius: 10}]} onPress={() => this.setState({modalVisible:!this.state.modalVisible,bidAmount:''})}>
                                      <Text style={{ textAlign: 'center', fontSize: 12, fontWeight: "bold", color: "black" }}> Cancel </Text>
                                  </TouchableOpacity>

                                </View>

                            </View>
                        </View>
                    </Modal>

                    <Modal visible={this.state.imageVisible} onPress = {()=>{this.setState({imageVisible:!this.state.imageVisible})}} animationType={'slide'} onRequestClose={() => this.closeImageModal()}>

                        <View style={{flex:1}}>

                                <View style = {{}}>
                                <TouchableOpacity onPress = {() => this.closeImageModal()} style={{ width: 40, height: 40, top: 15 ,position : 'absolute' ,alignSelf: "flex-end" }} >
                                    <Image source={deleteimg} style ={{position : 'absolute' }} />
                                 </TouchableOpacity>
                                </View>

                                <PhotoView
                                      source={profile_pic}
                                      minimumZoomScale={1}
                                      maximumZoomScale={5}
                                      androidScaleType="fitCenter"
                                      onLoad={() => console.log("Image loaded!")}
                                      style={{width:AppSizes.screen.width-20,height:AppSizes.screen.height-60,marginTop:50,resizeMode:'cover',alignSelf:'center'}} />


                        </View>
                    </Modal>
                <Spinner visible={this.state._visible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />
            </ScrollView>
            </View>
            </SafeAreaView>
            </KeyboardAwareView>
        );
    }


          // clicks to zoom Image

          zoomImage=()=>{
            this.openImageModal()
          }


          //show image pop up
          openImageModal() {
              this.setState({ imageVisible: true });
          }

          /*
          closes the pop up
          */

          closeImageModal() {
              this.setState({ imageVisible: false });
          }



          // go to ShareAuction screen with some arguments
          ShareAuction = (product_id) => {
              _that.props.navigation.navigate('ShareAuction', { UserId: this.state.UserID, ProductID: product_id });
          }
          //go to AddExchangeProduct screen
          exchangeProduct =(product_id)=>{
            _that.props.navigation.navigate('AddExchangeProduct',{ UserId: this.state.UserID, ProductID: product_id });
          }

          /**
           * Display Folders list  from API's
           */

          FetchAuctionInvitesListDetail=()=>{
              this.setState({ _visible: true })
              var data = {
                  "UserID": this.state.UserID,
                  "ProductID":product_id

              }
              this.PostToApiCalling('POST', 'GetAuctionInviteProductDetail', Constant.URL_GetAuctionInviteProductDetail, data);
              this.setState({refreshing:false})
          }

          /**
           * [LiveUpdateAuctionInvites calls GetAuctionInviteProductDetail api for Live Updating details]
           */

          LiveUpdateAuctionInvites=()=>{

            var data = {
                "UserID": this.state.UserID,
                "ProductID":product_id

            }

              this.PostToApiCalling('POST', 'GetAuctionInviteProductDetail', Constant.URL_GetAuctionInviteProductDetail, data);
          }



          setBidAmount=(ProductID,minBidAmount,maxBidAmount)=>{
            this.setState({product_id:ProductID,minBidAmount:minBidAmount,maxBidAmount:maxBidAmount})
            this.openModal()

          }

          openModal() {
              this.setState({ modalVisible: true });
          }

          closeModal(bidAmount) {
              if (_that.state.bidAmount.length != 0) {
                  //this.setState({ modalVisible: false });

                  if (!commonFunctions.validateAmount(bidAmount)) {

                      commonFunctions.message("Please enter correct bid amount for item.")
                  }else if(parseFloat(bidAmount)>=parseFloat(this.state.minBidAmount)&&parseFloat(bidAmount)<=parseFloat(this.state.maxBidAmount))
                  {
                      this.setState({bidAmount:bidAmount})
                      this.setBidApiCalled()
                      this.setState({bidAmount:''})
                  }
                  else{
                    setTimeout(()=>{alert('Bid amount not between min bid and max bid')},300)

                  }

                  this.setState({bidAmount:''})

              }
              else{
                setTimeout(()=>{commonFunctions.message('Please enter bid amount')},200)

              }
          }

          closeModalNoBid=()=>{
            this.setState({ modalVisible: false });
          }

          setBidApiCalled=()=>{


            const {bidAmount,UserID,product_id} = this.state
            var data = {

                  "ID": 1,
                  "ProductID": product_id,
                  "UserID": UserID,
                  "BidAmount": bidAmount,

            }

            this.PostToApiCalling('POST', 'SetBidAmount', Constant.URL_SetBidAmount, data);

          }



          /**
          * API Calling
          */


          PostToApiCalling(method, apiKey, apiUrl, data) {

              new Promise(function (resolve, reject) {

                  if (method == 'POST') {
                      resolve(WebServices.callWebService(apiUrl, data));
                  } else {
                      resolve(WebServices.callWebService_GET(apiUrl, data));
                  }
              }).then((jsonRes) => {

                  if ((!jsonRes) || (jsonRes.ResponseCode == 0)) {

                   this.setState({ _visible: false })
                      this.apiSuccessfullResponse(apiKey, jsonRes)


                  } else {
                      commonFunctions.message(jsonRes.ResponseMessage)
                      this.setState({ _visible: false })
                  }
              })
              .catch((error) => {
                  console.log("ERROR" + error);
              })
          }

          apiSuccessfullResponse(apiKey, jsonRes) {

            if(apiKey == 'GetAuctionInviteProductDetail')
            {
               //this.setState({ _visible: false })
                  var jsonResponse = jsonRes.ResponsePacket;

                  if (jsonResponse.length == 0) {




                           setTimeout(()=>(Alert.alert(
                                    '',
                                    'No record found',
                                    [
                                      {text: 'OK', onPress: () => {this.props.navigation.goBack()}},

                                    ],
                                    { cancelable: false }
                                  )),1000);



                  } else {
                      // commonFunctions.message(jsonRes.ResponseMessage)

                      this.setState({ _visible: false, data: jsonResponse })
                  }

          } else if(apiKey == 'SetBidAmount'){
              this.setState({ _visible: false })

                  var jsonResponse= jsonRes.ResponsePacket;

                  if(jsonRes.ResponseCode == 0)
                  {
                    this.setState({ modalVisible: false });
                     _that.setState({ _visible: false })
                    if(Platform.OS == "ios")
                    {
                        _that.setState({ _visible: false })
                            setTimeout(()=>(AlertIOS.alert(
                          'Auction Invites',
                          jsonRes.ResponseMessage,
                          [
                            {
                              text: 'OK',
                              onPress: () => {_that.props.navigation.goBack(),_that.props.navigation.state.params.updateAuctionInvites()},

                            },

                          ]
                        )), 1000)


                    }
                    else {
                      Alert.alert(
                              'Auction Invites',
                              jsonRes.ResponseMessage,
                              [
                                {text: 'OK', onPress: () => {_that.setState({ _visible: false });_that.props.navigation.goBack(),_that.props.navigation.state.params.updateAuctionInvites()}},

                              ],
                              { cancelable: false }
                            );

                        _that.setState({ _visible: false })
                    }

                  }
                  else{
                    this.setState({ modalVisible: true });
                  }

                }


            }


      }

export default AuctionsInvitesDetails
