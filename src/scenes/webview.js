import React, { Component } from 'react';
import { WebView,View,Image} from 'react-native';
import { AppStyles, AppSizes, AppColors } from '../themes/';
import Buttons, { Header, } from "../components/Buttons";
import Spinner from 'react-native-loading-spinner-overlay';

const bckimg = require('../themes/Images/bg-bottom.png')

export default class webview extends Component {
    constructor(props) {
        super(props)
        this.state = {
          vivible : false
        }
    }
    componentWillMount(){
        // this.props.screenProps.setParams({
        //     myTitle: this.props.myTitle
        // })
    }
    showSpinner() {
        console.log('Show Spinner');
        this.setState({ visible: true });
    }

    hideSpinner() {
        console.log('Hide Spinner');
        this.setState({ visible: false });
    }

    render() {

      var {headerTxt} = this.props.navigation.state.params;
      var txt = (headerTxt == undefined)? 'Terms and Conditions' : headerTxt;

                return (
        <View style={{flex: 1, backgroundColor: AppColors.white, marginTop :0}}>
          <Image source={bckimg} style={{ width: 500, height: 500, position: 'absolute', top: 0, left: 0 }} />
        <Header text={txt} navigation = {this.props.navigation}/>

        <Spinner
                  visible={this.state.visible}
                  textContent={'User guide loading..'}
                  textStyle={{ color: '#FFF' }}
              />
            <WebView
                source={{ uri: this.props.navigation.state.params.url}}
                style={{ marginTop: 180,flex:1 }}
                onLoadStart={() => (this.showSpinner())}
               onLoad={() => (this.hideSpinner())}
            />
        </View>
        );
    }
}
