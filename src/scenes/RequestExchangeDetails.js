import React, { Component } from 'react';

import { AppStyles, AppSizes, AppColors } from '../themes/'
import Buttons, { LoginButton, Button2, Header, ShareBtn, BidderListBtn, Countdwn } from "../components/Buttons";
import Spinner from 'react-native-loading-spinner-overlay';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Button,
    ListView,
    Image,
    ScrollView,
    AsyncStorage,
    BackHandler,
    Alert,
    AlertIOS,
    TouchableOpacity,
    RefreshControl,
    SafeAreaView,
    DeviceEventEmitter,
    Modal,

} from 'react-native';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import PhotoView from 'react-native-photo-view';
var folderIcon = require('../themes/Images/folder.png')
const bckimg = require('../themes/Images/bg-bottom.png')
import * as commonFunctions from '../utils/CommonFunctions'
var Constant = require('../api/WebInteractor').Constant;
var WebServices = require('../api/WebInteractor').WebServices;
import TimerCountdown from 'react-native-timer-countdown';
const deleteimg = require('../themes/Images/close_red.png')

const _that = '';

var day = 'day';
var days = 'days';

class RequestExchangeDetails extends React.Component {
    constructor(props) {
        super(props);
        _that = this

        this.state = {
            _visible : false,
            refreshing: false,
            data:[],
            imageVisible:false,
            isAccepted : "",
            isConnected : true
        };
    }

    componentWillMount() {


        AsyncStorage.getItem('UserData')
            .then((res) => {
                if (res) {
                    var data = JSON.parse(res)
                    if (data.isRemember) {
                        this.setState({
                            UserID: data.UserID
                        })

                    }
                }
            });

            this.FetchExchangeProductDetails()

            DeviceEventEmitter.addListener("isConnected",this.checkConnection.bind(this));
            BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    componentWillUnmount() {
        DeviceEventEmitter.removeListener("isConnected",this.checkConnection.bind(this));
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    //GO BACK TO previous screen on device back button click

  handleBackButtonClick() {
      _that.props.navigation.goBack();
      return true;
   }
   /*
   Refreshing the product list
   */
   _onRefresh() {
     if(_that.state.isConnected){
       this.setState({refreshing: true});
       this.FetchExchangeProductDetails()
     }
   }
   //Checking the Internet Connection
   checkConnection(data){

        this.setState({isConnected : data.isConnected});
  }


    render() {

      const {data} = this.state

      const profile_pic = (data.ExchangeProductImage) ? { uri: data.ExchangeProductImage } : '';
      const time = (data.TimeLeft * 1000)
        return (
            <KeyboardAwareView animated={true}>
           <SafeAreaView style={{ flex: 1}}>
           <View style={{flex: 1, backgroundColor: AppColors.white, }}>
            <Image source={bckimg} style={{ width: AppSizes.screen.width, height: AppSizes.screen.height, position: 'absolute', top: 0, left: 0 }} />

              <ScrollView refreshControl={
                               <RefreshControl
                                 refreshing={this.state.refreshing}
                                   onRefresh={this._onRefresh.bind(this)}
                               />
                         }>
                <Header text='Exchange Product Details' navigation = {this.props.navigation}/>



                    <View style={[{marginTop: 170}]}>
                        <TouchableOpacity style={AppStyles.detail_filemanager_folder_view} onPress = {()=>this.zoomImage()}>
                            <Image
                                source={profile_pic}
                                style={[AppStyles.filemanager_folder_icon,{marginLeft :5,resizeMode:'contain'},{width :AppSizes.screen.width - 30},{height:200}]}
                            />
                        </TouchableOpacity>
                        <View style={[AppStyles.detail_filemanager_folder_view,{paddingTop:10},{width : AppSizes.screen.width-80},{marginBottom : 20}]}>
                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Title : {data.ExchangeProductTitle} {"\n"}
                            </Text>
                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={4}>{data.ExchangeProductDescription} {"\n"}
                            </Text>
                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Duration : {data.DurationOfAuctionInDays} {(data.Duration!=1)?days:day} {"\n"}
                            </Text>
                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={4}>Auction Status : {(data.AuctionStatusId!=3)?"Not Won":data.WonType} {"\n"}
                            </Text>
                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Exchange Offer By : {data.ExchangeProductUserName}  {"\n"}
                            </Text>
                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Exchange Offer By PhoneNo : {data.ExchangeProductUserPhoneNo} {"\n"}
                            </Text>
                            <Text style={[AppStyles.detail_filemanager_folder_title,{fontWeight:'bold'}]} numberOfLines={2}>Request Status : {(data.AuctionStatusId==3 && data.IsAccepted != true)?"lost":(data.IsAccepted)?"Accepted":(data.IsAccepted==null)?"In Progress":"Declined"} {"\n"}
                            </Text>
                            {
                              (data.IsAccepted!=true)?<Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Time Left : <Countdwn time={time} /></Text>:null
                            }
                        </View>
                        <View style = {{flexDirection:"row",justifyContent:"space-around"}} >

                        <View style={[AppStyles.filemanager_folder_view,{paddingTop:3}]}>
                        {
                          (data.AuctionStatusId!=3 && data.IsAccepted == null)?<ShareBtn text="Accept offer" onPress={()=>this.AcceptExchangeProduct(data.ExchangeProductID,true)} style={[AppStyles.Auctionlist_button,{fontSize : (Platform.OS == "ios")? 11 : 12}, {justifyContent:'center', fontWeight:'bold',backgroundColor: '#f77f83' },{width:100,height:38,paddingTop:10},{alignSelf: 'center'},{marginBottom:20}]} />:null
                        }
                        </View>
                        <View style={[AppStyles.filemanager_folder_view,{paddingTop:3}]}>
                        {
                          (data.AuctionStatusId!=3 && data.IsAccepted == null)?<ShareBtn text="Decline" onPress={()=>this.AcceptExchangeProduct(data.ExchangeProductID,false)} style={[AppStyles.Auctionlist_button,{fontSize : (Platform.OS == "ios")? 11 : 12}, {justifyContent:'center', fontWeight:'bold',backgroundColor: '#f77f83' },{width:100,height:38,paddingTop:10},{alignSelf: 'center'},{marginBottom:20}]} />:null
                        }
                        </View>

                      </View>

                    </View>

                    <Modal visible={this.state.imageVisible} onPress = {()=>{this.setState({imageVisible:!this.state.imageVisible})}} animationType={'slide'} onRequestClose={() => this.closeImageModal()}>
                      <View style={{flex:1}}>

                                <View style = {{}}>
                                <TouchableOpacity onPress = {() => this.closeImageModal()} style={{ width: 40, height: 40, top: 15 ,position : 'absolute' ,alignSelf: "flex-end" }} >
                                    <Image source={deleteimg} style ={{position : 'absolute' }} />
                                 </TouchableOpacity>
                                </View>

                                <PhotoView
                                      source={profile_pic}
                                      minimumZoomScale={1}
                                      maximumZoomScale={5}
                                      androidScaleType="fitCenter"
                                      onLoad={() => console.log("Image loaded!")}
                                      style={{width:AppSizes.screen.width-20,height:AppSizes.screen.height-60,marginTop:50,resizeMode:'cover',alignSelf:'center'}} />
                        </View>
                    </Modal>


                <Spinner visible={this.state._visible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />
            </ScrollView>
            </View>
            </SafeAreaView>
            </KeyboardAwareView>
        );
    }

    // clicks to zoom Image
    zoomImage=()=>{
      this.openImageModal()
    }


    //show image pop up
    openImageModal() {
        this.setState({ imageVisible: true });
    }


    /*
    closes the pop up
    */

    closeImageModal() {
        this.setState({ imageVisible: false });
    }

    //Accepts the exchange offer
    AcceptExchangeProduct=(ExchangeProductID,isAccept)=>{
      this.setState({isAccepted : isAccept}),
      this.setState({ _visible: true })
      var data = {
          "ExchangeProductID": ExchangeProductID,
          "IsAccepted" : isAccept,
      }

      this.PostToApiCalling('POST', 'AcceptExchangeProduct', Constant.URL_AcceptExchangeProduct, data);
    }

      //Fetches the exchange Product detail
    FetchExchangeProductDetails=()=>{
      var {ExchangeProductID} = _that.props.navigation.state.params
      this.setState({ _visible: true })
      var data = {
          "ExchangeProductID": ExchangeProductID
      }


      this.PostToApiCalling('POST', 'ExchangeProductDetail', Constant.URL_ExchangeProductDetail, data);
      this.setState({refreshing:false})
    }

    /**
    * API Calling
    */


    PostToApiCalling(method, apiKey, apiUrl, data) {
        new Promise(function (resolve, reject) {
            if (method == 'POST') {
                resolve(WebServices.callWebService(apiUrl, data));
            } else {
                resolve(WebServices.callWebService_GET(apiUrl, data));
            }
        }).then((jsonRes) => {
            if ((!jsonRes) || (jsonRes.ResponseCode == 0)) {
                this.apiSuccessfullResponse(apiKey, jsonRes)
                this.setState({ _visible: false })

            } else {
                commonFunctions.message(jsonRes.ResponseMessage)
                this.setState({ _visible: false })
            }
        }).catch((error) => {
            console.log("ERROR" + error);
        });
    }

    apiSuccessfullResponse(apiKey, jsonRes) {

        if (apiKey == 'ExchangeProductDetail') {
            var jsonResponse = jsonRes.ResponsePacket;

            if (jsonResponse.length == 0)
            {
              if(this.state.searchText != "")
               {
                   this.setState({_visible: false})
               }
                 else{
                   if(Platform.OS=="ios")
                 {
                       setTimeout(()=>(AlertIOS.alert(
                       'Auction List',
                       'No record found',
                       [
                         {
                           text: 'OK',
                           onPress: () => {_that.props.navigation.goBack()},

                         },

                       ]
                     )), 10)
                     _that.setState({ _visible: false })
                     }
                     else {
                       Alert.alert(
                               'Auction List',
                               'No record found',
                               [
                                 {text: 'OK', onPress: () => {this.props.navigation.goBack()}},

                               ],
                               { cancelable: false }
                             )
                              _that.setState({ _visible: false })
                     }
                 }
            } else {

                this.setState({ _visible: false, data: jsonResponse, })
            }

        }else if (apiKey == 'AcceptExchangeProduct') {


              this.setState({ _visible: false })
                  var jsonResponse= jsonRes.ResponsePacket;

                  if(jsonRes.ResponseCode == 0)
                  {
                     _that.setState({ _visible: false })
                    if(Platform.OS == "ios")
                    {
                        _that.setState({ _visible: false })
                            setTimeout(()=>(AlertIOS.alert(
                          '',
                          jsonRes.ResponseMessage,
                          [
                            {
                              text: 'OK',
                              onPress: () => {_that.props.navigation.goBack()},

                            },

                          ]
                        )), 1000)


                    }
                    else {
                      Alert.alert(
                              '',
                              jsonRes.ResponseMessage,
                              [
                                {text: 'OK', onPress: () => {_that.setState({ _visible: false });_that.props.navigation.goBack()}}

                              ],
                              { cancelable: false }
                            );

                        _that.setState({ _visible: false })
                    }

                  }
                  else{}

                }




        }


}

export default RequestExchangeDetails
