import React, { Component } from 'react';

import {
    Platform,
    StyleSheet,
    Text,
    View,
    Button,
    TouchableWithoutFeedback,
    Keyboard,
    Image,
    TouchableOpacity,
    TextInput,
    ScrollView,
    AsyncStorage,
    BackHandler,
    Alert,
    AlertIOS,
    SafeAreaView,
    StatusBar,
    DeviceEventEmitter,
} from 'react-native';


import { KeyboardAwareView } from 'react-native-keyboard-aware-view';

import { AppStyles, AppSizes, AppColors } from '../themes/';

import Colors from '../themes/color';

import Buttons, { LoginButton, Button2, Header } from "../components/Buttons";

import Spinner from 'react-native-loading-spinner-overlay';

var ImagePicker = require('react-native-image-picker');

import * as commonFunctions from '../utils/CommonFunctions'

var Constant = require('../api/WebInteractor').Constant;

var WebServices = require('../api/WebInteractor').WebServices;

var _that = "";

class AddExchangeProduct extends React.Component {

  constructor(props) {
      super(props)

      this.state = {
          _title: "",
          _description: "",
          avatarSource: '',
          _coreimage: "",
          visible: false,
          UserID:this.props.navigation.state.params.UserId,
          ProductID:this.props.navigation.state.params.ProductID,
          isConnected : true,
      }
      _that = this;
  }

  //handles back button in android
  componentWillMount() {
          BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
          DeviceEventEmitter.addListener("isConnected",this.checkConnection.bind(this));
  }

  componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
      DeviceEventEmitter.removeListener("isConnected",this.checkConnection.bind(this));
  }

//GO BACK TO previous screen on device back button click
  handleBackButtonClick() {
      _that.props.navigation.goBack(null);
      return true;
  }
  //Checking the net connection
  checkConnection(data){

        this.setState({isConnected : data.isConnected});
  }

  /*
      renders Exchange product screen
  */

  render() {
      const { _title, _description, avatarSource } = this.state
      return (
        <KeyboardAwareView animated={true}>
          <View style={{ flex: 1,backgroundColor:'white'}}>
        <SafeAreaView style={{ flex: 1,backgroundColor:'white'}}>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <ScrollView>
                <View style={[AppStyles.container, { paddingLeft: AppColors.paddingLeft30, paddingRight: AppColors.paddingRight30 }]}>
                    {
                      (this.props.navigation.state.params.updateData==null)?<Header text='Exchangeable Product' navigation = {this.props.navigation} />:<Header text='Update Product Info' navigation = {this.props.navigation}/>
                    }
                    <View style={[AppStyles.Input_iconcnt, { marginTop: 180 }]}>
                        <View style={AppStyles.Inputtxt}>
                            <TextInput
                                autoCorrect={false}
                                underlineColorAndroid="transparent"
                                returnKeyType={"next"}
                                blurOnSubmit={false}
                                autoFocus={false}
                                placeholder="Title"
                                placeholderTextColor={AppColors.lightGray_text}
                                style={AppStyles.InputCnt_txt}
                                ref="_title"
                                maxLength = {50}
                                autoCapitalize = "none"
                                onChangeText={_title => this.setState({ _title })}
                                value={_title}
                                onSubmitEditing={() => { this.refs._description.focus(); }}
                            />
                        </View>
                    </View>



                    <View style={{
                    borderColor: Colors.lightGray_text,
                    borderWidth: 1,
                    borderRadius: 10,
                   marginTop: Colors.marginTop10}}>

                            <TextInput
                                autoCorrect={false}
                                underlineColorAndroid="transparent"
                                returnKeyType={"next"}
                                autoFocus={false}
                                placeholder="Description"
                                placeholderTextColor={AppColors.lightGray_text}
                                style={{fontSize : 18,marginLeft : Colors.marginLeft10,height : 100,fontFamily: Colors.fontFamilyRegular}}
                                ref="_description"
                                multiline={true}
                                blurOnSubmit = {true}
                                numberOfLines={3}
                                maxLength = {50}
                                autoCapitalize = "none"
                                onChangeText={_description => this.setState({ _description })}
                                value={_description}

                            />

                    </View>
                    <LoginButton text={"Select Image"} onPress={this.PickerImage.bind()} style={AppStyles.green_round_button} />
                    <Image source={avatarSource} style={[AppStyles.login_logo,{height : this.state.imageHeight},{marginTop : 30}]} />
                  <LoginButton text={"Save"} onPress={()=>{if(_that.state.isConnected){this.Save()}}} style={[AppStyles.green_round_button,{marginBottom : 40}]} />
                    <Spinner visible={this.state._visible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />
                </View>

            </ScrollView>
        </TouchableWithoutFeedback>

        </SafeAreaView>

        </View>
      </KeyboardAwareView>
      );
  }

  /*
      Image picker shows
  */

  PickerImage = () => {
      // More info on all the options is below in the README...just some common use cases shown here
      const options = {
          quality: 1.0,
          maxWidth: 500,
          maxHeight: 500,
      };

      /**
               * The first arg is the options object for customization (it can also be null or omitted for default options),
               * The second arg is the callback which sends object: response (more info below in README)
               */
      ImagePicker.showImagePicker(options, (response) => {
          console.log('Response = ', response);

          if (response.didCancel) {
              console.log('User cancelled image picker');
          }
          else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
          }
          else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
          }
          else {

              let source = { uri: 'data:image/png;base64,' + response.data };
              this.setState({
                  avatarSource: source,
                  _coreimage: response.data
              });
              if(this.state.avatarSource != null)
              {
              this.setState({imageHeight : 150});
              }
          }
      });
  }

  /**
    add Exchangeable product
  */

  Save = () => {
      const {  _title,  _description ,_coreimage,UserID,ProductID} = this.state
      // Loader show
      this.setState({ _visible: true })

      /**
       *  Form  Validations
       */

      if (_title == "") {
          this.setState({ _visible: false });
          commonFunctions.message("Please Enter Title of item")
      } else if (commonFunctions.validateName(_title)) {
          this.setState({ _visible: false });
          commonFunctions.message("Please enter Correct Title of item")
      }else if (_description == "") {
          this.setState({ _visible: false });
          commonFunctions.message("Please enter short description of item")
      } else if (_coreimage == "") {
          this.setState({ _visible: false });
          commonFunctions.message("Please select an image of the item")
      }
    else {
          var data = {

              "UserID": UserID,
              "ProductID":ProductID,
              "Title": _title,
              "Description": _description,
              "ExchangeProductImage": _coreimage,

          }
          this.PostToApiCalling('POST', 'AddUpdateExchangeProduct', Constant.URL_AddUpdateExchangeProduct, data);

      }

  }


  /**
   * API Calling
   */


  PostToApiCalling(method, apiKey, apiUrl, data) {
      new Promise(function (resolve, reject) {
          if (method == 'POST') {
              resolve(WebServices.callWebService(apiUrl, data));
          } else {
              resolve(WebServices.callWebService_GET(apiUrl, data));
          }
      }).then((jsonRes) => {
          if ((!jsonRes) || (jsonRes.ResponseCode == 0)) {
              this.apiSuccessfullResponse(apiKey, jsonRes)
              this.setState({ _visible: false })

          } else {
              commonFunctions.message(jsonRes.ResponseMessage)
              this.setState({ _visible: false })
          }
      }).catch((error) => {
          console.log("ERROR" + error);
      });
  }

  apiSuccessfullResponse(apiKey, jsonRes) {
      if (apiKey == 'AddUpdateExchangeProduct') {
          var jsonResponse = jsonRes.ResponsePacket;
          console.log(jsonResponse)
          if (jsonRes.ResponseCode = "0") {
            if(Platform.OS == "ios")
            {
              _that.setState({ _visible: false })
                    setTimeout(()=>(AlertIOS.alert(
                  '',
                  jsonRes.ResponseMessage,
                  [
                    {
                      text: 'OK',
                      onPress: () => {_that.props.navigation.goBack()},

                    },

                  ]
                )), 500)
                _that.setState({ _visible: false })
            }
            else {
              Alert.alert(
                      '',
                      jsonRes.ResponseMessage,
                      [
                        {text: 'OK', onPress: () => {_that.setState({ _visible: false });_that.props.navigation.goBack()}},

                      ],
                      { cancelable: false }
                    );

                _that.setState({ _visible: false })
            }

          } else {

            if(Platform.OS == "ios")
            {
              _that.setState({ _visible: false })
                    setTimeout(()=>(AlertIOS.alert(
                  '',
                  jsonRes.ResponseMessage,
                  [
                    {
                      text: 'OK',
                      onPress: () => {_that.props.navigation.goBack()},

                    },

                  ]
                )), 500)
                _that.setState({ _visible: false })
            }
            else {
              Alert.alert(
                      '',
                      jsonRes.ResponseMessage,
                      [
                        {text: 'OK', onPress: () => {_that.setState({ _visible: false });_that.props.navigation.goBack()}},

                      ],
                      { cancelable: false }
                    );

                _that.setState({ _visible: false })
            }

          }
      }
}



}

export default AddExchangeProduct
