import React, { Component } from 'react';

import { AppStyles, AppSizes, AppColors } from '../themes/'
import Buttons, { LoginButton, Button2, Header, ShareBtn, BidderListBtn, Countdwn } from "../components/Buttons";
import Spinner from 'react-native-loading-spinner-overlay';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Button,
    ListView,
    Image,
    ScrollView,
    AsyncStorage,
    BackHandler,
    Alert,
    AlertIOS,
    TouchableOpacity,
    RefreshControl,
    SafeAreaView,
    DeviceEventEmitter,
    Modal
} from 'react-native';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';

var folderIcon = require('../themes/Images/folder.png')
const bckimg = require('../themes/Images/bg-bottom.png')
import * as commonFunctions from '../utils/CommonFunctions'
var Constant = require('../api/WebInteractor').Constant;
var WebServices = require('../api/WebInteractor').WebServices;
import TimerCountdown from 'react-native-timer-countdown';
import PhotoView from 'react-native-photo-view';
const deleteimg = require('../themes/Images/close_red.png')
const _that = '';
var product_id ;
var data=[];
var day = 'day';
var days = 'days';
var key;
class ExchangeProductDetails extends React.Component {
    constructor(props) {
        super(props);
        _that = this

        this.state = {
            _visible : false,
            isBidderExist:false,
            boxHeight:315,
            isBack:false,
            WinProductId:'',
            refreshing: false,
            data:[],
            modalVisible:false
        };
    }

    componentWillMount() {

          exchangeProduct_id = this.props.navigation.state.params.ExchangeProductID;
          key = this.props.navigation.state.key
        AsyncStorage.getItem('UserData')
            .then((res) => {
                if (res) {
                    var data = JSON.parse(res)
                    if (data.isRemember) {
                        this.setState({
                            UserID: data.UserID
                        })
                        this.fetchGetMyAcceptedExchangeProductDetail()
                    }
                }
            });


               DeviceEventEmitter.addListener('RefreshManageRequest', function (data) {
                   this.props.navigation.state.params.updateMyAuction()


               }.bind(this));




            BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    componentWillUnmount() {

        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    //GO BACK TO previous screen on device back button click
    handleBackButtonClick() {
        _that.props.navigation.goBack();
        return true;
     }

     /*
     Refreshing the product list
     */
   _onRefresh() {
       this.setState({refreshing: true});
       this.FetchGetMyAcceptedExchangeProductDetail()
       _that.props.navigation.state.params.updateMyAuction()

   }

   /*
    Rendering the ExchangeProductDetails
    */


    render() {
      const {data} = this.state
      const profile_pic = (data.ExchangeProductImage) ? { uri: data.ExchangeProductImage } : '';
      const time = (data.TimeLeft * 1000)
        return (
            <KeyboardAwareView animated={true}>
           <SafeAreaView style={{ flex: 1}}>
           <View style={{flex: 1, backgroundColor: AppColors.white, }}>
            <Image source={bckimg} style={{ width: AppSizes.screen.width, height: AppSizes.screen.height, position: 'absolute', top: 0, left: 0 }} />

              <ScrollView style={{flex: 1 }}
                          refreshControl={
                                           <RefreshControl
                                             refreshing={this.state.refreshing}
                                               onRefresh={this._onRefresh.bind(this)}
                                           />
                                     }>
                <Header text='Exchange Product Details' navigation = {this.props.navigation}/>



                    <View style={[{marginTop: 170}]}>
                        <TouchableOpacity style={AppStyles.detail_filemanager_folder_view} onPress = {()=>this.zoomImage()}>
                            <Image
                                source={profile_pic}
                                style={[AppStyles.filemanager_folder_icon,{marginLeft :5,resizeMode:'contain'},{width :AppSizes.screen.width - 30},{height:150}]}
                            />
                        </TouchableOpacity>

                        <View style={[AppStyles.detail_filemanager_folder_view,{paddingTop:10},{width : AppSizes.screen.width-80},{marginBottom : 20}]}>
                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Exchange Product Title : {data.ExchangeProductTitle} {"\n"}
                            </Text>
                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={4}>Exchange Product Description : {data.ExchangeProductDescription} {"\n"}
                            </Text>
                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Min Bid Amount : {data.CurrencyName}{data.MinBidAmount} {"\n"}
                            </Text>
                            {
                              (data.MAxBidAmount==9999999999999999.00)?null
                              :<Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Max Bid Amount: {data.CurrencyName}{data.MAxBidAmount} {"\n"}
                              </Text>
                            }

                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Owner Name : {data.ProductOwnerName} {"\n"}
                            </Text>
                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Owner Mobile Number : {data.ProductOwnerPhoneNo} {"\n"}
                            </Text>
                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Invited Product Title : {data.ProductTitle} {"\n"}
                            </Text>
                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Invited Product Description : {data.ProductDescription} {"\n"}
                            </Text>
                            <Text style={[AppStyles.detail_filemanager_folder_title,{fontWeight:'bold'}]} numberOfLines={2}>Product Status : {((data.WonType=="Won" || data.WonType=="Won by Exchange") && (data.IsAccepted != true) )?"Lost":(data.IsAccepted)?"Accepted":(data.IsAccepted==null)?"In Progress":"Declined"} {"\n"}
                            </Text>
                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Duration : {data.DurationOfAuctionInDays} {(data.DurationOfAuctionInDays!=1)?days:day} {"\n"}
                            </Text>
                          {
                              (data.AuctionStatus != 3)?<Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Time Left : <Countdwn time={time} /></Text>:null
                          }
                        </View>

                    </View>

                    <Modal visible={this.state.modalVisible} onPress = {()=>{this.setState({modalVisible:!this.state.modalVisible})}} animationType={'slide'} onRequestClose={() => this.closeModal()}>

                        <View style={{flex:1}}>

                                <View style = {{}}>
                                <TouchableOpacity onPress = {()=>this.closeModal()} style={{ width: 40, height: 40, top: 15 ,position : 'absolute' ,alignSelf: "flex-end" }} >
                                    <Image source={deleteimg} style ={{position : 'absolute' }} />
                                 </TouchableOpacity>
                                </View>

                                <PhotoView
                                      source={profile_pic}
                                      minimumZoomScale={1}
                                      maximumZoomScale={5}
                                      androidScaleType="fitCenter"
                                      onLoad={() => console.log("Image loaded!")}
                                      style={{width:AppSizes.screen.width-20,height:AppSizes.screen.height-60,marginTop:50,resizeMode:'cover',alignSelf:'center'}} />


                        </View>
                    </Modal>

                <Spinner visible={this.state._visible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />
            </ScrollView>
            </View>
            </SafeAreaView>
            </KeyboardAwareView>
        );
    }

    // clicks to zoom Image

    zoomImage=()=>{
      this.openModal()
    }

    /*
      opens pop up
    */
    openModal=()=> {
        this.setState({ modalVisible: true });
    }

    /*
    closes the pop up
    */

    closeModal=()=> {
        this.setState({ modalVisible: false });

    }


      fetchGetMyAcceptedExchangeProductDetail=()=>{
        this.setState({ _visible: true })
        var data = {

            "ExchangeProductID":exchangeProduct_id

        }

        this.PostToApiCalling('POST', 'GetMyAcceptedExchangeProductDetail', Constant.URL_GetMyAcceptedExchangeProductDetail, data);
        this.setState({refreshing:false})
      }

      /**
      * API Calling
      */


      PostToApiCalling(method, apiKey, apiUrl, data) {

          new Promise(function (resolve, reject) {

              if (method == 'POST') {
                  resolve(WebServices.callWebService(apiUrl, data));
              } else {
                  resolve(WebServices.callWebService_GET(apiUrl, data));
              }
          }).then((jsonRes) => {

              if ((!jsonRes) || (jsonRes.ResponseCode == 0)) {

               this.setState({ _visible: false })
                  this.apiSuccessfullResponse(apiKey, jsonRes)


              } else {
                  commonFunctions.message(jsonRes.ResponseMessage)
                  this.setState({ _visible: false })
              }
          })
          .catch((error) => {
              console.log("ERROR" + error);
          })
      }

      apiSuccessfullResponse(apiKey, jsonRes) {

        if(apiKey == 'GetMyAcceptedExchangeProductDetail')
        {
           //this.setState({ _visible: false })
              var jsonResponse = jsonRes.ResponsePacket;
              console.log('get exchange product details data is '+jsonResponse.IsAccepted);

              if (jsonResponse.length == 0) {




                       setTimeout(()=>(Alert.alert(
                                '',
                                'No record found',
                                [
                                  {text: 'OK', onPress: () => {this.props.navigation.goBack()}},

                                ],
                                { cancelable: false }
                              )),1000);



              } else {


                  this.setState({ _visible: false, data: jsonResponse })
              }

      }

    }




}

export default ExchangeProductDetails
