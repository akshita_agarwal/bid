/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

 /**
  * Sample React Native App
  * https://github.com/facebook/react-native
  * @flow
  */

 import React, { Component } from 'react';
 import { StackNavigator } from 'react-navigation';
 import Login from './Login';
 import Details from './Details';
 import Buttons, { Button1, Button2 } from "../components/Buttons";
 import { SYMBOL } from "../Constants";
 import Home from './Home';
 import AddNewProduct from './AddNewProduct';
 import MyAuctions from './MyAuctions';
 import AuctionsInvites from './AuctionsInvites';
 import AuctionsInvitesDetails from './AuctionsInvitesDetails';
 import AuctionsWon from './AuctionsWon';
 import ShareAuction from './ShareAuction';
 import Registration from './Registration';
 import Model from './Model';
 import ForgotPassword from './ForgotPassword';
 import ViewProfile from './ViewProfile';
 import BidderList from './BidderList';
 import MyExchange from './MyExchange'
 import AddExchangeProduct from './AddExchangeProduct';
 import ExchangeRequests from './ExchangeRequests';
 import RequestExchangeDetails from './RequestExchangeDetails';
 import WonDetails from './AuctionWonDetails';
 import webview from './webview';
 import OpenPDF from './OpenPDF';
 import ExchangeProductDetails from './ExchangeProductDetails'
 import SplashScreen from 'react-native-splash-screen';
 import { NavigationActions } from 'react-navigation';
 import {AsyncStorage,Alert,Platform,TouchableOpacity,Image,NetInfo,View,Dimensions,DeviceEventEmitter,Text}from 'react-native';
 import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType, notificationUnsubscribe} from 'react-native-fcm';

 click = () => {
     alert(SYMBOL)
 }

 class Index extends Component<{}> {


   constructor(props) {
     super(props)
     console.disableYellowBox = true;
     this.state = {
         status:false,
         title:'offline',
         isVisible:false,
     }
     that=this;
   }


   componentWillUnmount() {
    NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
   }


   handleConnectionChange = (isConnected) => {
        var titile='';
        this.setState({ status: isConnected });
        if(this.state.status){
          titile="Internet Connected";
        }else{
          titile="Please check your internet connection ";
        }
        this.setState({title:titile,isVisible:true})
        if(this.state.status){
        setTimeout(function () {
          that.setState({isVisible:false})
        }, 2000);
      }
       //Alert.alert('network connection is '+this.state.status)
      DeviceEventEmitter.emit('isConnected',{isConnected:this.state.status} );
      }

   componentDidMount() {
        SplashScreen.hide();
        NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);

        NetInfo.isConnected.fetch().done(
          (isConnected) => { this.setState({ status: isConnected,isConnectionStatus:'Online', }); }
        );
     }



    componentDidUpdate () {
       SplashScreen.hide();
     }

     render() {


         const { props } = this.props;
         return (
             <View style={{flex:1}}>
             <MainStack />
               {
                 (this.state.isVisible && this.state.status == false)?
                 <View style={{alignSelf : "center", borderRadius : 7,height:35,alignItems:"center",justifyContent : "center", backgroundColor:'white',position:'absolute',marginTop : (Platform.OS=="ios")?15:2,width:Dimensions.get("window").width - 10,backgroundColor:(this.state.status)?"rgba(51, 204, 204,0.7)":"rgba(255,60,60,0.8)"}}>
                 <Text style={{fontSize:16,color:'white',alignSelf:'center',fontWeight:'bold'}}>{this.state.title}</Text>
                 </View>:null
               }
             </View>
         );
     }
 }


 const MainStack = StackNavigator({

     Login: {
         screen: Login,
         navigationOptions: {
             header: null,
             gesturesEnabled: false,
         }
     },
     Home: {
         screen: Home,
         navigationOptions: {
             header: null,
             gesturesEnabled: false,
         }
     },
     AddNewProduct: {
         screen: AddNewProduct,
         navigationOptions:({navigation, screenProps}) => ({

                      headerStyle : {

                               backgroundColor: '#89f4ef'
                      },
                      headerTintColor : "white",

                      headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.goBack()}}>
                          <Image source = {require('../themes/Images/back-arrow.png')}/>
                        </TouchableOpacity>,


         })


     },

     MyAuctions: {
         screen: MyAuctions,
         navigationOptions:({navigation, screenProps}) => ({

                      headerStyle : {

                               backgroundColor: '#89f4ef'
                      },
                      headerTintColor : "white",

                      headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.goBack()}}>
                          <Image source = {require('../themes/Images/back-arrow.png')}/>
                        </TouchableOpacity>,


         })
     },
     Details: {
         screen: Details,
         navigationOptions:({navigation, screenProps}) => ({

                      headerStyle : {

                               backgroundColor: '#89f4ef'
                      },
                      headerTintColor : "white",

                      headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.goBack()}}>
                          <Image source = {require('../themes/Images/back-arrow.png')}/>
                        </TouchableOpacity>,


         })
     },
     AuctionsInvites: {
         screen: AuctionsInvites,
         navigationOptions:({navigation, screenProps}) => ({

                      headerStyle : {

                               backgroundColor: '#89f4ef'
                      },
                      headerTintColor : "white",

                      headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.dispatch({
                      type: NavigationActions.NAVIGATE,
                      routeName: 'Home',
                      action: {
                        type: NavigationActions.RESET,
                        index: 1,
                        actions: [{type: NavigationActions.NAVIGATE, routeName: 'Home'}]
                      }})}}>
                          <Image source = {require('../themes/Images/back-arrow.png')}/>
                        </TouchableOpacity>,


         })
     },
     AuctionsWon: {
         screen: AuctionsWon,
         navigationOptions:({navigation, screenProps}) => ({

                      headerStyle : {

                               backgroundColor: '#89f4ef'
                      },
                      headerTintColor : "white",

                      headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.dispatch({
                              type: NavigationActions.NAVIGATE,
                              routeName: 'Home',
                              action: {
                                type: NavigationActions.RESET,
                                index: 1,
                                actions: [{type: NavigationActions.NAVIGATE, routeName: 'Home'}]
                              }})}}>
                          <Image source = {require('../themes/Images/back-arrow.png')}/>
                        </TouchableOpacity>,


         })
     },
     WonDetails: {
         screen: WonDetails,
         navigationOptions:({navigation, screenProps}) => ({

                      headerStyle : {

                               backgroundColor: '#89f4ef'
                      },
                      headerTintColor : "white",

                      headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.goBack()}}>
                          <Image source = {require('../themes/Images/back-arrow.png')}/>
                        </TouchableOpacity>,


         })
     },
     webview: {
         screen: webview,
         navigationOptions:({navigation, screenProps}) => ({

                       headerTitle:"",
                      headerStyle : {

                               backgroundColor: '#89f4ef'
                      },
                      headerTintColor : "white",

                      headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.goBack()}}>
                          <Image source = {require('../themes/Images/back-arrow.png')}/>
                        </TouchableOpacity>,


         })
     },
     OpenPDF: {
         screen: OpenPDF,
         navigationOptions:({navigation, screenProps}) => ({

                       headerTitle:"",
                      headerStyle : {

                               backgroundColor: '#89f4ef'
                      },
                      headerTintColor : "white",

                      headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.goBack()}}>
                          <Image source = {require('../themes/Images/back-arrow.png')}/>
                        </TouchableOpacity>,


         })
     },
     ShareAuction: {
         screen: ShareAuction,
         navigationOptions:({navigation, screenProps}) => ({

                      headerStyle : {

                               backgroundColor: '#89f4ef'
                      },
                      headerTintColor : "white",

                      headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.goBack()}}>
                          <Image source = {require('../themes/Images/back-arrow.png')}/>
                        </TouchableOpacity>,


         })
     },
     AuctionInvitesDetails: {
         screen: AuctionsInvitesDetails,
         navigationOptions:({navigation, screenProps}) => ({

                      headerStyle : {

                               backgroundColor: '#89f4ef'
                      },
                      headerTintColor : "white",

                      headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.goBack()}}>
                          <Image source = {require('../themes/Images/back-arrow.png')}/>
                        </TouchableOpacity>,


         })
     },
     ExchangeProductDetails :{
       screen : ExchangeProductDetails,
       navigationOptions:({navigation, screenProps}) => ({

                    headerStyle : {

                             backgroundColor: '#89f4ef'
                    },
                    headerTintColor : "white",

                    headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.goBack()}}>
                        <Image source = {require('../themes/Images/back-arrow.png')}/>
                      </TouchableOpacity>,


       })

     },
     ForgotPassword: {
         screen: ForgotPassword,
         navigationOptions:({navigation, screenProps}) => ({

                      headerStyle : {

                               backgroundColor: '#89f4ef'
                      },
                      headerTintColor : "white",

                      headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.goBack()}}>
                          <Image source = {require('../themes/Images/back-arrow.png')}/>
                        </TouchableOpacity>,


         })
     },
     Model: {
         screen: Model,
         navigationOptions: {
             header: null
         }
     },

     Registration: {
         screen: Registration,
         navigationOptions:({navigation, screenProps}) => ({

                      headerStyle : {

                               backgroundColor: '#89f4ef'
                      },
                      headerTintColor : "white",

                      headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.goBack()}}>
                          <Image source = {require('../themes/Images/back-arrow.png')}/>
                        </TouchableOpacity>,


         })
     },
     ViewProfile: {
         screen: ViewProfile,
         navigationOptions:({navigation, screenProps}) => ({

                      headerStyle : {

                               backgroundColor: '#89f4ef'
                      },
                      headerTintColor : "white",

                      headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.goBack()}}>
                          <Image source = {require('../themes/Images/back-arrow.png')}/>
                        </TouchableOpacity>,


         })
     },
     MyExchange: {
         screen: MyExchange,
         navigationOptions:({navigation, screenProps}) => ({

                     headerTitle:'',
                      headerStyle : {

                               backgroundColor: '#89f4ef'
                      },
                      headerTintColor : "white",

                      headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.goBack()}}>
                          <Image source = {require('../themes/Images/back-arrow.png')}/>
                        </TouchableOpacity>,


         })
     },

     AddExchangeProduct: {
         screen: AddExchangeProduct,
         navigationOptions:({navigation, screenProps}) => ({

                      headerStyle : {

                               backgroundColor: '#89f4ef'
                      },
                      headerTintColor : "white",

                      headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.goBack()}}>
                          <Image source = {require('../themes/Images/back-arrow.png')}/>
                        </TouchableOpacity>,


         })
     },
     ExchangeRequests: {
         screen: ExchangeRequests,
         navigationOptions:({navigation, screenProps}) => ({

                      headerStyle : {

                               backgroundColor: '#89f4ef'
                      },
                      headerTintColor : "white",

                      headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.goBack()}}>
                          <Image source = {require('../themes/Images/back-arrow.png')}/>
                        </TouchableOpacity>,


         })
     },
     RequestExchangeDetails: {
         screen: RequestExchangeDetails,
         navigationOptions:({navigation, screenProps}) => ({

                      headerStyle : {

                               backgroundColor: '#89f4ef'
                      },
                      headerTintColor : "white",

                      headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.goBack()}}>
                          <Image source = {require('../themes/Images/back-arrow.png')}/>
                        </TouchableOpacity>,


         })
     },
     BidderList: {
         screen: BidderList,
         navigationOptions:({navigation, screenProps}) => ({

                      headerStyle : {

                               backgroundColor: '#89f4ef'
                      },
                      headerTintColor : "white",

                      headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.goBack()}}>
                          <Image source = {require('../themes/Images/back-arrow.png')}/>
                        </TouchableOpacity>,


         })
     },
   },

 );

 export default Index
