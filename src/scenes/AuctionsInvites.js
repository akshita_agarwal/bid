/**
 * Created by Rishi Paliwal
 * 9 Nov 2017
 * FileManager Class
 */

 import React, { Component } from 'react';
 import { DrawerNavigator, StackNavigator, } from 'react-navigation';
 import { AppStyles, AppSizes, AppColors } from '../themes/'
 import Buttons, { LoginButton, Button2, Header, ShareBtn, BidderListBtn ,NoRecordFound,Countdwn} from "../components/Buttons";
 import Spinner from 'react-native-loading-spinner-overlay';
 import {
     Platform,
     StyleSheet,
     Text,
     View,
     Button,
     ListView,
     Image,
     ScrollView,
     Modal,
     AlertIOS,
     AsyncStorage,
     TextInput,
     TouchableOpacity,
     BackHandler,
     Alert,
     RefreshControl,
     DeviceEventEmitter,
     SafeAreaView,
     StatusBar,
     TouchableWithoutFeedback,
 } from 'react-native';
 import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
 import SearchBar from 'react-native-material-design-searchbar';
 const deleteimg = require('../themes/Images/close_red.png')
 import Model from './Model';
 var folderIcon = require('../themes/Images/folder.png')
 const bckimg = require('../themes/Images/bg-bottom.png')
 var Constant = require('../api/WebInteractor').Constant;
 var WebServices = require('../api/WebInteractor').WebServices;
 import * as commonFunctions from '../utils/CommonFunctions'
 import { NavigationActions } from 'react-navigation';
 import TimerCountdown from 'react-native-timer-countdown'
 var _that = '';
 var data = [];
 var ds;
var search='';
var day = 'day';
var days = 'days';
var auctionDelmsg = "";

 class AuctionsInvites extends React.Component {
     constructor(props) {
           super(props);
           _that = this
           ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
           this.state = {
               dataSource: ds.cloneWithRows(data),
               modalVisible:false,
               bidAmount:'',
               UserID:'',
               product_id:'',
               minBidAmount:'',
               maxBidAmount:'',
               isTimeLeft:false,
               refreshing:false,
               _visible:false,
               searchText:'',
               isConnected : true,
               isDataAvailable:true,//intially its show LISTVIEW with zero Items
           };
     }



       componentWillMount() {
       //  commonFunctions.checkConnection();
           AsyncStorage.getItem('UserData')
               .then((res) => {
                   if (res) {
                       var data = JSON.parse(res)
                       if (data.isRemember) {
                           this.setState({
                               UserID: data.UserID
                           })
                           this.FetchAuctionInvitesList()
                       }
                   }
               });
                 BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
                 DeviceEventEmitter.addListener("isConnected",this.checkConnection.bind(this));
                // DeviceEventEmitter.addListener("Refresh",this.FetchInvitesAfterDeletion.bind(this));

                 DeviceEventEmitter.addListener('Refresh', function (data) {
                   // alert("msg:Fgff" + data);
                   auctionDelmsg = data.msg;
                 this.FetchInvitesAfterDeletion(data)
             }.bind(this));
       }

       componentWillUnmount() {

          search=''
           BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
           DeviceEventEmitter.removeListener("isConnected",this.checkConnection.bind(this));
           DeviceEventEmitter.removeListener("Refresh",this.checkConnection.bind(this));

       }

      //GO BACK TO previous screen on device back button click
     handleBackButtonClick() {
         _that.props.navigation.dispatch({
         type: NavigationActions.NAVIGATE,
         routeName: 'Home',
         action: {
           type: NavigationActions.RESET,
           index: 1,
           actions: [{type: NavigationActions.NAVIGATE, routeName: 'Home'}]
         }})
         return true;
     }

     //Checking the Internet Connection

     checkConnection(data){

           this.setState({isConnected : data.isConnected});
     }

     _searchProducts=(searchText)=>{
       this.setState({searchText:searchText})


     }
     //Clicking on search icon
     _searchIconClicked=()=>{
       if(this.state.isConnected){
           search = this.state.searchText
           if(this.state.searchText==''){
             this.setState({ _visible: false })
             commonFunctions.message('Please enter some text')
           }
           else{
             this.FetchAuctionInvitesList()
            }
        }
     }

     /*
     Refreshing the product list
     */
     _onRefresh() {
       if(this.state.isConnected){
         this.setState({refreshing: true});
         this.FetchAuctionInvitesList()
       }
     }


     render() {
       let msg = "No Record Found";
             if(this.props.navigation.state.params.fromWhere == "Notification" && this.props.navigation.state.params.productDeleted == "No")
             {
                 msg = "This Auction is not currently available";
             }
       const {bidAmount} = this.state
         return (
             <KeyboardAwareView animated={true}>
           <SafeAreaView style={{ flex: 1}}>

           <View style={{flex: 1, backgroundColor: AppColors.white, }}>
             <Image source={bckimg} style={{ width: AppSizes.screen.width, height: AppSizes.screen.height, position: 'absolute', top: 0, left: 0 }} />

            <View style = {{flexDirection:'row'}}>
                 <Header text='Auction Invites' navigation = {this.props.navigation}/>

                   <SearchBar
                       onSearchChange={(searchText) => this._searchProducts(searchText)}
                       height={50}
                       onFocus={() => console.log('On Focus')}
                       onBlur={() => console.log('On Blur')}
                       placeholder={'Search'}
                       autoCorrect={false}
                       padding={5}
                       inputStyle = {{marginTop:160,width:AppSizes.screen.width-60}}
                       returnKeyType={'done'}
                     />

                     <TouchableOpacity style = {{marginTop:165,height:49,width:40,justifyContent:'center',backgroundColor:'#ddd',marginRight:10}} onPress = {this._searchIconClicked}>
                        <Image source = {require('../themes/Images/search_icon.png')} style = {{width:25,height:25,alignSelf:'center',resizeMode:'contain'}}/>
                     </TouchableOpacity>

                   </View>

                   <ScrollView style={{flex: 1 }}
                               refreshControl={
                                                <RefreshControl
                                                  refreshing={this.state.refreshing}
                                                    onRefresh={this._onRefresh.bind(this)}
                                                />
                                          }>

                  {
                   (this.state.isDataAvailable==true)?
                 <ListView
                     contentContainerStyle={AppStyles.filemanager_list}
                     dataSource={this.state.dataSource}
                     renderRow={(data, rowID) => this.renderRow(data, rowID)}
                     enableEmptySections={true}
                 />:<NoRecordFound txtMsg = {msg} marginTopSpace = {AppSizes.screen.height/5}></NoRecordFound>
             }
                 </ScrollView>
                 <Spinner visible={this.state._visible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />



             </View>
             </SafeAreaView>
             </KeyboardAwareView>
         );
     }
     /**
      * Display Products list  from API's
      */
     renderRow(data, rowID) {



       const profile_pic = (data.ProductImage) ? { uri: data.ProductImage } : '';
       const time = (data.TimeLeft * 1000)


         return (
           <View>
             <TouchableWithoutFeedback onPress = {()=>{if(_that.state.isConnected){this.goToDetail(data)}}}>
             <View style={[AppStyles.filemanager_list_item,{height:(Platform.OS == "ios"? 255:250)},{marginTop:30},{backgroundColor:(data.UserStatus == "In Progress"? 'white' :"#e0e0e0")}]}>
              <View  onPress = {()=>{}}>
                 <View style={AppStyles.filemanager_folder_view}>
                     <Image
                         source={profile_pic}
                         style={AppStyles.filemanager_folder_icon}
                     />
                 </View>
                 <View style={[AppStyles.filemanager_folder_view,{paddingTop:10}]}>
                     <Text style={AppStyles.filemanager_folder_title} numberOfLines={1}>Title:  {data.Title} {"\n\n"}
                     </Text>
                     <Text style={AppStyles.filemanager_folder_title} numberOfLines={1}>Duration : {data.Duration} {(data.Duration!=1)?days:day}
                     </Text>

                     {
                       (data.AuctionStatusId != 3)?<Text style={AppStyles.filemanager_folder_title} numberOfLines={2}>Time Left : <Countdwn time={time} /></Text>:null
                     }


                 </View>

                 <View style={[AppStyles.filemanager_folder_view,{paddingTop:3}]}>
                   <ShareBtn text="Details" onPress={()=>{this.goToDetail(data)}} style={[AppStyles.Auctionlist_button,{fontSize : (Platform.OS == "ios")? 11 : 12}, {marginTop : (data.AuctionStatusId == 3)?30:11,justifyContent:'center', fontWeight:'bold',backgroundColor: '#f77f83' },{width:100,height:38,paddingTop:10},{alignSelf: 'center'},{marginBottom:20}]} />
                 </View>


           </View>
          </View>
             </TouchableWithoutFeedback>
              <TouchableOpacity onPress = {()=>{if(this.state.isConnected){this.deleteProduct(data.Title,data.InvitedUserID,data.ProductID)}}} style={{ width: 30, height: 40, top: 15 ,position : 'absolute' ,alignSelf: "flex-end" }} >
                  <Image source={deleteimg} style ={{position : 'absolute' }} />
               </TouchableOpacity>

          </View>

         )
     }


     //On Pressing delete icon

     deleteProduct = (ProductName,InvitedUserID,ProductID)=> {

       var data = {
         "ProductID": ProductID,
         "InvitedUserID": InvitedUserID
       }

       if(Platform.OS=="ios")
       {
             setTimeout(()=>(AlertIOS.alert(
             ProductName,
             'Are you sure you want to delete this product',
             [
               {
                 text: 'YES',onPress: () =>{this.PostToApiCalling('POST', 'DeleteAuctionInvite', Constant.URL_DeleteAuctionInvite, data)}

               },
               {
                 text: 'NO'
               },

             ]
           )), 100)
           _that.setState({ _visible: false })
           }
           else {
             Alert.alert(
                     ProductName,
                     'Are you sure you want to delete this product',
                     [
                       {
                         text: 'YES',onPress: () =>{this.PostToApiCalling('POST', 'DeleteAuctionInvite', Constant.URL_DeleteAuctionInvite, data)}
                       },
                       {
                         text: 'NO'
                       },

                     ],
                     { cancelable: false }
                   )

           }



     }

     //Fetching AuctionInviteList

     FetchAuctionInvitesList=()=>{
         this.setState({ _visible: true })
         var data = {
             "UserID": this.state.UserID,
             "PageNumber": 1,
             "PageSize": 1000,
             "Search":search
         }
         console.log('data is '+JSON.stringify(data));
         this.PostToApiCalling('POST', 'AuctionInviteList', Constant.URL_AuctionInviteList, data);
         this.setState({refreshing:false})
     }


     /**
      update the AuctionsInvitesList after coming from AuctionInvitesDetails
     */

     updateAuctionInvites=()=>{
       this.FetchAuctionInvitesList()
     }

     /*

          Fetching the AuctionInviteList after acutioneer Deleting the product
      */
     FetchInvitesAfterDeletion(data){

        var {fromWhere , isProductDeleted} = this.props.navigation.state.params;
        if(data.opened_from_tray){
          Alert.alert(auctionDelmsg);
        }

       _that.FetchAuctionInvitesList();
 }

     /**
      Go to AuctionInvitesDetails
     */

     goToDetail=(data)=>{
       if(this.state.isConnected){
       _that.props.navigation.navigate('AuctionInvitesDetails',{ProductID:data.ProductID,updateAuctionInvites:this.updateAuctionInvites.bind(this)})
        }
      }



     /**
     * API Calling
     */


     PostToApiCalling(method, apiKey, apiUrl, data) {

         new Promise(function (resolve, reject) {

             if (method == 'POST') {
                 resolve(WebServices.callWebService(apiUrl, data));
             } else {
                 resolve(WebServices.callWebService_GET(apiUrl, data));
             }
         }).then((jsonRes) => {

             if ((!jsonRes) || (jsonRes.ResponseCode == 0)) {

                 this.setState({ _visible: false })
                 this.apiSuccessfullResponse(apiKey, jsonRes)


             } else {
                 commonFunctions.message(jsonRes.ResponseMessage)
                 this.setState({ _visible: false })
             }
         })
         .catch((error) => {
             console.log("ERROR" + error);
         })
     }

     apiSuccessfullResponse(apiKey, jsonRes) {

       if(apiKey == 'AuctionInviteList')
       {
         var jsonResponse = jsonRes.ResponsePacket;

             if (jsonResponse.length == 0)
             {
                       _that.setState({dataSource:ds.cloneWithRows([])})
                       search=''
                      _that.setState({ _visible: false })
                      _that.setState({ isDataAvailable: false })


             } else {
               let found=false;
                  /* Check whether the product is in the list When user going in InvitedAuction Screen through Notification*/
                   if(this.props.navigation.state.params.fromWhere == "Notification")
                   {

                            jsonRes.ResponsePacket.forEach(function(item){
                                   if(item.ProductID == _that.props.navigation.state.params.ProductID){
                                       found=true;
                                       return;
                                   }
                            });
                   }
                    /*showing the msg when user click on notification and the auction is deleted or expired*/
                    if((found == false) && (this.props.navigation.state.params.fromWhere == "Notification" && this.props.navigation.state.params.productDeleted == "No"))
                    {
                        //Alert.alert("This Auction is not currently available ");

                    }
                   // if(this.props.navigation.state.params.productDeleted == "Yes")
                   // {
                   //   Alert.alert("Auction deleted by auctioneer");
                   // }


                       this.setState({ _visible: false, dataSource: ds.cloneWithRows(jsonResponse), })
              }

     }
     else if (apiKey == 'DeleteAuctionInvite'){

       //fetching the auction invite LIST AFTER Deleting the product
       _that.FetchAuctionInvitesList()


     }

       }


 }

 export default AuctionsInvites
