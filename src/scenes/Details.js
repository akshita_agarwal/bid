import React, { Component } from 'react';

import { AppStyles, AppSizes, AppColors } from '../themes/'
import Buttons, { LoginButton, Button2, Header, ShareBtn, BidderListBtn, Countdwn } from "../components/Buttons";
import Spinner from 'react-native-loading-spinner-overlay';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Button,
    ListView,
    Image,
    ScrollView,
    AsyncStorage,
    BackHandler,
    Alert,
    AlertIOS,
    TouchableOpacity,
    RefreshControl,
    SafeAreaView,
    DeviceEventEmitter,
    Modal
} from 'react-native';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';

var folderIcon = require('../themes/Images/folder.png')
const bckimg = require('../themes/Images/bg-bottom.png')
import * as commonFunctions from '../utils/CommonFunctions'
var Constant = require('../api/WebInteractor').Constant;
var WebServices = require('../api/WebInteractor').WebServices;
import TimerCountdown from 'react-native-timer-countdown';
import PhotoView from 'react-native-photo-view';
const deleteimg = require('../themes/Images/close_red.png')
const _that = '';
var product_id ;
var data=[];
var day = 'day';
var days = 'days';
var key;
class Details extends React.Component {
    constructor(props) {
        super(props);
        _that = this

        this.state = {
            _visible : false,
            isBidderExist:false,
            boxHeight:315,
            isBack:false,
            WinProductId:'',
            refreshing: false,
            data:[],
            modalVisible:false,
            CurrencyName:'',
            isConnected : true,
        };
    }

    componentWillMount() {

          product_id = this.props.navigation.state.params.ProductID;
          key = this.props.navigation.state.key
        AsyncStorage.getItem('UserData')
            .then((res) => {
                if (res) {
                    var data = JSON.parse(res)
                    if (data.isRemember) {
                        this.setState({
                            UserID: data.UserID,

                        })
                        this.FetchMyAuctionDetail()
                    }
                }
            });


               DeviceEventEmitter.addListener('RefreshManageRequest', function (data) {
                   this.props.navigation.state.params.updateMyAuction()

               }.bind(this));

            DeviceEventEmitter.addListener('LiveUpdateRequest', this.LiveUpdateMyAuction.bind(this))
            BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
            DeviceEventEmitter.addListener("isConnected",this.checkConnection.bind(this));
    }
    componentWillUnmount() {

        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
        DeviceEventEmitter.removeListener("isConnected",this.checkConnection.bind(this));
    }

  //GO BACK TO previous screen on device back button click
  handleBackButtonClick() {
      _that.props.navigation.goBack();
      return true;
   }
   /*
   Refreshing the product list
   */

   _onRefresh() {
       this.setState({refreshing: true});
       this.FetchMyAuctionDetail();
       _that.props.navigation.state.params.updateMyAuction()

   }

   //Checking the Internet Connection

   checkConnection(data){

          this.setState({isConnected : data.isConnected});
    }


    /*
     Rendering the myAuctiondetail
     */

    render() {
      const {data,CurrencyName} = this.state
      const profile_pic = (data.ProductImage) ? { uri: data.ProductImage } : '';
      const time = (data.TimeLeft * 1000)
        return (
            <KeyboardAwareView animated={true}>
           <SafeAreaView style={{ flex: 1}}>
           <View style={{flex: 1, backgroundColor: AppColors.white, }}>
            <Image source={bckimg} style={{ width: AppSizes.screen.width, height: AppSizes.screen.height, position: 'absolute', top: 0, left: 0 }} />

              <ScrollView style={{flex: 1 }}
                          refreshControl={
                                           <RefreshControl
                                             refreshing={this.state.refreshing}
                                               onRefresh={this._onRefresh.bind(this)}
                                           />
                                     }>
                <Header text='Product Details' navigation = {this.props.navigation}/>



                    <View style={[{marginTop: 170}]}>
                        <TouchableOpacity style={AppStyles.detail_filemanager_folder_view} onPress = {()=>this.zoomImage()}>
                            <Image
                                source={profile_pic}
                                style={[AppStyles.filemanager_folder_icon,{marginLeft :5,resizeMode:'contain'},{width :AppSizes.screen.width - 30},{height:150}]}
                            />
                        </TouchableOpacity>

                        <View style={[AppStyles.detail_filemanager_folder_view,{paddingTop:10},{width : AppSizes.screen.width-80},{marginBottom : 20}]}>
                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Title : {data.Title} {"\n"}
                            </Text>
                            <Text style={AppStyles.detail_filemanager_folder_title}>Total Bid : {data.TotalBid} {"\n"}
                            </Text>
                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={4}>{data.Description} {"\n"}
                            </Text>
                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={4}>Status : {(data.IsProductShared)?"Shared":"Not Shared"} {"\n"}
                            </Text>
                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={4}>Auction Status : {(data.AuctionStatus!=3)?"Not won":data.WonType} {"\n"}
                            </Text>
                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Min Bid Amount : {data.CurrencyName}{data.MinBidAmount} {"\n"}
                            </Text>
                            {
                              (data.MAxBidAmount==9999999999999999.00)?null
                              :<Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Max Bid Amount: {
                              }{data.MAxBidAmount} {"\n"}
                              </Text>
                            }

                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Bidders : {data.Bidders} {"\n"}
                            </Text>
                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Highest Bid : {data.CurrencyName}{data.MaxBid} {"\n"}
                            </Text>
                            <Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Duration : {data.DurationOfAuctionInDays} {(data.DurationOfAuctionInDays!=1)?days:day} {"\n"}
                            </Text>
                            {
                              (data.AuctionStatus == 3)?<Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Winner : {data.ProductWinnerUsername} {"\n"}</Text>:null
                            }
                              {
                                (data.AuctionStatus == 3)?<Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Winner Mobile number : {data.ProductWinnerPhoneNo} {"\n"}</Text>:null
                              }
                              {
                                  (data.AuctionStatus != 3)?<Text style={AppStyles.detail_filemanager_folder_title} numberOfLines={2}>Time Left : <Countdwn time={time} /></Text>:null
                              }
                        </View>
                        <View style={{flexDirection:'row',justifyContent:"space-around"}}>
                        {
                            (parseInt(data.Bidders) != 0 && this.state.WinProductId != data.ID && data.AuctionStatus != 3)?<ShareBtn text="View Bidders" onPress={() => {if(_that.state.isConnected){this.ViewBidder(data.ID)}}} style={[AppStyles.Auctionlist_button,{fontSize : (Platform.OS == "ios")? 11 : 12}, {justifyContent:'center',fontWeight:'bold', backgroundColor: '#f77f83' },{width:100,height:38,paddingTop:10},{alignSelf: 'center'},{marginBottom:20}]} />:null
                       }
                       {
                           (data.IsExchangeProductAvailable == true)?<ShareBtn text="View Exchanger" onPress={() => {if(_that.state.isConnected){this.ViewExchanger(data.ID)}}} style={[AppStyles.Auctionlist_button,{fontSize : (Platform.OS == "ios")? 11 : 12}, {justifyContent:'center',fontWeight:'bold', backgroundColor: '#f77f83' },{width:100,height:38,paddingTop:10},{alignSelf: 'center'},{marginBottom:20}]} />:null

                       }
                       {
                              (data.TimeLeft != 0 && data.AuctionStatus != 3)?<ShareBtn text="Share Auction" onPress={() => { this.ShareAuction(data.ID) }} style={[AppStyles.Auctionlist_button,{fontSize : (Platform.OS == "ios")? 11 : 12}, {justifyContent:'center', fontWeight:'bold',backgroundColor: '#f77f83' },{width:100,height:38,paddingTop:10},{alignSelf: 'center'},{marginBottom:20}]} />: null
                       }
                       {
                         (data.IsProductShared)?null:<ShareBtn text="Update Details" onPress={() => { this.UpdateDetails(data) }} style={[AppStyles.Auctionlist_button,{fontSize : (Platform.OS == "ios")? 11 : 12}, {justifyContent:'center', backgroundColor: '#f77f83' },{width:100,height:38,paddingTop:10,fontWeight:'bold',},{alignSelf: 'center'},{marginBottom:20}]} />
                       }
                       </View>
                    </View>

                    <Modal visible={this.state.modalVisible} onPress = {()=>{this.setState({modalVisible:!this.state.modalVisible})}} animationType={'slide'} onRequestClose={() => this.closeModal()}>

                        <View style={{flex:1}}>

                                <View style = {{}}>
                                <TouchableOpacity onPress = {()=>this.closeModal()} style={{ width: 40, height: 40, top: 15 ,position : 'absolute' ,alignSelf: "flex-end" }} >
                                    <Image source={deleteimg} style ={{position : 'absolute' }} />
                                 </TouchableOpacity>
                                </View>

                                <PhotoView
                                      source={profile_pic}
                                      minimumZoomScale={1}
                                      maximumZoomScale={5}
                                      androidScaleType="fitCenter"
                                      onLoad={() => console.log("Image loaded!")}
                                      style={{width:AppSizes.screen.width-20,height:AppSizes.screen.height-60,marginTop:50,resizeMode:'cover',alignSelf:'center'}} />


                        </View>
                    </Modal>

                <Spinner visible={this.state._visible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />
            </ScrollView>
            </View>
            </SafeAreaView>
            </KeyboardAwareView>
        );
    }

    // clicks to zoom Image

    zoomImage=()=>{
      this.openModal()
    }

    /*
      opens pop up
    */
    openModal=()=> {
        this.setState({ modalVisible: true });
    }

    /*
    closes the pop up
    */

    closeModal=()=> {
        this.setState({ modalVisible: false });

    }

    //update product details

    UpdateDetails = (data) =>{
      this.props.navigation.navigate('AddNewProduct',{updateData:data,Routekey:key,refreshingDetail:this.backFromUpdateProduct.bind(this)})

    }


    // go to ShareAuction screen with some arguments

    ShareAuction = (product_id) => {
        _that.props.navigation.navigate('ShareAuction', { UserId: this.state.UserID, ProductID: product_id,Routekey:key,refreshingDetail:this.backFromShareAuction.bind(this)});
    }

    // go to View Bidders screen

    ViewBidder = (product_id) => {
        _that.props.navigation.navigate('BidderList', { key : key,UserId: this.state.UserID, ProductID: product_id, refresh:(product_id)=>{this.refresh(product_id)} })
    }

    ViewExchanger = (product_id) =>{

        _that.props.navigation.navigate('ExchangeRequests', { getAuctionDetail : this.getMyAuctionDetail.bind(this),key : key,fromWhere :"Details",UserId: this.state.UserID, ID: product_id, refresh:(product_id)=>{this.refresh(product_id)} })
    }

    /*
      hides the ViewBidder button after winning the product auction
    */
    refresh=(product_id)=>{

      this.setState({WinProductId:product_id})
      this.FetchMyAuctionDetail()
    }

    /*
      Refresh the details of the product after returning from AddNewProduct
      */

    backFromUpdateProduct=()=>{
      this.props.navigation.state.params.updateMyAuction()
    }

    /*
      Refresh the details of the product after returning from ShareAuction
      */

      backFromShareAuction=()=>{
        this.props.navigation.state.params.updateMyAuction()
      }

      /**
       * [LiveUpdateMyAuction calls GetProductList api for Live Updating details]
       */

      LiveUpdateMyAuction=()=>{

        var data = {

            "ProductID":product_id

        }

          this.PostToApiCalling('POST', 'GetProductDetailByProductID', Constant.URL_GetProductDetailByProductID, data);
      }

      /*
        Fetches the MyAuctions product detail
      */

      FetchMyAuctionDetail=()=>{
        this.setState({ _visible: true })
        var data = {

            "ProductID":product_id

        }
        this.PostToApiCalling('POST', 'GetProductDetailByProductID', Constant.URL_GetProductDetailByProductID, data);
        this.setState({refreshing:false})
      }


       getMyAuctionDetail=()=>{
        this.setState({ _visible: true })
        var data = {

            "ProductID":product_id

        }
        this.PostToApiCalling('POST', 'GetProductDetailByProductID', Constant.URL_GetProductDetailByProductID, data);
        this.setState({refreshing:false})
      }
      /**
      * API Calling
      */


      PostToApiCalling(method, apiKey, apiUrl, data) {

          new Promise(function (resolve, reject) {

              if (method == 'POST') {
                  resolve(WebServices.callWebService(apiUrl, data));
              } else {
                  resolve(WebServices.callWebService_GET(apiUrl, data));
              }
          }).then((jsonRes) => {

              if ((!jsonRes) || (jsonRes.ResponseCode == 0)) {

               this.setState({ _visible: false })
                  this.apiSuccessfullResponse(apiKey, jsonRes)


              } else {
                  commonFunctions.message(jsonRes.ResponseMessage)
                  this.setState({ _visible: false })
              }
          })
          .catch((error) => {
              console.log("ERROR" + error);
          })
      }

      apiSuccessfullResponse(apiKey, jsonRes) {

        if(apiKey == 'GetProductDetailByProductID')
        {
           //this.setState({ _visible: false })
              var jsonResponse = jsonRes.ResponsePacket;

              if (jsonResponse.length == 0) {




                       setTimeout(()=>(Alert.alert(
                                '',
                                'No record found',
                                [
                                  {text: 'OK', onPress: () => {this.props.navigation.goBack()}},

                                ],
                                { cancelable: false }
                              )),1000);



              } else {


                  this.setState({ _visible: false, data: jsonResponse })
              }

      }

    }




}

export default Details
