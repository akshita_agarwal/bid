import React, { Component } from 'react';
import { DrawerNavigator, StackNavigator } from 'react-navigation';
import { NavigationActions } from 'react-navigation';
import DatePicker from 'react-native-datepicker'
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Button,
    TouchableWithoutFeedback,
    Keyboard,
    Alert,
    Image,
    TextInput,
    ScrollView,
    AsyncStorage,
    TouchableHighlight,
    BackHandler,
    TouchableOpacity,
    AlertIOS,
    SafeAreaView,
    KeyboardAvoidingView,
    DeviceEventEmitter,

} from 'react-native';
import CountryPicker, { getAllCountries } from 'react-native-country-picker-modal';

import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import { AppStyles, AppSizes, AppColors } from '../themes/'
import Buttons, { LoginButton, Button2, Header } from "../components/Buttons";
const Logo = require('../themes/Images/logo.png');
import Spinner from 'react-native-loading-spinner-overlay';
import * as commonFunctions from '../utils/CommonFunctions';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
var Constant = require('../api/WebInteractor').Constant;
var WebServices = require('../api/WebInteractor').WebServices;
const countryList = ['US', 'GB'];
var _that;
var timeStamp;
var currentDate = "";

const Left = ({ onPress }) => (
    <TouchableHighlight onPress={onPress}>
        <Text>dfgdfgf</Text>
    </TouchableHighlight>
);
var radio_props = [
  {label: 'UK        ', value: '£' , color : '#ffffff'},
  {label: 'USA', value: '$' }
];
class ViewProfile extends React.Component<{}> {
    constructor(props) {
        super(props)
        var m_names = new Array("Jan", "Feb", "Mar",
        "Apr", "May", "Jun", "Jul", "Aug", "Sep",
        "Oct", "Nov", "Dec");
        var tdate = new Date();
           var dd = tdate.getDate(); //yields day
           var MM = tdate.getMonth(); //yields month
           var yyyy = tdate.getFullYear(); //yields year
           //var todayDate= yyyy + "-" +( MM+1) + "-" + dd;
           var todayDate= dd+ "-"+ m_names[MM]+"-"+yyyy
          //   currentDate = todayDate.toString();
           currentDate = todayDate;
           const userCountryData = getAllCountries()
            .filter((country) => countryList.includes(country.cca2))
            .filter((country) => country.cca2 === 'US').pop();
        let callingCode = null;
        let cca2 = 'US';
        if (!cca2 || !userCountryData) {
            cca2 = 'US';
            callingCode = '1';
        } else {
            callingCode = userCountryData.callingCode;
        }
        this.state = {
            full_name: '',
            mobile_number: '',
            Password: '',
            confirm_password: '',
            email_id: '',
            _visible: false,
            avatarSource: '',
            _coreimage: '',
            UserID: '',
            isEditing: false,
            isFbLogin: "",
            currency:'',
            preCountrySelectedIndex:'',
            country:'',
            _isRemember : true,
            cca2: 'US',
            callingCode: '1',
            isConnected : true,
            dob : currentDate,
            date:currentDate,
            timeStamp : "",
            tdyDate : currentDate,

        }
        _that = this
    }
    static navigationOptions = ({ navigation, screenProps }) => ({ headerStyle : {
              backgroundColor: '#89f4ef'
     }, headerRight: <TouchableOpacity  style = {{overflow: (Platform.OS == "ios")?'hidden': null,borderRadius : 10,backgroundColor : "#f67378",marginRight : 10, width : 80, height : 35,justifyContent : "center",alignItems : "center"}} onPress={() => {_that.logOutConfirm(navigation)}}>
      <Text style = {{color : "white", fontWeight : "bold"}}> LOGOUT </Text>
      </TouchableOpacity>,
     headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {
       navigation.goBack()}}>
        <Image source = {require('../themes/Images/back-arrow.png')}/>
      </TouchableOpacity>, });



    click(navigation){
      AsyncStorage.removeItem("UserData"),
      AsyncStorage.removeItem("fcmToken")
      _that.logoutApiCall();

    }

    logOutConfirm=(navigation)=>{
      Alert.alert(
              'Log out',
              'Are you sure you want to log out?',
              [
                {
                  text: 'OK',
                 onPress: () => {_that.click(navigation)},
               },
               {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},

              ],
              { cancelable: false }
            );

    }
    componentWillMount() {
        AsyncStorage.getItem('UserData')
            .then((res) => {
                if (res) {
                    var data = JSON.parse(res)
                    if (data.isRemember) {
                        this.setState({
                            UserID: data.UserID
                        })
                    }
                    this.btnApiCalled = this.btnApiCalled.bind(this)
                    this.btnApiCalled()
                }
            });
            AsyncStorage.getItem('isFaceBookLogin', (err, res) => {

                   this.setState({isFbLogin: res});


             });

          //  BackHandler.removeEventListener('hardwareBackPress');
             DeviceEventEmitter.addListener("isConnected",this.checkConnection.bind(this));


          }
          componentDidMount(){
            BackHandler.addEventListener('hardwareBackPress',this.handleBackButtonClick);

          }
          componentWillUnmount()
          {
            BackHandler.removeEventListener('hardwareBackPress',this.handleBackButtonClick)
            DeviceEventEmitter.removeListener("isConnected",this.checkConnection.bind(this));
          }

          //GO BACK TO previous screen on device back button click

          handleBackButtonClick()
          {
              _that.props.navigation.goBack(null);
              return (true)
          }

          //Checking the Internet Connection
          checkConnection(data){

              this.setState({isConnected : data.isConnected});
            }




    render() {
        const { full_name, mobile_number, Password, confirm_password, email_id, avatarSource, isEditing ,currency,country,preCountrySelectedIndex} = this.state
        return (

              <ScrollView  style = {{backgroundColor :"white"}} keyboardShouldPersistTaps={'always'} >
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                  <View style={[AppStyles.container, { paddingLeft: AppColors.paddingLeft30, paddingRight: AppColors.paddingRight30 }]}>
                      <Header text='MY PROFILE' navigation = {this.props.navigation} />
                      <View style={{ marginTop: 150 }}>
                          <LoginButton text='Edit' style={[AppStyles.green_round_button, { width: 100, height: 50, alignSelf: 'flex-end' }]} onPress={this.setEditingTextInput} />
                      </View>

                      <View style={AppStyles.Input_iconcnt}>
                          <View style={AppStyles.Inputtxt}>
                              <TextInput
                                  autoCorrect={false}
                                  underlineColorAndroid="transparent"
                                  editable={isEditing}
                                  returnKeyType={"next"}
                                  blurOnSubmit={false}
                                  autoFocus={false}
                                  placeholder="Full Name"
                                  placeholderTextColor={AppColors.lightGray_text}
                                  style={AppStyles.InputCnt_txt}
                                  ref="full_name"
                                  maxLength = {50}
                                  autoCapitalize = "none"
                                  onChangeText={full_name => this.setState({ full_name })}
                                  value={full_name}
                                  onSubmitEditing={(event) => { this.refs.password.focus(); }}
                              />
                          </View>
                      </View>


                      <View style={AppStyles.Input_iconcnt}>
                        <View style={{flexDirection:'row',marginLeft:10,}}>
                          <View style={{marginTop:5}}>
                          <CountryPicker
                              countryList={countryList}
                              disabled={true}
                              onChange={(value) => {
                                  this.setState({
                                      cca2: value.cca2,
                                      callingCode: value.callingCode
                                  });
                              }}
                              cca2={this.state.cca2}
                              translation='eng'
                              />
                            </View>
                            <View style={{ marginLeft: 3, width: 300 }}>
                              <TextInput
                                  autoCorrect={false}
                                  underlineColorAndroid="transparent"
                                  editable={isEditing}
                                  returnKeyType={"next"}
                                  blurOnSubmit={false}
                                  keyboardType={'numeric'}
                                  autoFocus={false}
                                  placeholder="Mobile Number"
                                  maxLength={15}
                                  placeholderTextColor={AppColors.lightGray_text}
                                  style={AppStyles.InputCnt_txt}
                                  ref="mobile_number"
                                  onChangeText={mobile_number => this.setState({ mobile_number })}
                                  value={mobile_number}
                              />
                              </View>
                            </View>
                      </View>

                      <View style={AppStyles.Input_iconcnt}>
                          <View style={AppStyles.Inputtxt}>
                              <TextInput
                                  autoCorrect={false}
                                  underlineColorAndroid="transparent"
                                  editable={isEditing}
                                  returnKeyType={"next"}
                                  blurOnSubmit={false}
                                  autoFocus={false}
                                  maxLength = {50}
                                  autoCapitalize = "none"
                                  placeholder="Password"
                                  secureTextEntry={true}
                                  placeholderTextColor={AppColors.lightGray_text}
                                  style={AppStyles.InputCnt_txt}
                                  ref="password"
                                  onChangeText={Password => this.setState({ Password })}
                                  value={Password}
                                  onSubmitEditing={() => { this.refs.confirm_password.focus(); }}
                              />
                          </View>
                      </View>
                      <View style={AppStyles.Input_iconcnt}>
                          <View style={AppStyles.Inputtxt}>
                              <TextInput
                                  autoCorrect={false}
                                  underlineColorAndroid="transparent"
                                  editable={isEditing}
                                  returnKeyType={"next"}
                                  blurOnSubmit={false}
                                  autoFocus={false}
                                  maxLength = {50}
                                  autoCapitalize = "none"
                                  secureTextEntry={true}
                                  placeholder="Confirm Password"
                                  placeholderTextColor={AppColors.lightGray_text}
                                  style={AppStyles.InputCnt_txt}
                                  ref="confirm_password"
                                  onChangeText={confirm_password => this.setState({ confirm_password })}
                                  value={confirm_password}
                                  onSubmitEditing={() => { this.refs.email_id.focus(); }}
                              />
                          </View>
                      </View>
                      <View style={[AppStyles.Input_iconcnt,{marginBottom:10}]}>
                          <View style={AppStyles.Inputtxt}>
                              <TextInput
                                  autoCorrect={false}
                                  underlineColorAndroid="transparent"
                                  editable={isEditing}
                                  returnKeyType={"done"}
                                  autoFocus={false}
                                  maxLength = {50}
                                  autoCapitalize = "none"
                                  placeholder="Email ID"
                                  keyboardType={'email-address'}
                                  placeholderTextColor={AppColors.lightGray_text}
                                  style={[AppStyles.InputCnt_txt, { height: 50 , paddingBottom : 20}]}
                                  ref="email_id"
                                  multiline={false}
                                  onChangeText={email_id => this.setState({ email_id })}
                                  value={email_id}
                              />
                          </View>
                      </View>
                      {
                    (!this.state.isEditing)?<View style = {{flexDirection :"row",marginTop : 20,marginBottom:15}}>
                          <Text style = {{paddingLeft : 5,fontSize : 17, fontWeight :"bold"}}>DOB : </Text>
                        <Text  style = {{fontSize : 17}}> {this.state.date}</Text>
                      </View>:null
                    }
                      {
                   (this.state.isEditing)?<View>
                      <View style = {{marginTop : 20, flexDirection : "row"}}>
                        <Text style = {{marginTop : 10, marginRight: 10, fontSize : 17}}> DOB: </Text>
                      <DatePicker
                                 style={{width: 200, borderRadius : 10}}
                                 date={this.state.date}
                                 mode="date"
                                 placeholder="select date"
                                 format="DD-MMM-YYYY"
                                 minDate="01-Jan-1900"
                                 maxDate={this.state.tdyDate}
                                 confirmBtnText="Confirm"
                                 cancelBtnText="Cancel"
                                 disabled = {false}
                                 customStyles={{
                                   dateIcon: {
                                     position: 'absolute',
                                     left: 0,
                                     top: 4,
                                     marginLeft: 0
                                   },
                                   dateInput: {
                                     marginLeft: 36,
                                     borderRadius : 10,
                                     backgroundColor :"#DCDCDC"
                                   }
                                   // ... You can check the source to find the other keys.
                                 }}
                                 onDateChange={(date) => {this.setState({date: date},()=>{this.state.date})}}
                                />

                      </View>
                    </View>:null
                    }
                      {
                   (this.state.isEditing == true)?<View>
                                       <View style={[AppStyles.Input_iconcnt,{borderWidth:0}]}>
                                       <Text style = {{fontWeight:'bold',fontSize:15}}>Choose your country</Text>
                                     </View>
                                       <RadioForm
                                               radio_props={radio_props}
                                               initial={preCountrySelectedIndex}
                                               style={AppStyles.radio}
                                               labelStyle={{fontSize: 15, color: '#208a92',backgroundColor:'#00000000',}}
                                               buttonColor={'#208a92'}
                                               color={'#208a92'}
                                               onPress={(currency) => {this.setCurrencyData(currency)}}/>
                                     </View>:
                                     <View style={[AppStyles.Input_iconcnt,{borderWidth:0}]}>
                                       <Text style = {{fontWeight:'bold',fontSize:15}}> Country :  {country}</Text>
                                     </View>
                 }
                                     <View style={[AppStyles.Input_iconcnt,{borderWidth:0}]}>
                                       <Text style = {{fontWeight:'bold',fontSize:15}}> Currency :  {currency}</Text>
                                     </View>




                      {
                        (this.state.isEditing)?<LoginButton text={"Update"} onPress={()=>{if(_that.state.isConnected){this.Save()}}} style={[AppStyles.green_round_button,{marginBottom : 50}]} />:null
                      }
                      <Spinner visible={this.state._visible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />
                  </View>
                </TouchableWithoutFeedback>
              </ScrollView>


        );
    }
    setCurrencyData(currency){
      if(currency=='$'){
        this.setState({currency:currency,cca2:'US'})
      }else{
        this.setState({currency:currency,cca2:'GB'})
      }
    }

    //Calling Logout API Call
    //C
    logoutApiCall = ()=>{
      var data = {
          "UserID": this.state.UserID

      }
      this.PostToApiCalling('POST', 'UserLogout', Constant.URL_UserLogout, data)


    }

    setEditingTextInput = () => {
        this.setState({ isEditing: !this.state.isEditing })
    }

    /*
            Get User Profile Data
     */
    btnApiCalled = () => {
        const { UserID } = this.state

        this.setState({ _visible: true })
        var data = {
            "UserID": this.state.UserID,
        }

        this.PostToApiCalling('POST', 'GetUserProfile', Constant.URL_ViewProfile, data);

    }
/*
        Functionality of Update Btn
 */
        Save = () => {
          if(this.state.isEditing == true)
          {
            const { coreimage, email, full_name, mobile_number, Password, confirm_password, email_id, avatarSource, UserID ,currency} = this.state
            // Loader show
            this.setState({ _visible: true })

            /**
             *  Form  Validations
             */

            if (full_name == "") {
                this.setState({ _visible: false });
                commonFunctions.message("Please enter Full Name.")
            } else if (commonFunctions.validateName(full_name)) {
                this.setState({ _visible: false });
                commonFunctions.message("Please enter Correct Name.")
            }else if (mobile_number == "") {
                this.setState({ _visible: false });
                commonFunctions.message("Please enter Mobile Number.")
            }
            else if(this.state.isFbLogin == "0")
            {
                      if (Password == "") {
                          this.setState({ _visible: false });
                          commonFunctions.message("Please enter Password.")
                      } else if (confirm_password == "") {
                          this.setState({ _visible: false });
                          commonFunctions.message("Please enter Confirm Password.")
                      } else if (Password != confirm_password) {
                          this.setState({ _visible: false });
                          commonFunctions.message("Confirm Password does not match.")
                      }else if (email_id == "") {
                          this.setState({ _visible: false });
                          commonFunctions.message("Please enter Email ID.")
                      }
                      else  if(!commonFunctions.validateEmail(email_id)) {
                          this.setState({ _visible: false });
                          Alert.alert("Enter Valid Email.");
                      }
                      // else if(this.checkDate())
                      // {
                      //       commonFunctions.message("Please enter Valid Date.")
                      //
                      // }
                      else {


                          _that.setProfileData(timeStamp)

                      }
            }
            else {



              _that.setFacebookProfileData(timeStamp)

          }
        }

        // checkDate = () => {
        //   var strDate = this.state.date.toString();
        //   var numDate = parseInt(strDate);
        //   var strTdyDate = this.state.tdyDate;
        //   var numTdyDate = parseInt(strTdyDate);
        //   if(numDate <= numTdyDate)
        //   {
        //     return true
        //   }
        //   return false
        //
        // }

    /**
     * API Calling
     */
}


/*
    update the Profile data after coming from facebook login
 */
     setFacebookProfileData(){

         const { coreimage, email, full_name, mobile_number, Password, confirm_password, email_id, avatarSource, UserID ,currency} = this.state

         var data = {

             "Name": full_name,
             "PhoneNo": mobile_number,
             "Password": Password,
             "Email": email_id,
             "UserID": this.state.UserID,
             "CountryName": (currency=='£')?'UK':'USA',
             "CurrencyName": currency,
             "CountryCode":(currency=='£')?"44":"1",
             "DOB":this.state.date,

         }
         this.PostToApiCalling('POST', 'UserProfileUpdate', Constant.URL_Update_Profile, data);
     }

     /*
        update the Profile data and calling UserProfileUpdate API
      */

    setProfileData(){
      const { coreimage, email, full_name, mobile_number, Password, confirm_password, email_id, avatarSource, UserID ,currency} = this.state

      var data = {

          "Name": full_name,
          "PhoneNo": mobile_number,
          "Password": Password,
          "Email": email_id,
          "UserID": this.state.UserID,
          "CountryName": (currency=='£')?'UK':'USA',
          "CurrencyName": currency,
          "CountryCode":(currency=='£')?"44":"1",
          "DOB":this.state.date,

      }

      this.PostToApiCalling('POST', 'UserProfileUpdate', Constant.URL_Update_Profile, data);

    }

    /**
    * API Calling
    */


    PostToApiCalling(method, apiKey, apiUrl, data) {
        new Promise(function (resolve, reject) {
            if (method == 'POST') {
                resolve(WebServices.callWebService(apiUrl, data));
            } else {
                resolve(WebServices.callWebService_GET(apiUrl, data));
            }
        }).then((jsonRes) => {
            this.setState({ _visible: false })
            _that.apiSuccessfullResponse(apiKey, jsonRes)

        }).catch((error) => {
            console.log("ERROR" + error);
        });
    }
    getTimeStamp = ()=>{

      // var date = this.state.date.toString();
      // const dateTime = new Date(date).getTime();
      // timeStamp = (Math.floor(dateTime / 1000));

       var date = this.state.date;
      // const dateTime = new Date(date).getTime();
       timeStamp = date
    }

    apiSuccessfullResponse(apiKey, jsonRes) {
        const { _email, _password, _isRemember } = this.state
        if (apiKey == 'GetUserProfile') {
          //console.log('user ptofile is '+JSON.stringify(jsonResponse.ResponsePacket));

          this.setState({ _visible: false })
            var jsonResponse = jsonRes.ResponsePacket;
            var unixTime = jsonRes.ResponsePacket.DOB;

          //Converting a unix date into date object
            var dateObj = new Date(unixTime *1000);
            var month = dateObj.getUTCMonth() + 1; //months from 1-12
            var day = dateObj.getUTCDate();
            var year = dateObj.getUTCFullYear();

           var dateFmt = year + "-" + month + "-" + day;
            console.log(dateFmt);
            if (jsonRes.ResponseCode == "0") {
              this.setState({ _visible: false })
                this.setState({ full_name: jsonResponse.Name })
                this.setState({ mobile_number: jsonResponse.PhoneNo })
                this.setState({ Password: jsonResponse.Password })
                this.setState({ confirm_password: jsonResponse.Password })
                this.setState({ email_id: jsonResponse.Email })
                this.setState({currency:jsonResponse.CurrencyName})
                this.setState({country:jsonResponse.CountryName})
                this.setState({ date: jsonRes.ResponsePacket.DOB })
                this.setState({ _visible: false })
                if(jsonResponse.CurrencyName=='$'){
                  this.setState({preCountrySelectedIndex:1,cca2:'US'})
                }else{
                  this.setState({preCountrySelectedIndex:0,cca2:'GB'})
                }
            } else {

            }
        }else if (apiKey == 'UserProfileUpdate') {
            if (jsonRes.ResponseCode == 0) {
              if(Platform.OS == "ios")
              {
                _that.setState({ _visible: false })
                      setTimeout(()=>(AlertIOS.alert(
                    'My Profile',
                    jsonRes.ResponseMessage,
                    [
                      {
                        text: 'OK',
                        onPress: () => {_that.props.navigation.goBack()},

                      },

                    ]
                  )), 500)
                  _that.setState({ _visible: false })
              }
              else {
                Alert.alert(
                        'My Profile',
                        jsonRes.ResponseMessage,
                        [
                          {text: 'OK', onPress: () => {_that.setState({ _visible: false });_that.props.navigation.goBack()}},

                        ],
                        { cancelable: false }
                      );

                  _that.setState({ _visible: false })
              }

                this.setState({ _visible: false })


                var data = {

                    "PhoneNo": jsonRes.ResponsePacket.PhoneNo,
                    "Name": jsonRes.ResponsePacket.Name,
                    "UserID": jsonRes.ResponsePacket.UserID,
                    "CurrencyName":jsonRes.ResponsePacket.CurrencyName,
                    "isRemember": this.state._isRemember,



                }

              if (this.state._isRemember){  AsyncStorage.setItem("UserData",JSON.stringify(data))}
                this.setState({ _visible: false })
            }else if(jsonRes.ResponseCode == 1){

              if(Platform.OS == "ios")
              {
                _that.setState({ _visible: false })
                      setTimeout(()=>(AlertIOS.alert(
                    'My Profile',
                    jsonRes.ResponseMessage,
                    [
                      {
                        text: 'OK',
                        onPress: () => {_that.props.navigation.goBack()},

                      },

                    ]
                  )), 500)
                  _that.setState({ _visible: false })
              }
              else {
                Alert.alert(
                        'My Profile',
                        jsonRes.ResponseMessage,
                        [
                          {text: 'OK', onPress: () => {_that.setState({ _visible: false });_that.props.navigation.goBack()}},

                        ],
                        { cancelable: false }
                      );

                  _that.setState({ _visible: false })
              }

            }


            else if(jsonRes.ResponseCode == "2"){

              if(Platform.OS == "ios")
              {
                _that.setState({ _visible: false })
                      setTimeout(()=>(AlertIOS.alert(
                    'My Profile',
                    jsonRes.ResponseMessage,
                    [
                      {
                        text: 'OK',
                        onPress: () => {},

                      },

                    ]
                  )), 500)
                  _that.setState({ _visible: false })
              }
              else {
                Alert.alert(
                        'My Profile',
                        jsonRes.ResponseMessage,
                        [
                          {text: 'OK', onPress: () => {_that.setState({ _visible: false })}},

                        ],
                        { cancelable: false }
                      );

                  _that.setState({ _visible: false })
              }

            }


        }
        else if(apiKey == 'UserLogout')
        {
          commonFunctions.message(jsonRes.ResponseMessage)
            if (jsonRes.ResponseCode == "0") {

              this.props.navigation.dispatch({
              type: NavigationActions.NAVIGATE,
              routeName: 'Login',
              action: {
                type: NavigationActions.RESET,
                index: 0,
                actions: [{type: NavigationActions.NAVIGATE, routeName: 'Login'}]
              }})
            // this.props.navigation.navigate('Login',{screen:"logout"})
              BackHandler.removeEventListener('hardwareBackPress',this.handleBackButtonClick);
            }
        }

    }



}

export default ViewProfile;
