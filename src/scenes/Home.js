/**
 *  Please find react-native-signature-capture on below url
 *  https://github.com/RepairShopr/react-native-signature-capture
 */
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Button,
    TouchableOpacity,
    Image,
    Dimensions,
    ScrollView,
    TouchableHighlight,
    AsyncStorage,
    BackHandler,
    SafeAreaView,
    Alert,
    DeviceEventEmitter,
    StatusBar,
    NetInfo
} from 'react-native';
var _that;
var ImagePicker = require('react-native-image-picker');
var { height, width } = Dimensions.get('window');
import { AppStyles, AppSizes, AppColors } from '../themes/'
const add_new_product = require('../themes/Images/add-new-product.png');
const auction_invites = require('../themes/Images/auction-invites.png');
const auctions_won = require('../themes/Images/auctions-won.png');
const my_auction_list = require('../themes/Images/my-auction-list.png');
const bckimg = require('../themes/Images/bg-bottom.png')
import Custom_Component, { LoginButton, Button2, Header } from "../components/Buttons";
import * as commonFunctions from '../utils/CommonFunctions'


class Home extends React.Component {
    constructor(props) {
        super(props)
        _that = this;
        this.state = {
            isConnected : null,
        }
    }
    componentWillMount() {
      NetInfo.isConnected.fetch().done(
        (isConnected) => { this.setState({ isConnected: isConnected}); }
      );
        //  commonFunctions.checkConnection();
        DeviceEventEmitter.addListener("isConnected",this.checkConnection.bind(this));

    }
    componentDidMount() {
     BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

      componentWillUnmount() {
          DeviceEventEmitter.removeListener("isConnected",this.checkConnection.bind(this));
      }
      //Checking the Internet Connection
      checkConnection(data){

            this.setState({isConnected : data.isConnected});
      }

 handleBackButton() {

     return true;
 }
    static navigationOptions = {
        drawerLabel: 'Messages'
    };
    render() {
        return (


             <SafeAreaView style={{ flex: 1,backgroundColor:"white"}}>
               <View>

                <Image source={bckimg} style={{ width: AppSizes.screen.width, height: AppSizes.screen.height, position: 'absolute', top: 0, left: 0 }} />

              <ScrollView>

                <View style={{
                    justifyContent: 'center',
                    alignItems: 'center', flexDirection: 'row', marginTop: 0,
                }}>
                <Header text='Home' navigation = {this.props.navigation}/>
                <View style={[AppStyles.GridBox, { marginTop: 165 }]}>
                    <TouchableOpacity onPress={this.ViewProfile} style = {{alignItems: "center", justifyContent :"center"}}>
                        <Image source={auctions_won} style={[AppStyles.GridBoxImg,{ width: 150, height: 120}]} />
                        <Text style={[AppStyles.GridBoxTxt]} >View {"\n"}Profile</Text>
                    </TouchableOpacity>
                </View>

                <View style={[AppStyles.GridBox, { marginTop: 165 }]} >
                    <TouchableOpacity onPress={this.MyExchange} style = {{alignItems: "center", justifyContent :"center"}}>
                        <Image source={auctions_won} style={[AppStyles.GridBoxImg,{ width: 150, height: 120}]} />
                        <Text style={[AppStyles.GridBoxTxt]} >Exchange {"\n"}Products </Text>
                    </TouchableOpacity>
                </View>


                </View>
                <View style={{
                    justifyContent: 'center',
                    alignItems: 'center', flexDirection: 'row', marginTop: 0
                }}>

                    <View style={AppStyles.GridBox} >
                        <TouchableOpacity onPress={this.AddNewProduct} style = {{alignItems: "center", justifyContent :"center"}}>
                            <Image source={add_new_product} style={AppStyles.GridBoxImg} />
                          <Text style={[AppStyles.GridBoxTxt]}>Add   {"\n"}New Product </Text>
                        </TouchableOpacity>
                    </View>

                    <View style={AppStyles.GridBox}>
                        <TouchableOpacity onPress={this.MyAuctions} style = {{alignItems: "center", justifyContent :"center"}}>
                            <Image source={my_auction_list} style={AppStyles.GridBoxImg} />
                            <Text style={[AppStyles.GridBoxTxt]}>My {"\n"}Auction List </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <View style={AppStyles.GridBox} >
                        <TouchableOpacity onPress={this.AuctionsInvites} style = {{alignItems: "center", justifyContent :"center"}}>
                            <Image source={auction_invites} style={AppStyles.GridBoxImg} />
                            <Text style={[AppStyles.GridBoxTxt]}>Auction{"\n"}Invites</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={AppStyles.GridBox} >
                        <TouchableOpacity onPress={this.AuctionsWon} style = {{alignItems: "center", justifyContent :"center"}}>
                            <Image source={auctions_won} style={AppStyles.GridBoxImg} />
                            <Text style={[AppStyles.GridBoxTxt]} >Auction's{"\n"}won</Text>
                        </TouchableOpacity>
                    </View>
                </View>
              </ScrollView>
              </View>

            </SafeAreaView>

        );
    }
    AddNewProduct = () => {
        this.props.navigation.navigate('AddNewProduct',{screen:"Home"})
    }

    MyAuctions = () => {
      if(this.state.isConnected)
      {
        this.props.navigation.navigate('MyAuctions')
      }
    }
    AuctionsInvites = () => {
      if(this.state.isConnected)
      {
          this.props.navigation.navigate('AuctionsInvites',{fromWhere :"HomeScreen"})
      }
    }
    AuctionsWon = () => {
      if(this.state.isConnected)
      {
        this.props.navigation.navigate('AuctionsWon')
      }
    }
    MyExchange = () => {
      if(this.state.isConnected)
      {
      const { navigate } = _that.props.navigation;
      navigate('MyExchange', { ProductID: "" })
    }
        //this.props.navigation.navigate('MyExchange')

    }
    ViewProfile = ()=>{

      if(this.state.isConnected)
      {
      this.props.navigation.navigate('ViewProfile')
      }
    }


}


export default Home

const styles = StyleSheet.create({

    button: {
        backgroundColor: '#467702',
        height: 50,
        marginTop: 10,
        marginBottom: 10,
        alignItems: 'center',
        paddingTop: 13,
        borderRadius: 4,
        width: width - 50,
        borderBottomWidth: 5,
    }

});
