import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    ScrollView,
    FlatList,
    Dimensions,
    TextInput,
    ListView,
    DatePickerIOS,
    TouchableOpacity,
    Alert,
    Keyboard,
    TouchableWithoutFeedback,
    NetInfo,
    Platform,
    AsyncStorage,
    BackHandler,
    RefreshControl,
    SafeAreaView,
    DeviceEventEmitter

} from 'react-native';
import SelectMultiple from 'react-native-select-multiple';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';

import { AppStyles, AppSizes, AppColors } from '../themes/'
import Buttons, { LoginButton, Button2,Header,NoRecordFound } from "../components/Buttons";
import Spinner from 'react-native-loading-spinner-overlay';
import * as commonFunctions from '../utils/CommonFunctions'
var Constant = require('../api/WebInteractor').Constant;
var WebServices = require('../api/WebInteractor').WebServices;
var logo_img = require('../themes/Images/logo-img.png');
var bckimg = require('../themes/Images/bg-bottom.png');
var bg_red = require('../themes/Images/bg_red.png');

var data = []

var ds;

class BidderList extends Component<{}>
{
    constructor(props)
    {
        super(props)
        _that = this
        ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
          _bidderName:'',
          _bidAmount:'',
          _bidDate:'',
          bidID:'',
          dataSource: ds.cloneWithRows(data),
          ProductID: this.props.navigation.state.params.ProductID,
          UserID: '',
          selectedbidder_lst: [],
          bidderList:[],
          refreshing:false,
          isDataAvailable:true,//intially its show LISTVIEW with zero Items

        }
    }
    static navigationOptions = ({navigation, screenProps}) => ({

                 headerStyle : {

                          backgroundColor: '#89f4ef'
                 },
                 headerTintColor : "white",

                 headerLeft : <TouchableOpacity style = {{justifyContent :"center",alignItems :"center",marginLeft : 3,width : 40,height : 40}} onPress = {() => {navigation.goBack()}}>
                     <Image source = {require('../themes/Images/back-arrow.png')}/>
                   </TouchableOpacity>,


    });

    componentWillMount(){
      AsyncStorage.getItem('UserData')
          .then((res) => {
              if (res) {
                  var data = JSON.parse(res)
                  if (data.isRemember) {
                      this.setState({
                          UserID: data.UserID
                      })
                      this.GetBiddersListApiCalled()
                  }
              }
          });
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
      DeviceEventEmitter.addListener('LiveUpdateRequest', this.LiveUpdateBidderList.bind(this))

    }
    componentWillUnmount() {

        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
        DeviceEventEmitter.removeListener('LiveUpdateRequest', '')
    }

  //GO BACK TO previous screen on device back button click
  handleBackButtonClick() {
      _that.props.navigation.goBack();
      return true;
   }

   /*
   Refreshing the product list
   */
   _onRefresh() {
       this.setState({refreshing: true});
       this.GetBiddersListApiCalled()
   }

   /*
      Rendering the BidderList
   */

   render(){

     return(
         <KeyboardAwareView animated={true}>
        <SafeAreaView style={{ flex: 1}}>
       <View style={{flex: 1, backgroundColor: AppColors.white, }}>
        <Image source={bckimg} style={{ width: AppSizes.screen.width, height: AppSizes.screen.height, position: 'absolute', top: 0, left: 0 }} />
        <ScrollView style={{flex: 1 }}
                    refreshControl={
                                     <RefreshControl
                                       refreshing={this.state.refreshing}
                                         onRefresh={this._onRefresh.bind(this)}
                                     />
                               }>

         <Header text='BIDDER LIST' navigation = {this.props.navigation}/>
         <View style = {{paddingTop:170}}>
         </View>

         <View style = {{alignItems:'center',paddingTop:10}}>
           <View style = {[AppStyles.bidderList_cnt,{borderWidth : 2, borderColor :"black"}]}>
           <View style = {{flexDirection:'row',height : 30,borderColor : "black",borderBottomWidth : 2,backgroundColor:"#0c8580",height:50,alignitems :"center",justifyContent :"center"}}>
             <View style = {{flexDirection:'column',flex:1,borderRightColor : "black",alignSelf :"center"}}>
               <Text style = {[AppStyles.bidderListText,{color:"white"}]}>Bidder Name</Text>
             </View>
             <View style = {{flexDirection:'column',flex:1,alignSelf :"center"}}>
               <Text style = {[AppStyles.bidderListText,{color:"white"}]}>Amount</Text>
             </View>
             <View style = {{flexDirection:'column',flex:1.1,alignSelf :"center"}}>
               <Text style = {[AppStyles.bidderListText,{color:"white"}]}>Bid Date</Text>
             </View>
             <View style = {{flexDirection:'column',flex:1.1,alignSelf :"center"}}>
               <Text style = {[AppStyles.bidderListText,{color:"white",textAlign :"center"}]}>Acceptance</Text>
             </View>
           </View>

           {
            (this.state.isDataAvailable==true)?
             <ListView dataSource = {this.state.dataSource} renderRow={(data, rowID) => this.renderRow(data, rowID)} enableEmptySections={true}/>:<NoRecordFound></NoRecordFound>

             }
           </View>
         </View>




           <Spinner visible={this.state._visible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />


          </ScrollView>
       </View>
       </SafeAreaView>
       </KeyboardAwareView>
     )
   }


    renderRow(data, rowID) {



        return (
          <View style = {{flexDirection:'row',flex:1}}>
            <View style = {{flexDirection:'column',flex:1,paddingTop:23,paddingLeft:5}}>
              <Text style = {{textAlign:'center',color:'black',fontSize : 15}}>{data.UserName}</Text>
            </View>
            <View style = {{flexDirection:'column',flex:1,paddingTop:23,paddingLeft:5}}>
              <Text style = {{textAlign:'center',color:'black',fontSize : 15}}>{data.CurrencyName}{data.BidAmount}</Text>
            </View>
            <View style = {{flexDirection:'column',flex:1.3,paddingTop:23,paddingLeft:0}}>
              <Text style = {{textAlign:'center',color:'black',fontSize : 15}}>{(data.BidDate).substring(0,10)}</Text>
            </View>
            <View style = {{flexDirection:'column',flex:1,paddingTop:15,paddingLeft:5}}>
                <TouchableOpacity style = {{backgroundColor:"#f77f83",width:65,height : 35,borderRadius : 15,alignSelf : "center",justifyContent : "center"}} onPress = {()=> this.setBidApiCalled(data.ID)}>
                  <Text style = {{color:'black',fontWeight:'bold',textAlign:'center',fontSize : 12}}>ACCEPT</Text>
                </TouchableOpacity>
            </View>
          </View>
        )
    }

/*
  On ACCEPTING bid
 */
    setBidApiCalled=(bidId)=>{

      Alert.alert(
              'Bid Win',
              'Do you really want to let win the selected person',
              [
                {text: 'YES', onPress: () => {this.AcceptedBid(bidId);this.props.navigation.goBack();(this.props.navigation.state.params.from=='BidderList')?null:this.props.navigation.state.params.refresh(this.state.ProductID)}},
                {text: 'NO', onPress: () => {},style: 'cancel'},
              ],
              { cancelable: false }
            )



    }
    //Calling API when ccepting the bid
    AcceptedBid = (bidId)=>{
      this.setState({ _visible: true })
      var data = {
          "BidID":bidId
      }
      this.PostToApiCalling('POST', 'UpdateAuctionStatus', Constant.URL_UpdateAuctionStatus, data)
    }



    onSelectionsChange = (selectedcontacts_lst) => {

        if (selectedcontacts_lst.length < 15) {

            this.setState({selectedcontacts_lst})

        } else {
            alert("Maximum invitation 15 user at a time")
        }
    }


 //API Calling for BidderList
    GetBiddersListApiCalled=()=>{

      const {ProductID} = this.state
      this.setState({ _visible: true })
      var data = {
          "ProductID": ProductID,
          "PageNumber": 1,
          "PageSize": 1000
      }
      this.PostToApiCalling('POST', 'GetBiddersList', Constant.URL_GetBiddersList, data);
      this.setState({ refreshing: false })
    }


    /**
     * [LiveUpdateBidderList calls GetBiddersList api for Live Updating BidderList]
     */

    LiveUpdateBidderList=()=>{

      const {ProductID} = this.state
      var data = {
          "ProductID": ProductID,
          "PageNumber": 1,
          "PageSize": 1000
      }
      this.PostToApiCalling('POST', 'GetBiddersList', Constant.URL_GetBiddersList, data);

    }



    /**
    * API Calling
    */


    PostToApiCalling(method, apiKey, apiUrl, data) {
        new Promise(function (resolve, reject) {
            if (method == 'POST') {
                resolve(WebServices.callWebService(apiUrl, data));
            } else {
                resolve(WebServices.callWebService_GET(apiUrl, data));
            }
        }).then((jsonRes) => {
            if ((!jsonRes) || (jsonRes.ResponseCode == 0)) {
                this.apiSuccessfullResponse(apiKey, jsonRes)
                this.setState({ _visible: false })

            } else {
                commonFunctions.message(jsonRes.ResponseMessage)
                this.setState({ _visible: false })
            }
        }).catch((error) => {
            console.log("ERROR" + error);
        });
    }

    apiSuccessfullResponse(apiKey, jsonRes) {

        if (apiKey == 'GetBiddersList') {
            var jsonResponse = jsonRes.ResponsePacket;

            if (jsonResponse.length == 0) {
                _that.setState({ _visible: false })
                setTimeout(()=>{_that.setState({ _visible: false });
                _that.setState({ isDataAvailable: false })},300)
              //  _that.setState({ _visible: false })

            } else {
                // commonFunctions.message(jsonRes.ResponseMessage)
                this.setState({ _visible: false, dataSource: ds.cloneWithRows(jsonResponse), })

            }
        } else if(apiKey == 'UpdateAuctionStatus'){

            var jsonResponse = jsonRes.ResponsePacket;

            if (jsonRes.ResponseCode == "0") {
                setTimeout(()=>{commonFunctions.message(jsonRes.ResponseMessage)},300)
                this.setState({ _visible: false })
            } else {

            }
        }
    }
}

export default BidderList;
