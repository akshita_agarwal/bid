/**
 * Created by rishi paliwal on 6 nov 2017
 * Global function
*/

import React, { Component } from 'react';
import {
    Alert,
    NetInfo,
} from 'react-native';
/*
    This is use for Email validation
*/
export function validateEmail(email) {
    //var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var re=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email);
};
export function validateAmount(amount) {
    //var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var pattern = /^-?\d*(\.\d+)?$/;
    return(pattern.test(amount))
};
export function validatePassword(password) {
    var pattern = /^[0-9a-zA-Z]{6,}$/

    return(pattern.test(password))
};
export function validatePhoneNum(phoneNum, selectedIndex) {
    var pattern = /^([0-9]){10,15}$/;
    var num = phoneNum.replace(/\b0+/g, '');

    //var data = num.test(phoneNum)
    var data;
    if(!pattern.test(num)){
      data = true;
    }
    else if(selectedIndex == "0")
    {
        if(num.startsWith("44", 0))
        {

            data =  "UK";
        }
    }
    else  if(selectedIndex == "1")
      {
          if(num.startsWith("1", 0))
          {
              data =  "USA";
          }
      }
    return data
};
export function validateContact(phoneNum) {
    var pattern = /^[- +()]*[0-9][- +()0-9]*$/

    return pattern.test(phoneNum)
};


export function validateName(name) {
    var str = name.replace(/^[ ]+|[ ]+$/g,'');
    if(str.length > 0)
    {
        return false;
    }
    else {
        return true;
    }

};


/*
    This is use for alert messaging
*/
export function message(msg) {
    return setTimeout(()=>(Alert.alert(msg),500));
};

export function checkConnection()
{
  var a;
  NetInfo.isConnected.fetch().then(isConnected => {
    // Alert.alert('First, is ' + (isConnected ? 'online' : 'offline'));
     if(isConnected == false)
     {
          a = false;

     }
     else {
       a = true;
     }
  });
      if(a == false)
     {
        return false;
     }
     if(a == true)
     {
        return true;
     }

 };




/*
    This is use for Email validation
*/
export function fetchInterestIDFromArray(arr) {
    var arrIntrstIDs = []
    arr.map(function (datas, indexs) {
        arrIntrstIDs.push(datas.InterestID + 1)
    })
    //output like ['1', '2','3']
    return arrIntrstIDs
}



/*
    This is use for Email validation
*/
export function repeatedOn(sun, mon, tue, wed, thur, fri, sat) {
    var arr = []
    if (sun) {
        arr.push(' Sunday')
    }
    if (mon) {
        arr.push(' Monday')
    }
    if (tue) {
        arr.push(' Tuesday')
    }
    if (wed) {
        arr.push(' Wednesday')
    }
    if (thur) {
        arr.push(' Thursday')
    }
    if (fri) {
        arr.push(' Friday')
    }
    if (sat) {
        arr.push(' Saturday')
    }
    return arr
}



/*
    This is use for Email validation
*/
export function dateFormat_Date(dateInput) {
    var dateFormat = new Date(dateInput).toLocaleDateString('en-GB', {
        day: 'numeric',
        month: 'short',
        year: 'numeric'
    }).split(' ').join(' ');

    return dateFormat
}
