import React from 'react';
import {
    Dimensions,
    Platform
} from 'react-native';

const { width, height } = Dimensions.get("window");

import Colors from './color';
import Fonts from './fonts';
import Sizes from './sizes';

export default {

    appContainer: {
        backgroundColor: Colors.white,

    },

    // parent Container style


    container: {
        flex: Colors.flex1,
        flexDirection: Colors.flexDirectioncolumn,
        backgroundColor: Colors.white,
    },
    rounded_button: {

    },
    /**
     * Login file css
     */

    container_space: {
        flex: 1,
        flexDirection: Colors.flexDirectioncolumn,
        justifyContent: Colors.justifyContentcenter,
        alignItems: Colors.alignItemscenter,
        marginLeft: Colors.marginLeft30,
        marginRight: Colors.marginRight30,
        marginBottom: Colors.marginBottom30,
    },
    login_logo_cnt: {
        justifyContent: Colors.justifyContentcenter,
        alignItems: Colors.alignItemscenter,
        flexDirection: Colors.flexDirectionrow
    },

    login_logo: {
        width: width-180,
        height: 100,
        marginTop: Colors.marginTop15,
        marginBottom: 10,
        alignSelf : "center",
        resizeMode: 'contain',
    },
    Input_iconcnt: {
        flexDirection: Colors.flexDirectionrow,
        alignItems: Colors.alignItemscenter,
        borderColor: Colors.lightGray_text,
        borderWidth: 1,
        borderRadius: 10,
        height: 50,
        marginTop: Colors.marginTop10
    },
    InputCnt_icon: {
        width: 55,
        height: 39,
        alignItems: Colors.alignItemscenter
    },
    Input_img: {
        width: 30,
        height: 25,
        marginTop: Colors.marginTop6,
    },

    Inputtxt: {
        flex: Colors.flex6, height: 40
    },
    InputCnt_txt: {
        marginLeft: Colors.marginLeft10,
        height: (Platform.OS === 'ios') ? 40 : 40,
        fontSize: 18,
        fontFamily: Colors.fontFamilyRegular,
    },
    forgot_txt: {
        color: Colors.lightGray_text,
        fontSize: 17,
        fontWeight: 'bold',
        //fontFamily: Colors.fontFamilyRegular,
        textAlign: Colors.textAlignCenter,
        marginTop: Colors.marginTop15,
        backgroundColor: 'rgba(0,0,0,0)',
    },
    checkBox: {
        flex: Colors.flex1,
        padding: Colors.padding10,
    },
    green_round_button: {
        backgroundColor: '#f77f83',
        height: 50,
        marginTop: Colors.marginTop20,
        textAlign: Colors.textAlignCenter,
        color: Colors.white,
        paddingTop: Colors.paddingTop15 - 2,
        borderRadius: 10,
        fontSize: 20,
        fontFamily: Colors.fontFamilyBold,
        width : Sizes.screen.width-60,

    },
    radio: {
    flexDirection:'row',
    marginLeft:3,
    marginRight:5,
    },
    /**
        * Login file css
        */
    login_container: {
        flex: 1,
        padding: 20
    },

    bg_topImg: {
        width: Sizes.screen.width,
        height: Sizes.screen.height,
        position: 'absolute',


    },

    logo_login_cnt: {
        height: 100,
        flexDirection: 'column',
        paddingTop: 5,
        alignItems: 'center'
    },
    logo_login: {
        width: 100,
        height: 100
    },

    logo_textImg_cnt: {
        height: 100,
        paddingTop: 40

    },

    logo_textImg: {
        alignSelf: 'center',
        resizeMode: 'contain',

    },

    InputPhnNumtxt_cnt: {
        borderColor: 'grey',
        alignSelf: 'center',
        marginTop: 20,

    },

    InputPhnNumtxt: {
        width: 150,
        height: 40,
        fontWeight: 'bold',
        backgroundColor: 'white',

    },


    InputPasswordtxt_cnt: {
        borderColor: 'grey',
        alignSelf: 'center',


    },

    InputPasswordtxt: {
        width: 150,
        fontWeight: 'bold',
        color: 'black',
        backgroundColor: 'white',
        height: 40
    },


    loginTextStyle_cnt: {
        alignItems: 'center',
        paddingTop: 10,
        borderBottomWidth : 1,
        borderColor : "black",
    },
    loginWithFbTextStyle_cnt: {
        alignItems: 'center',
        paddingTop: 10,

    },

    loginbtn_cnt: {
        alignItems: 'center',
        paddingTop: 5,

    },

    loginbtn: {
        paddingTop: Platform.OS =="ios"?9:7,
        fontSize: 18,
        width: 100,
        textAlign: 'center',
        height: 40,
        color: 'white',
        borderRadius: 10,
        overflow: (Platform.OS == "ios")?'hidden': null,

        backgroundColor: '#3B9D99'
    },

    fb_img_cnt: {
        alignItems: 'center',
        paddingTop: 5,

    },
    fb_img: {
        width: 50,
        height: 50
    },
    forgetPasswordBtn: {
        width: window.width,
        marginRight: 1,
        backgroundColor: 'transparent',
    },
    forgetPasswordText: {
        fontSize: 12,
      //  fontFamily: 'FuturaStd-Book',
        marginLeft: 2,
        color: 'black',
        textAlign: 'center',
        backgroundColor: 'transparent',

    },

    SignUpBottomView: {
        width: window.width,
        height: 40,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: 'transparent',
        alignItems: 'center',
        position: 'absolute',
        left: 0,
        right: 0,
        marginTop : 30,
        alignSelf: 'center',

    },
    loginWithViewCentre: {
        width: (Platform.OS == "ios")? 300 :270,
        height: 30,
        backgroundColor: 'transparent',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',

    },
    loginWithText: {
      //  width: window.width,
        height: 30,
        flex: 1,
        textAlign: 'center',
        marginRight: 0,
        fontSize: 18,

      //  fontFamily: 'FuturaStd-Book',
        color: 'black',
    },
    signUpText: {
        height: 30,
        width: 80,
        textAlign: 'center',
        paddingTop: 5,
        fontSize: 15,
        borderRadius: 5,
        color: '#fff',
        backgroundColor: '#3B9D99',
      //  fontFamily: 'FuturaStd-Book',

    },
    signUpView: {
        height: 30,
        marginLeft : 0,
        alignItems :"center",
        justifyContent :"center",

    },

    copyright: {
        position: 'absolute', left: 0, right: 0, bottom: 0
    },
    copyright_txt: {
        color: '#00002f',
        fontSize: 10,
        textAlign: 'center',
        marginTop: 15,
        backgroundColor: 'rgba(0,0,0,0)',
    },


    modalContainer: {
        marginTop: (Sizes.screen.height / 5.5),
        alignSelf: "center",
        backgroundColor: 'white',
        borderWidth: 1,
        width: (Sizes.screen.width - 100),

    },
    innerContainer: {
        alignItems: 'center',

        //  backgroundColor : "yellow",
        marginTop: 20,
    },

    radio: {
    flexDirection:'row',
    marginLeft:5,
    marginRight:5,
    marginTop:5
  },




    /**
     * Custom Component
     */


    Headerbckimg: { padding: 15, position: 'absolute', top: 0, height: 110, width: Sizes.screen.width, backgroundColor: '#89f4ef' },
    Titlebckimg: { position: 'absolute', top: 110, height: 80, width: Sizes.screen.width, },
    TitleTxt: { position: 'absolute', marginTop: 5, fontSize: 25, fontWeight: 'bold', paddingLeft: 13, color: '#000' },
    customcpimg: { width: 300, height: 80 },

    /**
     * Home Screen
     */

    GridBox: { alignItems: 'center', justifyContent: 'center', flex: 1, height:  (Sizes.screen.height == 568)?140:150 },

    GridBoxImg: { width: 150, height: 120 },
    GridBoxTxt: { position: 'absolute', top: 0, textAlign: "center", marginTop: 30, fontSize: (Platform.OS == "ios" ? 21:22), fontWeight: 'bold', },

    /**
        * MyAuction list file css
        */


    filemanager_list: {
        flex: Colors.flex1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: Colors.justifyContentspacebetween,
        margin: 5,
        marginTop: 5,
        alignItems: Colors.alignItemscenter
    },
    filemanager_list_item: {

        borderWidth: 1,
        borderColor: '#000',
        margin: 5,
        paddingTop: 10,
        width: Sizes.screen.widthHalf - 20,
        borderRadius: 5
    },
    filemanager_folder_icon: {
        height: 100,
        marginTop: Colors.marginTop10,
        width: Sizes.screen.widthHalf - 50,
        resizeMode:"cover"
    },
    filemanager_folder_title: {
        textAlign: 'left',
        width: Sizes.screen.widthHalf - 40,
        paddingLeft : 5,

    },
    detail_filemanager_folder_title: {
        textAlign: 'left',
        width: Sizes.screen.width - 40,
        paddingLeft : 5,

    },
    filemanager_folder_view: {
        justifyContent: Colors.justifyContentcenter,
        alignItems: Colors.alignItemscenter,
    },
    detail_filemanager_folder_view: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        paddingRight: 10,
        margin:10,
    },
    Auctionlist_button: {
        alignSelf: 'center',
        width: 160,
        backgroundColor: '#89f4ef',
        height: 40,
        marginTop: Colors.marginTop10,
        textAlign: Colors.textAlignCenter,
        color: "#000",
        paddingTop: Colors.paddingTop10 - 4,
        borderRadius: 10,
        fontSize: 20,
        fontFamily: Colors.fontFamilyBold,
    },

    /*
    close
    */
    /**
         * BidderList file css
         */

    container_bidderList: {
        flex: 1,
        padding: 20
    },

    logo_img_cnt: {
        backgroundColor: 'powderblue',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 5
    },

    textBidderList_cnt: {
        alignItems: 'center',

        width: 200,
        height: 50,
        flexDirection: 'column',
        justifyContent: 'center'


    },

    textBidderList: {
        fontWeight: 'bold',
        fontSize: 15,
        color: 'black'
    },
    textAuctionTitle_cnt: {
        alignSelf: 'center',
        alignItems: 'center',
        padding: 5,
        backgroundColor: '#cdfaf8',
        width: 325
    },
    textAuctionTitle: {
        fontWeight: 'bold',
        fontSize: 20,
        color: 'black'
    },

    bidderList_cnt: {
        borderWidth: 1,
        borderColor: 'black',
        width: Sizes.screen.width-16,
        height: Sizes.screen.height-280,
        backgroundColor: 'white',
        marginLeft : 8,
        marginRight : 8,
    },
    bidderListText: {

        fontWeight: 'bold',
        fontSize: 13,
        textAlign : "center"

    },

    btnAcceptBid_cnt: {
      alignSelf: 'center',
      alignItems: 'center',
      padding: 5,
      backgroundColor: '#f77f83',
      width: 325,
    },

    textAcceptBid: {
      fontWeight: 'bold',
      fontSize: 20,
      color: 'black'
    },

    bg_Bottom: {

        position: 'absolute',
        width: Sizes.screen.width,
        height: Sizes.screen.height
    },
    /**
     * close
     */

};
