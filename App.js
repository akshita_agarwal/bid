/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ListView,
  Image,
  TouchableOpacity
} from 'react-native';
import TimerCountdown from 'react-native-timer-countdown'
import SendSMS from 'react-native-sms'
import SplashScreen from 'react-native-splash-screen';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
  'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
  'Shake or press menu button for dev menu',
});
var Contacts = require('react-native-contacts')
const fruits = [];
const renderLabel = (label, style) => {
  return (
    <View style={{ Position: 'absolute', left: 0, top: 0 }}>
      <View style={{}}>
        <Text style={{}}>{label}</Text>
      </View>
    </View>
  )
}
export default class App extends Component<{}> {


  componentDidUpdate () { SplashScreen.hide(); }
  constructor(prop) {
    super(prop)

  
    this.state = {
      selectedFruits: [],
      ContactsList: [],
    };
    Contacts.getAll((err, contacts) => {
      if (err === 'denied') {
        // error
      } else {
        contacts.forEach(function (element) {
          // alert(element.givenName)
          fruits.push(element.givenName)

        });
        this.setState({
          ContactsList: fruits
        })
        //alert(JSON.stringify(contacts[0].givenName))
        // alert(JSON.stringify(contacts[1].phoneNumbers[0].number));
        //contacts[0].givenName)

      }
    })
    //// send message to multiple recipient
    // SendSMS.send({
    //   body: 'The default body of the SMS!',
    //   recipients: ['8949493908', '9602068815','7877655025'],
    //   //android:launchMode="singleTask"
    //   successTypes: ['sent', 'queued']
    // }, (completed, cancelled, error) => {

    //   console.log('SMS Callback: completed: ' + completed + ' cancelled: ' + cancelled + 'error: ' + error);

    // });
  }

  fetchFacbookInfo = () => {
    LoginManager.logOut()
    const infoRequest = new GraphRequest('/me', {
      httpMethod: 'GET',
      version: 'v2.5',
      parameters: {
        'fields': {
          'string': 'email,name,friends'
        }
      }
    }, this.responseInfoCallback, );

    LoginManager.logInWithReadPermissions(['public_profile']).then(
      function (result) {
        if (result.isCancelled) {
        } else {
          new GraphRequestManager().addRequest(infoRequest).start();
        }
      },
      function (error) {
      }
    );
  }

  responseInfoCallback(error: ?Object, result: ?Object) {

    if (error) {
    } else {
      alert(JSON.stringify(result))
    }
  }



  onSelectionsChange = (selectedFruits) => {
    if (selectedFruits.length < 4) {
      this.setState({ selectedFruits })
    } else {
      alert("maximum invitation 10 user at a time")
    }


  }

  render() {
    return (
      <View>
        <TimerCountdown
          initialSecondsRemaining={60}
          onTick={() => console.log('tick')}
          onTimeElapsed={() => console.log('complete')}
          allowFontScaling={true}

        />
        <TouchableOpacity activeOpacity={.8} onPress={this.fetchFacbookInfo.bind()}>
          <Text>Login</Text>
        </TouchableOpacity>
        <SelectMultiple
          checkboxStyle={{ position: 'absolute', right: 10, top: 0 }}
          renderLabel={renderLabel}
          items={this.state.ContactsList}
          selectedItems={this.state.selectedFruits}
          onSelectionsChange={this.onSelectionsChange} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
